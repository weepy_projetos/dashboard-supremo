<div class="content-wrapper container-sem-menu">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="erro-atualizar">
    <div class="col-md-10">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao reservar</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="atualizado-sucesso">
    <div class="col-md-10">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Reservado com sucesso</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-10 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Reserva</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
            Inserir reserva de clientes.
          </p>
          <div class="row">
            <div class="col-md-6 ">
            <form id="formularios">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Novo cliente?</label>
                      <button type="button" class="novo-cliente btn btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                      <div class="handle"></div>
                      <input type="checkbox" name="compartilhamento" value="1" hidden="">
                    </button>
                  </div>
                  <div class="form-group novo-input" style="display:none">
                    <label>Nome</label>
                    <input type="text" name="nome" class="form-control">
                  </div>
                  <div class="form-group pesquisa-input">
                    <label>Pesquisar Cliente</label>
                    <div class="input-group">
                      <input type="text" name="nome-pesquisa" class="form-control" placeholder="Digite o nome">
                      <input type="text" name="id" hidden>
                      <div class="input-group-append">
                        <button id="consultar-nome" class="btn btn-sm btn-primary" type="button"><i class="mdi mdi-account-search icon-md"></i></button>
                      </div>
                    </div>
                    <ul id="lista-nome">
                    </ul>
                  </div>
                  <div class="form-group">
                    <label>Data</label>
                    <div id="mdp-demo"></div>
                    <div class="date-picker">
                        <div class="input">
                            <div class="result"><span id="dataSelecionada"><?php echo date("d/m/Y"); ?></span></div>
                            <!--<button id="btn-Calendário"><i class="mdi mdi-calendar-check menu-icon"></i></button>-->
                            <input type="hidden" id="data" name="data" value="<?php echo date("d/m/Y"); ?>">
                        </div>
                        <div class="calendar"></div>
                        <button type="button" id="confirmarData" class="btn btn-outline-primary btn-sm btn-block">Confirmar data</button>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Horário</label>
                    <select name="horario"class="form-control" id="horarioDisponivel">
                    <?php

                    $Hora = date_format($Data,"H"); //Hora de AGORA
                    $inicioAtendimento=strtotime($Hora . ':00');
                    $fim=strtotime('19:30');

                    $arrayHorarios = array();

                    foreach ($horarioLista as $linhHorario) {
                      $date = date_create($linhHorario->horario);
                      $horario = date_format($date,"H:i");
                      $arrayHorarios[] = $horario;
                    }

                    for ($i=$inicioAtendimento;$i<=$fim;$i = $i + 30*60) {

                      $tempo = date('H:i',$i);
                        
                      if (!empty($horarioLista)) {

                          if (in_array("$tempo",$arrayHorarios)) {
                              
                          } else {
                            echo "<option value='$tempo'>$tempo</option>";
                          }
     
                      } else {
                          echo "<option value='$tempo'>$tempo</option>";
                      }
                    };
                      ?>

                    </select>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Serviço</label>
                    <ul class="menu-servico">
                      <?php foreach ($categoriaLista as $linhaCategoria) { ?>
                      <?php foreach ($servicoLista as $key => $linha) { ?>
                      <?php if ($linhaCategoria->categoria == $linha->categoria) { ?>         
                        <li> 
                          <input type="checkbox" id="item<?php echo $key;?>">
                          <label for="item<?php echo $key;?>">
                            <div class="toggleIcon"></div><?php echo $linhaCategoria->categoria ?></label>
                          <div class="options">
                            <ul>
                              <?php foreach ($servicoLista as $linha) { ?>
                              <?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
                              <li>
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" name="servico[]" value="<?php echo $linha->id ?>"><?php echo $linha->nome_servico ?>                          <i class="input-helper"></i></label>
                                </div>
                              </li>
                              <?php };?>
                              <?php };?>
                            </ul>
                          </div>
                        </li>
                      <?php break; };?>
                      <?php };?>
                      <?php };?>          
                    </ul>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Observação</label>
                    <textarea class="form-control" name="observacao" cols="30" rows="5"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <button type="button" id="reservaTempo" class="btn btn-primary btn-sm">Reservar</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <div class="col-md-6 ">
              <label>Lista de reserva</label>
              <div class="form-group ajax-carregamento">
                <table class="table table-responsive table-striped">
                  <thead>
                    <tr>
                      <th>Horário</th>
                      <th>Cabeleireiro</th>
                      <th>Nome</th>
                      <th>Serviço</th>
                      <th>Observação</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  <?php 
                  if (isset($myArray)) {
                  
                  $contador = count($myArray); 
                  for ($i=0; $i < $contador; $i++) { ?>
                  <tr>
                    <td><?php $horario = date_create($myArray[$i]["tempo"]);$hora = date_format($horario,"H:i");echo $hora;?></td>
                    <td><?php echo $myArray[$i]["atendimento"]; ?></td>
                    <td><?php echo $myArray[$i]["nome"]; ?></td>
                    <td>
                      <?php 
                      if (!empty($myArray[$i]["servico"])) {
                        $virgula = true;
                        $servicos = unserialize($myArray[$i]["servico"]);
                        foreach ($servicoLista as $key => $linhaa) {
                          
                          if (in_array($linhaa->id, $servicos)) {
  
                              if($virgula){
                                  echo $linhaa->nome_servico;
                                  $virgula = false;
                              } else {
                                  echo ", " . $linhaa->nome_servico;
                              }
                              
                          }
                          
                      };
                      };
                      ?>
                    </td>
                    <td><span class='ajuda' data-tip='<?php echo $myArray[$i]["observacao"]; ?>' tabindex='1'><i class='mdi mdi-help '></i></span></td>

                  </tr>
                    <?php };};?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>