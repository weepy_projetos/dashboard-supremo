<div class="content-wrapper container-sem-menu">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="atualizado-sucesso">
    <div class="col-md-12">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Iniciou o serviço com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="erro-atualizar">
    <div class="col-md-12">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="alerta">
    <div class="col-md-12">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-alerta">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-12 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Atendimento</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
            Conteúdo de destaque do site com seu serviço ou produto.
          </p>
          <table class="table table-responsive table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Serviço</th>
                <th>Observação</th>
                <th>Horário</th>
                <th>Status</th>
                <th></th>  
              </tr>
            </thead>
            <tbody id="lista-Atendimento" class="ajax-carregamento">
            <?php foreach ($reservaLista as $key => $linha) { ?>
              <tr id="linha<?php echo $linha->id; ?>">
                <td>
                  <?php echo $linha->nome;?>
                </td>
                <td>
                <?php
                $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as $linhaa) {

                        if (in_array($linhaa->id, $servicos)) {

                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                            
                        }
                        
                    };
                  ?>
                </td>
                <td>
                  <?php echo $linha->observacao; ?>
                </td>
                <td>
                  <?php $horario = date_create($linha->horario);$hora = date_format($horario,"H:i");echo $hora;  ?>
                </td>
                <td  <?php if ($key == 0) { echo 'id="status"';}; ?>>
                  <?php echo $linha->status; ?>
                </td>
                <td>
                <?php if ($key == 0) {?>
                  <?php if ($linha->status != "Atendimento") {?>
                    <button title="Visualizar Mensagem" idobjeto="<?php echo $linha->id;?>" destino="reservaTempo/iniciarAtendimento" class="iniciar-atendimento btn btn-primary btn-rounded btn-fw">
                      <div class="centro-icone">
                        <!--<i class="mdi mdi-content-cut"></i>-->Atender
                      </div>
                    </button>
                  <?php } ?>
                  <button title="Visualizar Mensagem" idobjeto="<?php echo $linha->id;?>" class=" btn btn-info btn-rounded btn-fw">Remarcar</button>
                  <button type="button" style="display:<?php if($linha->status == "Atendimento"){echo "block";}else{echo "none";};?>" class="finalizado-atendimento deletar-modal btn btn-success btn-rounded btn-fw" idobjeto="<?php echo $linha->id;?>" msg="<?php echo $linha->nome; ?>" destino="reservatempo/finalizarAtendimento" data-toggle="modal" data-target="#FinalizadoModal">Finalizado</button>
                  <?php if ($linha->status != "Atendimento") {?>
                    <button type="button" class="alerta-modal perdeu-atendimento-oculta btn btn-warning btn-rounded btn-fw" msg='<?php echo $linha->nome; ?>' idobjeto="<?php echo $linha->id;?>" destino="reservatempo/perdeuAtendimento" data-toggle="modal" data-target="#AlertaModal">Perdeu a vez</button>
                  <?php } ?>
                <?php } ?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>