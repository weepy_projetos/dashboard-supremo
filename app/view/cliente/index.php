<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Clientes</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
          Lista de clientes cadastrado no sistema
          </p>
          <table class="table table-responsive table-striped">
            <thead>
              <tr>
                <th>Perfil</th>
                <th>Nome</th>
                <th>Cadastro</th>
                <th>Opções</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($clienteLista as $linha) { ?>
              <tr id="linha<?php echo $linha->id; ?>">
                <td>
                  <img src="<?php echo URL;?>images/cliente/<?php $img = explode('.',$linha->img); echo $img[0] . "_mini." . $img[1]; ?>" alt="image">
                </td>
                <td>
                  <?php echo $linha->nome_cliente ." ". $linha->sobrenome; ?>
                </td>
                <td>
                <?php echo $linha->tipo_cadastro; ?>
                </td>
                <td>
                <a title="Editar" href="cliente/editar/<?php echo $linha->id; ?>" class="btn btn-inverse-primary btn-rounded btn-icon">
                  <div class="centro-icone">
                    <i class="mdi mdi-lead-pencil"></i>
                  </div>
                </a>
                <button type="button" title="Desativar" class="btn btn-inverse-warning btn-rounded btn-icon" idobjeto="1">
                  <i class="mdi mdi-account-off"></i>
                </button>
                <button type="button" title="Deletar" class="deletar-modal btn btn-inverse-danger btn-rounded btn-icon"  idobjeto="<?php echo $linha->id; ?>" msg="cliente <?php echo $linha->nome_cliente ." ". $linha->sobrenome; ?>" destino="cliente/deletar/" data-toggle="modal" data-target="#deletarModal">
                  <i class="mdi mdi-delete-forever"></i>
                </button>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu cliente</h4>   
          <div class="media">
            <i class="mdi mdi-file-document-box icon-sm d-flex align-self-start mr-2"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text"><?php echo count($clienteLista); ?></p>
            </div>
          </div><div class="media">        
            <i class="mdi mdi-delete-forever icon-smd-flex align-self-start mr-2"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text"><a href="destaque/lixeira">2</a></p> 
            </div>
          </div>
          <a href="cliente/novo" class="btn btn-inverse-secondary btn-rounded btn-fw">
            Novo
          </a>
        </div>
      </div>
    </div>
  </div>
</div>