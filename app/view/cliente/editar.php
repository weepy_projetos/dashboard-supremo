<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Editar Cliente</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <?php foreach ($clienteLista as $linha) { ?>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
              <div class="form-group">
                  <label>Imagem</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?><?php if(property_exists($linha, 'img')){echo "images/clientes/".$linha->img;}else{echo'images/img-upload.png';};?>');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem">
                        </label>
                      </div>
                      <button type="button" id="editarImagem" class="btn btn-outline-primary btn-sm">Escolher</button>
                      <button type="button" id="removerImagem" class="btn btn-outline-danger btn-sm">Remover</button>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="nome" class="form-control" placeholder="Nome" value="<?php echo $linha->nome_cliente;?>">
                </div>
                <div class="form-group">
                  <label>Sobrenome</label>
                  <input type="text" name="sobrenome" class="form-control" placeholder="Sobrenome" value="<?php echo $linha->sobrenome;?>">
                </div>
                <div class="form-group">
                  <label>Aniversário</label>
                  <input type="text" name="aniversario" data-date-format="DD MMMM YYYY" class="form-control" placeholder="aniversário" value="<?php echo $linha->aniversario;?>">
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Email</label>
                  <input type="text" name="email" class="form-control" placeholder="Email" value="<?php echo $linha->email;?>">
                </div>
                <div class="form-group">
                  <label>Telefone</label>
                  <input type="text" name="telefone" class="form-control" placeholder="Telefone" value="<?php echo $linha->whatsapp;?>">
                </div>        
              </div>
            </div>
          </form>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu cliente</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado </p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por </p>
            </div>
          </div>
          <button destino="cliente/atualizar/<?php echo $id;?>" class="link-ajax-atualizar btn btn-inverse-primary btn-rounded btn-fw">
            Atualizar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>