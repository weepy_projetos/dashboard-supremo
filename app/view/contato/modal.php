<!-- modal -->
<div class="modal fade" id="visualizar-email" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Vizualizar E-mail</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5 id="data">Modal Body</h5>
                <form id="formularios">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" id="contatoNome" name="nome" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label>Assunto</label>
                                <input type="text" name="assunto" id="contatoAssunto" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Telefone</label>
                                <input type="text" name="data" id="contatoTel" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Data</label>
                                <input type="text" name="data" id="contatoData" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" id="contatoEmail" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Mensagem</label>
                                <textarea name="mensagem" class="form-control" id="contatoMsg" rows="8"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="mandar-email" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Mandar E-mail</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
            <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
                <div class="col-md-12">
                <div class="card border-0">
                    <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
                    <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Enviado com sucesso.</p>
                    <div class="d-flex">
                        <button id="bannerClose1" class="btn border-0 p-0">
                        <i class="mdi mdi-close text-white"></i>
                        </button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row ativo-msg-modal d-none" id="erro-atualizar">
                <div class="col-md-12">
                <div class="card border-0">
                    <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
                    <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao enviar, tente novamente.</p>
                    <div class="d-flex">
                        <button id="bannerClose2" class="btn border-0 p-0">
                        <i class="mdi mdi-close text-white"></i>
                        </button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
            <div class="modal-body">
                <h5 id="data">Modal Body</h5>
                <form id="formulariosContato">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Para</label>
                                <input type="text" name="email" class="form-control contatoEmail">
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Assunto</label>
                                <input type="text" name="assunto" class="form-control contatoAssunto">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="form-group">
                                <label>Mensagem</label>
                                <textarea name="mensagem" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </form>    
            </div>
            <div class="modal-footer">
                <button id="enviarEmail" class="btn btn-outline-success">Enviar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- FIM modal -->
<!-- Apagar modal -->
<div class="modal fade" id="deletarModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Deletar</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>Deseja realmente deletar o <span id="descricaodeletar"></span>?</h5>
            </div>
            <div class="modal-footer">
                <button id="linkdeletar" destino="" idobjeto="" type="button" class="link-ajaxxx btn btn-outline-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- Apagar modal FIm-->