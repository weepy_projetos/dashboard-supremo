<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Salvo com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao salvar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Novo Depoimento</h4>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="nome" class="form-control">
                </div>
                <div class="form-group">
                  <label>Profissão</label>
                  <input type="text" name="profissao" class="form-control">
                </div>
                <div class="form-group">
                  <label>Depoimento</label>
                  <textarea name="descricao" class="form-control" rows="4"></textarea>
                </div>
                <div class="form-group">
                  <label>Avaliação</label>
                  <div class="rating-widget">
                    <input type="checkbox" class="star-input" id="1"/>
                    <label class="star-input-label" for="1">1
                      <i class="mdi mdi-star"></i>
                      <i class="mdi mdi-star orange"></i>
                    </label>
                    <input type="checkbox" class="star-input" id="2"/>
                    <label class="star-input-label" for="2">2
                      <i class="mdi mdi-star"></i>
                      <i class="mdi mdi-star orange"></i>
                    </label>
                    <input type="checkbox" class="star-input" id="3"/>
                    <label class="star-input-label" for="3">3
                      <i class="mdi mdi-star"></i>
                      <i class="mdi mdi-star orange"></i>
                    </label>
                    <input type="checkbox" class="star-input" id="4"/>
                    <label class="star-input-label" for="4">4
                      <i class="mdi mdi-star"></i>
                      <i class="mdi mdi-star orange"></i>
                    </label>
                    <input type="checkbox" class="star-input" id="5"/>
                    <label class="star-input-label" for="5">5
                      <i class="mdi mdi-star"></i>
                      <i class="mdi mdi-star orange"></i>
                    </label>   
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Imagem</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?>images/usuario.png');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem" value="">
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Descrição da imagem</label>
                      <textarea name="alt" class="form-control" rows="4"></textarea>
                      <button type="button" id="editarImagem" class=" btn btn-primary btn-sm"><i
                          class="mdi mdi-upload"></i>Escolher</button>
                      <button type="button" id="removerImagem" class="btn btn-danger btn-sm"><i
                          class="mdi mdi-close"></i>Remover</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu destaque</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado </p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por </p>
            </div>
          </div>
          <button destino="depoimento/inserir" class="novo-ajax btn btn-light btn-rounded btn-fw">
            Salvar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

