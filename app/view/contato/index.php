<div class="content-wrapper container-sem-menu">
  <div class="row">
    <div class="col-md-12 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Contato</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
          Lista de mensagem recebidas.
          </p>
          <table class="table table-responsive table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Assunto</th>
                <th>Mensagem</th>
                <th>Telefone</th>
                <th>Data</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            <?php foreach ($contatoLista as $linha) { ?>
              <tr id="linha<?php echo $linha->id; ?>">
                <td>
                  <?php echo $linha->nome; ?>
                </td>
                <td class="py-1">
                <?php echo $linha->assunto; ?>
                </td>
                <td>
                <?php echo mb_strimwidth($linha->mensagem, 0, 30, "..."); ?>
                </td>
                <td>
                <?php echo $linha->telefone; ?>
                </td>
                <td>
                <?php $data = date_create("$linha->data"); $data = date_format($data,"d/m/Y"); echo $data; ?>
                </td>
                <td>
                <button title="Visualizar Mensagem" destino="<?php echo $linha->id;?>" class="ajaxcontato btn btn-inverse-success btn-rounded btn-icon"  data-toggle="modal" data-target="#visualizar-email">
                  <div class="centro-icone">
                    <i class="mdi mdi-eye "></i>
                  </div>
                </button>
                  <button title="Mandar Email" destino="<?php echo $linha->email;?>" assunto="<?php echo $linha->assunto;?>" type="button" title="Deletar" class="ajaxEmail deletar-modal btn btn-inverse-primary btn-rounded btn-icon"  data-toggle="modal" data-target="#mandar-email">
                    <i class="mdi mdi-email"></i>
                  </button>
                  <button type="button" title="Deletar" class="deletar-modal btn btn-inverse-danger btn-rounded btn-icon"  idobjeto="<?php echo $linha->id; ?>" msg="contato <?php echo $linha->nome; ?>" destino="contato/deletar/" data-toggle="modal" data-target="#deletarModal">
                    <i class="mdi mdi-delete-forever"></i>
                  </button>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>