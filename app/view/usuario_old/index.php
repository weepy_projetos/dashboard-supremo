<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar"<?php if(isset($MsgModell) && $MsgModell == 'erro'){echo 'style="display:block"';};?>>
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Página Perfil</h4>
          <p class="card-description">
            <?php //foreach ($PaginasLista as $linha) { ?>
            Basic form elements
          </p>
          <?php foreach ($usuario as $linha) { ?>
          <form class="forms-sample" id="formularioPagina">
            <div class="row">
            <div class="col-md-6 ">
                <div class="form-group">
                  <label>Informações do Perfil</label>
                  <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?>images/usuario/<?php if(property_exists($linha, 'img')){echo $linha->img;}else{echo'img-upload.png';};?>');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem" value="">
                        </label>
                      </div>
                      </div>
                      <div class="form-group">
                        <button type="button" id="editarImagem" class=" btn btn-primary btn-sm"><i
                            class="mdi mdi-upload"></i>Escolher</button>
                        <button type="button" id="removerImagem" class="btn btn-danger btn-sm"><i class="mdi mdi-close"></i>Remover</button>
                      </div>
            
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputName1">Nome</label>
                        <input type="text" name="nome" class="form-control" id="exampleInputName1" placeholder="Name"
                          value="<?php echo $linha->nome;?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Email</label>
                        <input type="text" name="email" class="form-control" id="exampleInputName1" placeholder="Name"
                          value="<?php echo $linha->email;?>">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Nível</label>
                        <input type="text" name="nivel" class="form-control" id="exampleInputName1" placeholder="Name"
                          value="<?php echo $linha->nivel;?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Segurança</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputName1">Nova senha</label>
                        <input type="password" name="nova_senha" class="form-control" id="exampleInputName1" value="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Repita nova senha</label>
                        <input type="password" name="repita_senha" class="form-control" id="exampleInputName1" value="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputName1">Senha antiga</label>
                        <input type="password" name="senha_antiga" class="form-control" id="exampleInputName1" value="">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>         
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
              <div class="card menu-atualizar">
                <div class="card-body">
                  <h4 class="card-title">Menu</h4>
                  <div class="media">
                    <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
                    <div class="media-body d-flex align-self-center">
                      <p class="card-text">Atualizado </p>
                    </div>
                  </div>
                  <div class="media">
                      <span class="d-flex align-self-start mr-3">
                          <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile" style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
                      </span>
                      <div class="media-body d-flex align-self-center">
                        <p class="card-text">Editado por </p>
                      </div>
                    </div>
                  <button type="submit" name="atualizar" form="formularioPagina"  class="btn btn-primary btn-rounded btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Atualizar
                  </button>
                </div>
              </div>
            </div>