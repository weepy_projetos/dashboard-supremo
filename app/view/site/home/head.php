<!doctype html>

<html lang="pt-BR">

<head>
	<!-- Meta -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $paginaLista[0]->meta_titulo;?></title>
	<meta name="description" content="">
    <meta name="keywords" content="<?php echo $paginaLista[0]->meta_descricao;?>">
	<meta name="author" content="Supremo Digital">
	
	<!-- Favicon e atalhos-->
	<link rel="apple-touch-icon" sizes="120x120" href="site3/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="site3/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="site3/favicon/favicon-16x16.png">
	<link rel="manifest" href="site3/favicon/site.webmanifest">
	<link rel="mask-icon" href="site3/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#ffc40d">
	<meta name="theme-color" content="#be9342">

	<!-- Google Fonts css-->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700%7CMontserrat:400,500,600,700,800,900" rel="stylesheet">
	<!-- Bootstrap css -->
	<link href="site3/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<!-- Font css -->
	<link rel="stylesheet" type="text/css" href="site3/font/flaticon.css">
	<!-- Font Awesome & Flat icon css-->
	<link href="site3/css/font-awesome.min.css" rel="stylesheet" media="screen">
	<!-- Carousel css -->
	<link rel="stylesheet" href="site3/css/owl.carousel.css">
	<!-- Slick nav css -->
	<link rel="stylesheet" href="site3/css/slicknav.css">
	<!-- Main custom css -->
	<link href="site3/css/custom.css" rel="stylesheet" media="screen" >
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>