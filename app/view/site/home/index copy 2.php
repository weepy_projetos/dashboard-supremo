<body data-spy="scroll" data-target="#navigation" data-offset="71">
	<!-- CARREGAMENTO -->
	<div class="preloader">
		<div class="browser-screen-loading-content">
			<div class="hair_light">
				<div class="head"></div>
				<div class="body"></div>
				<div class="footer"></div>
			</div>
		</div>
	</div>
	<!-- CARREGAMENTO FIM -->

	<!-- HEADER-->
	<header>
		<nav id="main-nav" class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<!-- Logo -->
					<a class="navbar-brand" href="#">
						<img src="site3/images/small-logo.png" alt="Barbershop" />
					</a>
					<!-- Logo fim -->

					<!-- Responsive Menu -->
					<div class="navbar-toggle">
					</div>
					<!-- Responsive Menu fim -->
				</div>

				<div id="responsive-menu"></div>

				<!-- NAV -->
				<div class="navbar-collapse collapse" id="navigation">
					<ul class="nav navbar-nav navbar-right main-navigation" id="main-menu">
						<li class="active"><a href="#home">Início</a></li>
						<li><a href="#about">Sobre</a></li>
						<li><a href="#service">Serviços</a></li>
						<li><a href="#ourteam">Equipe</a></li>
						<li><a href="#gallery">Galeria</a></li>
						<li><a href="#pricing">Preços</a></li>
						<li><a href="#testimonial">Depoimento</a></li>
						<li><a href="#contact">Contato</a></li>
						<li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>
					</ul>
				</div>
				<!-- NAV FIM-->
			</div>
		</nav>
	</header>
	<!-- HEADER FIM-->

	<!-- BANNER -->
	<section class="banner" id="home" style="background: url(images/slider/<?php echo $sliderLista[0]->img; ?>) no-repeat center center;">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="header-banner">
						<img src="site3/images/logo.png" alt="Logo" />
						<p class="senha"><?php echo $sliderLista[0]->titulo; ?></p>
						<h3><?php echo $sliderLista[0]->descricao; ?></h3>
					</div>
				</div>
				<div class="col-md-5 no-padd">
					<div class="banner-info-single">
						<div  class="">
							<div class="">
								<ul class="menu-servico reserva-index">
									<li>
										<input type="checkbox" id="item33">
										<label class="evento-fechar-acordiao" for="item33">
											<div class="lista-profissional">
											<span class="mask"><img src="<?php echo URL;?>images/usuario/sammer.jpg" alt="admin" title="admin"></span>
											<span>Ícaro</span>
											</div>
											<div>
											<span class="btn-reserva">Reservar com ícaro</span>
											</div>
											<div>
											<span class="fila-aguadando" title="Aguardando">11</span>
											</div>
										</label>
										<div class="options">
											<ul>
												<li>
													<div class="input-group">
														<label for="">Selecione os serviços</label>
														<ul class="menu-servico fechar-acordiao">
															<?php $i = 0; foreach ($categoriaLista as $linhaCategoria) { ?>
																<?php foreach ($servicoLista as $key => $linha) { ?>
																	<?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
																		<?php //if($i % 2 == 0){echo "<div class='row'>";}?>
																		<li>
																			<input type="checkbox" id="item<?php echo $key; ?>">
																			<label for="item<?php echo $key; ?>">
																				<div class="toggleIcon"></div><?php echo $linhaCategoria->categoria ?>
																			</label>
																			<div class="options">
																				<ul>
																					<?php foreach ($servicoLista as $linha) { ?>
																						<?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
																							<li>
																							<label class="check-flex">
																								<input type="checkbox" class="option-input checkbox" name="servico[]" value="<?php echo $linha->id ?>" valor="15">
																								<?php echo $linha->nome_servico ?><span class="valor-servico">R$15</span>
																							</label>
																							</li>
																						<?php }; ?>
																					<?php }; ?>
																				</ul>
																			</div>
																		</li>
																		<?php if($i % 2 == 1){echo "<span class='quebra-linha'></span>";}?>
																	<?php break;
																	}; ?>
																<?php }; ?>
															<?php $i = ++$i; }; ?>
														</ul>
													</div>
													<div class="input-group">
														<label for="">Observação</label>
														<textarea class="form-control" name="observacao" id="reservaObs" cols="50" rows="3"></textarea>
													</div>
													<div class="input-group">
														<button data-toggle="modal" data-target="#login-cadastro-modal" class="btn-reserva">Marcar</button>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<li>
										<input type="checkbox" id="item2">
										<label class="evento-fechar-acordiao" for="item2">
											<div class="lista-profissional">
											<span class="mask"><img src="<?php echo URL;?>images/usuario/sammer.jpg" alt="admin" title="admin"></span>
											<span>admin</span>
											</div>
											<div>
												<span class="btn-reserva">Reservar com admin</span>
											</div>
											<div>
											<span class="fila-aguadando" title="Aguardando">9</span>
											</div>
										</label>
										<div class="options">
											<ul>
												<li>
													<div class="input-group">
														<label for="">Servico</label>
														<ul class="menu-servico fechar-acordiao">
															<?php foreach ($categoriaLista as $linhaCategoria) { ?>
																<?php foreach ($servicoLista as $key => $linha) { ?>
																	<?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
																		<li>
																			<input type="checkbox" id="item<?php echo $key; ?>">
																			<label for="item<?php echo $key; ?>">
																				<div class="toggleIcon"></div><?php echo $linhaCategoria->categoria ?>
																			</label>
																			<div class="options">
																				<ul>
																					<?php foreach ($servicoLista as $linha) { ?>
																						<?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
																							<li>
																								<label class="check-flex">
																									<input type="checkbox" class="option-input checkbox">
																									Checkbox <span class="valor-servico">R$15</span>
																								</label>
																								<div class="form-check form-check-success">
																									<label class="form-check-label">
																										<input type="checkbox" class="form-check-input" name="servico[]" value="<?php echo $linha->id ?>"><?php echo $linha->nome_servico ?> <i class="input-helper"></i></label>
																								</div>
																							</li>
																						<?php }; ?>
																					<?php }; ?>
																				</ul>
																			</div>
																		</li>
																	<?php break;
																	}; ?>
																<?php }; ?>
															<?php }; ?>
														</ul>
													</div>
													<div class="input-group">
														<label for="">Observação</label>
														<textarea class="form-control" name="observacao" id="reservaObs" cols="50" rows="3"></textarea>
													</div>
													<div class="input-group">
														<button data-toggle="modal" data-target="#login-cadastro-modal" class="btn-reserva">Marcar</button>
													</div>
												</li>
											</ul>
										</div>
									</li>
									<!--<li>
										<input type="checkbox" id="item12">
										<label for="item12">
											<div class="toggleIcon"></div>Barba
										</label>
										<div class="options">
											<ul>
												<li>
													<div class="form-check form-check-success">
														<label class="form-check-label">
															<input type="checkbox" class="form-check-input" name="servico[]" value="6">Corte Adulto <i class="input-helper"></i></label>
													</div>
												</li>
												<li>
													<div class="form-check form-check-success">
														<label class="form-check-label">
															<input type="checkbox" class="form-check-input" name="servico[]" value="9">Barba Tradicional <i class="input-helper"></i></label>
													</div>
												</li>
											</ul>
										</div>
									</li>-->
								</ul>
								</li>
							</div>
						</div>
						<table class="tabela" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>Cabeleireiro</th>
									<th>Cliente</th>
									<th>Fila</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($reservaLista as $linha) { ?>
									<tr>
										<td>
											<img src="<?php echo URL . 'images/usuario/' . $linha->img; ?>" alt="<?php echo $linha->nome; ?>" title="<?php echo $linha->nome; ?>">
											<span><?php echo $linha->nome; ?></span>
										</td>
										<td><?php echo $linha->nome_cliente; ?></td>
										<td><?php echo $linha->tipo_cadastro; ?></td>
										<td>
											<?php if ($linha->estatus == "Atendimento") { ?>
												<div class="circle">
													<i class="flaticon-scissor"></i>
												</div>
											<?php } else { ?>
												<span class="fila-aguadando" title="<?php echo $linha->estatus; ?>"></span>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php if (!isset($_SESSION['idCliente'])) { ?>
							<button <?php echo 'data-toggle="modal" data-target="#login-modal"'; ?> class="btn-reserva">Entrar na fila</button>
						<?php } else { ?>
							<button <?php echo 'id="fazer-reserva-cliente"'; ?> class="btn-reserva">Sair da fila</button>
						<?php }; ?>
					</div>
				</div>
			</div>
		</div>

		<div class="banner-icon"><i class="flaticon-hair-salon-situation"></i></div>
	</section>
	<!-- BANNER FIM -->

	<!-- SOBRE -->
	<section class="aboutus" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="main-title">
						<h2><?php echo $paginaLista[0]->titulo; ?></h2>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="about-image">
						<div class="about-img-single">
							<img src="site3/images/about-1.jpg" alt="" />
						</div>

						<div class="about-img-single">
							<img src="site3/images/about-2.jpg" alt="" />
						</div>

						<div class="about-img-single">
							<img src="site3/images/about-3.jpg" alt="" />
						</div>
					</div>

					<div class="about-desc">
						<p><?php echo $paginaLista[0]->descricao; ?>.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- SOBRE FIM -->

	<!--SERVIÇOS -->
	<section class="service" id="service">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="main-title">
						<h2>Nossos Serviços</h2>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-theme testimonial-slider" id="servico-slider">
						<?php foreach ($servicoLista as $linha) { ?>
							<?php $date = new DateTime($linha->tempo); ?>
							<div class="item">
								<div class="service-single">
									<div class="icon-box-outer">
										<figure>
											<img src="<?php echo ("site3/images/$linha->img"); ?>" alt="" />
										</figure>
									</div>
									<h3><?php echo $linha->nome_servico; ?></h3>
									<p><?php echo $linha->descricao; ?></p>
									<h5>Tempo : <?php echo $date->format('i'); ?> Min</h5>
								</div>
							</div>
						<?php }; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- SERVIÇOS FIM -->

	<!-- EQUIPE-->
	<section class="ourteam background" id="ourteam">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="main-title">
						<h2>EQUIPE ESPECIALISTA</h2>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-theme testimonial-slider" id="equipe-slider">
						<?php $qtdEquipe = count($equipeLista); ?>
						<?php foreach ($equipeLista as $linha) { ?>
							<!--<div class="<?php if ($qtdEquipe == 4) {
												echo "col-md-3 ";
											} else {
												echo "col-md-4 ";
											}
											if ($qtdEquipe == 1) {
												echo "col-md-push-4";
											} elseif ($qtdEquipe == 2) {
												echo "col-md-push-2";
											}; ?>"> -->
							<div class="item">
								<div class="team-box">
									<figure>
										<img src="<?php echo "images/usuario/" . $linha->img; ?>" alt="equipe" />
									</figure>

									<div class="team-entry">
										<p>Especialista</p>
										<h3><?php echo $linha->nome; ?></h3>
										<div class="team-social">
											<a href="#"><i class="fa fa-facebook"></i></a>
											<a href="#"><i class="fa fa-instagram"></i></a>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</div>
									</div>
								</div>
							</div>
						<?php }; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- EQUIPE FIM -->



	<!-- GALERIA -->
	<section class="gallery" id="gallery">
		<div class="container-fluid">
			<div class="row no-pad">
				<div class="col-md-12">
					<div class="main-title">
						<h2>Nossa Galeria</h2>
					</div>
				</div>
			</div>

			<div class="row no-pad">
				<div id="instagram"></div>
			</div>
		</div>
	</section>
	<!-- GALERIA FIM -->

	<!-- PREÇOS -->
	<div class="pricing" id="pricing">
		<div class="container">
			<div class="row no-pad">
				<div class="col-md-6">
					<div class="store-image">
						<img src="site3/images/store.jpg" alt="" />
					</div>
				</div>

				<div class="col-md-6">
					<div class="pricing-logo">
						<img src="site3/images/logo.png" alt="" />
					</div>
					<?php foreach ($servicoLista as $linha) { ?>
						<div class="price-list">
							<div class="price-item"><?php echo $linha->nome_servico; ?></div>
							<div class="price-line"></div>
							<div class="price-amount">R$<?php echo $linha->valor; ?></div>
						</div>
					<?php }; ?>
				</div>
			</div>
		</div>
	</div>
	<!-- Pricing Section ends -->

	<!-- Testimonial section starts -->
	<section class="testimonial" id="testimonial">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="main-title">
						<h2>Depoimentos</h2>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="owl-carousel owl-theme testimonial-slider" id="testimonial-slider">
						<?php foreach ($depoimentoLista as $linha) { ?>
							<div class="item">
								<div class="testimonial-single">
									<div class="icon-box">
										<img src="images/depoimento/<?php echo $linha->img; ?>" alt="Perfil de cliente">
										<!--<i class="fa fa-quote-left"></i>-->
									</div>
									<p><?php echo $linha->depoimento; ?>.</p>
									<div class="star-rating">
										<span class="fa fa-star" data-rating="1"></span>
										<span class="fa fa-star" data-rating="2"></span>
										<span class="fa fa-star" data-rating="3"></span>
										<span class="fa fa-star" data-rating="4"></span>
										<span class="fa fa-star-o" data-rating="5"></span>
									</div>
									<h3>- <?php echo $linha->nome; ?></h3>
								</div>
							</div>
						<?php }; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial section ends -->

	<!-- Contactus section starts -->
	<section class="contactus" id="contact">
		<div class="container-fluid">
			<div class="row no-pad">
				<div class="col-md-4">
					<div class="contact-form">
						<h2 class="contact-form-title">Nos mande mensagem</h2>

						<form>
							<div class="form-group">
								<input type="text" class="form-control" placeholder="Nome" />
							</div>

							<div class="form-group">
								<input type="text" class="form-control" placeholder="Email" />
							</div>

							<div class="form-group">
								<textarea class="form-control" rows="5" placeholder="Sua mensagem"></textarea>
							</div>

							<div class="form-group">
								<input type="submit" class="btn-contact" value="Enviar" />
							</div>
						</form>
					</div>
				</div>

				<div class="col-md-8">
					<div class="google-map">
						<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14767.723033070624!2d70.75848835!3d22.280612599999998!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1504611295336" height="600" style="border:0;width:100%;" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Contactus section ends -->

	<script>
		// Barber: 		https://www.instagram.com/goldtimegestao/
		// Embelleze: 	https://www.instagram.com/embellezeferraz/
		var url = "https://www.instagram.com/goldtimegestao/?__a=1";

		var http = new XMLHttpRequest();
		var inst;
		var bloco = document.getElementById("instagram");

		http.open("GET", url);
		http.responseType = "json";
		http.addEventListener("readystatechange", function() {
			if (http.readyState == 4) {
				if (http.status == 200) {
					inst = http.response.graphql.user.edge_owner_to_timeline_media.edges;
					for (let i = 0; i < inst.length; i++) {
						bloco.innerHTML += `<div class="col-md-3 col-sm-4 col-xs-6">
							<div class="gallery-box">
								<figure>
					<img src="${inst[i].node.thumbnail_resources[2].src}" alt="KiBeleza" style="width:100%">
								</figure>
								<div class="gallery-overlay">
									<div class="gallery-info">
										<p>Link Instagram</p>
									</div>
								</div>
							</div>
						</div>`;
					}
				}
			}
		});

		http.send();
	</script>

	<!-- MENU CELULAR -->
	<nav id="menu-celular-fixo">
		<ul class="list-wrap" id="menu-celular">
			<a href="#home">
				<li data-color="linear-gradient(to top, #09203f 0%, #537895 100%)" title="Home" class="">
					<i class="flaticon-queue"></i>
				</li>
			</a>
			<a href="#service">
				<li data-color="#ff6b81" title="Profile" class="">
					<i class="flaticon-scissor"></i>
				</li>
			</a>
			<a href="#gallery">
				<li data-color="#7bed9f" title="Get a beer!" class="">
					<i class="flaticon-gallery"></i>
				</li>
			</a>
			<a <?php if (!isset($_SESSION['idCliente'])) {
					echo 'data-toggle="modal" data-target="#login-modal" href="#"';
				} else {
					echo 'href="painelcliente/perfil"';
				}; ?>>
				<li data-color="#70a1ff" title="Files" class="">
					<?php if (isset($_SESSION['idCliente'])) { ?>
						<img src="<?php echo URL . '/images/cliente/' . $_SESSION['imgCliente']; ?>">
					<?php } else { ?>
						<i class="flaticon-user"></i>
					<?php } ?>
				</li>
			</a>
		</ul>
	</nav>
	<!-- FIM MENU CELULAR -->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="footer-social">
						<a href="#">Facebook</a>
						<a href="#">Twitter</a>
						<a href="#">Instagram</a>
					</div>

					<div class="site-info">
						<p>Copyright &copy; Gold Time Gestão.Todos os direitos reservados. Dev.<a href="https://supremodigital.com.br/" target="_blank">Supremo Digital</a></p>
						<img src="site3/images/assinatura-gold-time-gestao.svg" alt="Gold Time Gestão" />
					</div>


				</div>
			</div>
		</div>
	</footer>

	<!-- MODAL LOGIN CADASTRO RESERVA -->
	<div class="modal fade" id="login-cadastro-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="">
				<div class="wrapper">
					<!--<ul class="steps">
						<li class="is-active">Step 1</li>
						<li>Step 2</li>
						<li>Step 3</li>
					</ul>-->
					<form class="form-wrapper form-reserva">
						<span class="mensagem" style="display: none"></span>
						<fieldset class="section is-active">
							<h3>Login</h3>
							<div class="row cf">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
								</div>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" name="senha" class="form-control input-lg" placeholder="Senha" required>
								</div>
								<button id="login-cliente-reserva" class="button" id="cadastro-cliente">
									<span>Acessar</span>
								</button>

								<div class="login-help">
									<a href="#" class="avancar-form">Cadastra-se</a> - <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#recuperar-modal">Esqueceu a senha?</a>
								</div>
							</div>
						</fieldset>
						<fieldset class="section">
							<h3 class="criarconta">Criar Conta</h3>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="text" name="email_cadastro" id="emailCadastroo" class="form-control input-lg" placeholder="Email">
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
								<input type="text" name="celular" class="form-control input-lg" placeholder="Whatsapp">
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" name="nome" maxlength="20" class="form-control input-lg" placeholder="Nome" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" id="senha-cadastro" name="senha_cadastro" class="form-control input-lg" placeholder="Senha" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" name="repita-senha" class="form-control input-lg" placeholder="Repita a senha" required>
							</div>
							<button class="button" id="cadastro-cliente-reserva">
								<span>Cadastrar</span>
							</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!--<div class="modal fade" id="login-cadastro-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="">
				<div class="wrapper">
					<ul class="steps">
						<li class="is-active">Step 1</li>
						<li>Step 2</li>
						<li>Step 3</li>
					</ul>
					<form class="form-wrapper form-reserva">
						<span class="mensagem" style="display: none"></span>
						<fieldset class="section is-active">
							<h3>Reserva</h3>
							<div class="input-group">
								<label for="">Servico</label>
								<ul class="menu-servico">
									<?php foreach ($categoriaLista as $linhaCategoria) { ?>
										<?php foreach ($servicoLista as $key => $linha) { ?>
											<?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
												<li>
													<input type="checkbox" id="item<?php echo $key; ?>">
													<label for="item<?php echo $key; ?>">
														<div class="toggleIcon"></div><?php echo $linhaCategoria->categoria ?>
													</label>
													<div class="options">
														<ul>
															<?php foreach ($servicoLista as $linha) { ?>
																<?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
																	<li>
																		<div class="form-check form-check-success">
																			<label class="form-check-label">
																				<input type="checkbox" class="form-check-input" name="servico[]" value="<?php echo $linha->id ?>"><?php echo $linha->nome_servico ?> <i class="input-helper"></i></label>
																		</div>
																	</li>
																<?php }; ?>
															<?php }; ?>
														</ul>
													</div>
												</li>
											<?php break;
											}; ?>
										<?php }; ?>
									<?php }; ?>
								</ul>
							</div>
							<?php $equipe = count($equipeLista);
							if ($equipe > 1) { ?>
								<div class="input-group">
									<label for="">Profissional</label>
									<div class="foto-reserva" style="display:none">
										<figure>
											<img src="<?php echo URL; ?>images/usuario/usuario.jpg" id="profissionalFoto" alt="image">
										</figure>
									</div>
									<select class="form-control" name="profissional" id="ProfissionalEscolha" disabled>
										<option value="ambos">Profissional</option>
									</select>
								</div>
							<?php } ?>
							<div class="input-group">
								<label for="">Observação</label>
								<textarea class="form-control" name="observacao" id="reservaObs" cols="30" rows="3" disabled></textarea>
							</div>
							<button class="button avancar-form">
								<span>Reservar</span>
							</button>
							<button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
						</fieldset>
						<fieldset class="section">
							<h3>Login</h3>
							<div class="row cf">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
								</div>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock"></i></span>
									<input type="password" name="senha" class="form-control input-lg" placeholder="Senha" required>
								</div>
								<button id="login-cliente-reserva" class="button" id="cadastro-cliente">
									<span>Acessar</span>
								</button>

								<div class="login-help">
									<a href="#" class="avancar-form">Cadastra-se</a> - <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#recuperar-modal">Esqueceu a senha?</a>
								</div>
							</div>
						</fieldset>
						<fieldset class="section">
							<h3 class="criarconta">Criar Conta</h3>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="text" name="email_cadastro" id="emailCadastroo" class="form-control input-lg" placeholder="Email">
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
								<input type="text" name="celular" class="form-control input-lg" placeholder="Whatsapp">
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" name="nome" maxlength="20" class="form-control input-lg" placeholder="Nome" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" id="senha-cadastro" name="senha_cadastro" class="form-control input-lg" placeholder="Senha" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								<input type="password" name="repita-senha" class="form-control input-lg" placeholder="Repita a senha" required>
							</div>
							<button class="button" id="cadastro-cliente-reserva">
								<span>Cadastrar</span>
							</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>-->

	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="loginmodal-container" style="background: url(<?php $a=array(1,2,3,4,5);$random_keys=array_rand($a,3); echo URL.'images/bg-login'.$a[$random_keys[0]].'.png'; ?>)">
				<object data="<?php echo URL;?>images/gold-time-gestao-branco.svg" width="120"></object>
				<h3>Login</h3>
				<form id="form-login">
					<span class="mensagem" style="display: none"></span>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" name="senha" class="form-control input-lg" placeholder="Senha" required>
					</div>
					<button id="login-cliente" class="button" id="cadastro-cliente">
						<span>Acessar</span>
					</button>
				</form>
				<div class="login-help">
					<a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#conta-modal">Cadastra-se</a> - <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#recuperar-modal">Esqueceu a senha?</a>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="conta-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="loginmodal-container" style="background: url(<?php $a=array(1,2,3,4,5);$random_keys=array_rand($a,3); echo URL.'images/bg-login'.$a[$random_keys[0]].'.png'; ?>)">
				<h3 class="teste">Cadastro</h3>
				<form id="form-cadastro-cliente">
					<span class="mensagem" style="display: none"></span>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input type="text" name="email_cadastro" id="email" class="form-control input-lg" placeholder="Email">
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
						<input type="text" name="celular" class="form-control input-lg" placeholder="Whatsapp">
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" name="nome" class="form-control input-lg" placeholder="Nome" required>
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" id="senha-cadastro" name="senha_cadastro" class="form-control input-lg" placeholder="Senha" required>
					</div>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-lock"></i></span>
						<input type="password" name="repita-senha" class="form-control input-lg" placeholder="Repita a senha" required>
					</div>
					<button class="button" id="cadastro-cliente">
						<span>Cadastrar</span>
					</button>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="recuperar-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="loginmodal-container">
				<h3>Recuperar Senha</h3>
				<form id="form-recuperar-cliente">
					<span class="mensagem" style="display: none"></span>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-user"></i></span>
						<input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
					</div>
					<button class="button" id="recuperar-cliente">
						<span>Recuperar</span>
					</button>
				</form>
			</div>
		</div>
	</div>

	<!-- Jquery Library File -->
	<script src="site3/js/jquery-1.12.4.min.js"></script>
	<!-- SmoothScroll -->
	<script src="site3/js/SmoothScroll.js"></script>
	<!-- Bootstrap js file -->
	<script src="site3/js/bootstrap.min.js"></script>
	<!-- Slick Nav js file -->
	<script src="site3/js/jquery.slicknav.js"></script>
	<!-- Owl Carousel js file -->
	<script src="site3/js/owl.carousel.js"></script>
	<!-- Main Custom js file -->
	<script src="site3/js/function.js"></script>
	<!-- Main Custom js file -->
	<script src="site3/js/script.js"></script>

</body>

</html>