<!-- BANNER ESTATICO -->
<?php echo $sliderLista[0]->img; ?>
<?php echo $sliderLista[0]->titulo; ?>
<?php echo $sliderLista[0]->descricao; ?>

<!-- PAGINA -->
<?php echo $paginaLista[0]->titulo; ?>
<?php echo $paginaLista[0]->descricao; ?>

<!-- RESERVA TABELA -->
<?php foreach ($reservaLista as $linha) { ?>
	<tr>
		<td><img src="<?php echo URL . 'images/cliente/' . $linha->img; ?>" alt="cliente perfil"></td>
		<td><?php echo $linha->nome_cliente; ?></td>
		<td><?php echo $linha->tipo_cadastro; ?></td>
		<td>
			<?php if ($linha->estatus == "Atendimento") { ?>
				<div class="circle">
					<i class="flaticon-scissor"></i>
				</div>
			<?php } else { ?>
				<img src="images/usuario.png" alt="" title="<?php echo $linha->estatus; ?>">
			<?php } ?>
		</td>
	</tr>
<?php } ?>

<button <?php if (!isset($_SESSION['idCliente'])) {
			echo 'data-toggle="modal" data-target="#login-modal"';
		} else {
			echo 'id="fazer-reserva-cliente"';
		}; ?> class="btn-reserva">Reservar</button>

<!-- SERVICO LISTA -->
<?php foreach ($servicoLista as $linha) { ?>
	<?php $date = new DateTime($linha->tempo); ?>
	<?php echo $linha->nome_servico; ?>
	<?php echo $linha->descricao; ?>
	<?php echo $date->format('i'); ?>
<?php }; ?>

<!-- EQUIPE LISTA -->
<?php foreach ($equipeLista as $linha) { ?>
	<?php echo "images/usuario/" . $linha->img; ?>
	<?php echo $linha->nome; ?>
<?php }; ?>

<!-- SERVICO LISTA -->
<?php foreach ($servicoLista as $linha) { ?>
	<?php echo $linha->nome_servico; ?>
	<?php echo $linha->valor; ?>
<?php }; ?>


<!-- Testimonial section starts -->
<section class="testimonial" id="testimonial">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="main-title">
					<h2>Depoimentos</h2>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel owl-theme testimonial-slider" id="testimonial-slider">
					<?php foreach ($depoimentoLista as $linha) { ?>
						<div class="item">
							<div class="testimonial-single">
								<div class="icon-box">
									<img src="images/depoimento/<?php echo $linha->img; ?>" alt="Perfil de cliente">
									<!--<i class="fa fa-quote-left"></i>-->
								</div>
								<p><?php echo $linha->depoimento; ?>.</p>
								<div class="star-rating">
									<span class="fa fa-star" data-rating="1"></span>
									<span class="fa fa-star" data-rating="2"></span>
									<span class="fa fa-star" data-rating="3"></span>
									<span class="fa fa-star" data-rating="4"></span>
									<span class="fa fa-star-o" data-rating="5"></span>
								</div>
								<h3>- <?php echo $linha->nome; ?></h3>
							</div>
						</div>
					<?php }; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Testimonial section ends -->

<!-- Contactus section starts -->
<section class="contactus" id="contact">
	<div class="container-fluid">
		<div class="row no-pad">
			<div class="col-md-4">
				<div class="contact-form">
					<h2 class="contact-form-title">Nos mande mensagem</h2>

					<form>
						<div class="form-group">
							<input type="text" class="form-control" placeholder="Nome" />
						</div>

						<div class="form-group">
							<input type="text" class="form-control" placeholder="Email" />
						</div>

						<div class="form-group">
							<textarea class="form-control" rows="5" placeholder="Sua mensagem"></textarea>
						</div>

						<div class="form-group">
							<input type="submit" class="btn-contact" value="Enviar" />
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-8">
				<div class="google-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14767.723033070624!2d70.75848835!3d22.280612599999998!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1504611295336" height="600" style="border:0;width:100%;" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Contactus section ends -->

<script>
	var url = "https://www.instagram.com/supremodigital/?__a=1";
	var http = new XMLHttpRequest();
	var inst;
	var bloco = document.getElementById("instagram");

	http.open("GET", url);
	http.responseType = "json";
	http.addEventListener("readystatechange", function() {
		if (http.readyState == 4) {
			if (http.status == 200) {
				inst = http.response.graphql.user.edge_owner_to_timeline_media.edges;
				for (let i = 0; i < inst.length; i++) {
					bloco.innerHTML += `<div class="col-md-3 col-sm-4 col-xs-6">
							<div class="gallery-box">
								<figure>
					<img src="${inst[i].node.thumbnail_resources[2].src}" alt="KiBeleza" style="width:100%">
								</figure>
								<div class="gallery-overlay">
									<div class="gallery-info">
										<p>Link Instagram</p>
									</div>
								</div>
							</div>
						</div>`;
				}
			}
		}
	});

	http.send();
</script>

<!-- MENU CELULAR -->
<nav id="menu-celular-fixo">
	<ul class="list-wrap" id="menu-celular">
		<a href="#home">
			<li data-color="linear-gradient(to top, #09203f 0%, #537895 100%)" title="Home" class="">
				<i class="flaticon-queue"></i>
			</li>
		</a>
		<a href="#service">
			<li data-color="#ff6b81" title="Profile" class="">
				<i class="flaticon-scissor"></i>
			</li>
		</a>
		<a href="#gallery">
			<li data-color="#7bed9f" title="Get a beer!" class="">
				<i class="flaticon-gallery"></i>
			</li>
		</a>
		<a <?php if (!isset($_SESSION['idCliente'])) {
				echo 'data-toggle="modal" data-target="#login-modal" href="#"';
			} else {
				echo 'href="painelcliente/perfil"';
			}; ?>>
			<li data-color="#70a1ff" title="Files" class="">
				<?php if (isset($_SESSION['idCliente'])) { ?>
					<img src="<?php echo URL . '/images/cliente/' . $_SESSION['imgCliente']; ?>">
				<?php } else { ?>
					<i class="flaticon-user"></i>
				<?php } ?>
			</li>
		</a>
	</ul>
</nav>
<!-- FIM MENU CELULAR -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="footer-social">
					<a href="#">Facebook</a>
					<a href="#">Twitter</a>
					<a href="#">Instagram</a>
					<a href="#">Pinterest</a>
					<a href="#">Googleplus</a>
					<a href="#">Linkedin</a>
				</div>

				<div class="site-info">
					<p>Copyright &copy; Untitled. All rights reserved. Design By <a href="https://awaikenthemes.com/" target="_blank">Awaiken Theme</a> Images <a href="https://unsplash.com/" target="_blank">Unsplash</a>, <a href="https://pixabay.com/" target="_blank">Pixabay</a>, <a href="http://www.freepik.com" target="_blank">Freepik</a>, Icon <a href="https://www.flaticon.com/" target="_blank">Flaticon</a></p>
				</div>

				<div class="footer-menu">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Services</a></li>
						<li><a href="#">Team</a></li>
						<li><a href="#">Gallery</a></li>
						<li><a href="#">Pricing</a></li>
						<li><a href="#">Testimonial</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="loginmodal-container">
			<h3>Login</h3>
			<form id="form-login">
				<span class="mensagem teste" style="display: none"></span>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input type="password" name="senha" class="form-control input-lg" placeholder="Senha" required>
				</div>
				<button id="login-cliente" class="button" id="cadastro-cliente">
					<span>Acessar</span>
				</button>
			</form>
			<div class="login-help">
				<a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#conta-modal">Cadastra-se</a> - <a href="#" data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target="#recuperar-modal">Esqueceu a senha?</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="conta-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="loginmodal-container">
			<h3 class="teste">Cadastro</h3>
			<form id="form-cadastro-cliente">
				<span class="mensagem" style="display: none"></span>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
					<input type="text" name="email" id="emailCadastro" class="form-control input-lg" placeholder="Email">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
					<input type="text" name="celular" class="form-control input-lg" placeholder="Whatsapp">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="nome" class="form-control input-lg" placeholder="Nome" required>
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input type="password" id="senha-cadastro" name="senha" class="form-control input-lg" placeholder="Senha" required>
				</div>
				<div class="input-group" 6>
					<span class="input-group-addon"><i class="fa fa-lock"></i></span>
					<input type="password" name="repita-senha" class="form-control input-lg" placeholder="Repita a senha" required>
				</div>
				<button class="button" id="cadastro-cliente">
					<span>Cadastrar</span>
				</button>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="recuperar-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="loginmodal-container">
			<h3>Recuperar Senha</h3>
			<form id="form-recuperar-cliente">
				<span class="mensagem" style="display: none"></span>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user"></i></span>
					<input type="text" name="email" class="form-control input-lg" placeholder="Email" required>
				</div>
				<button class="button" id="recuperar-cliente">
					<span>Recuperar</span>
				</button>
			</form>
		</div>
	</div>
</div>


<!-- Jquery Library File -->
<script src="site3/js/jquery-1.12.4.min.js"></script>
<!-- SmoothScroll -->
<script src="site3/js/SmoothScroll.js"></script>
<!-- Bootstrap js file -->
<script src="site3/js/bootstrap.min.js"></script>
<!-- Slick Nav js file -->
<script src="site3/js/jquery.slicknav.js"></script>
<!-- Owl Carousel js file -->
<script src="site3/js/owl.carousel.js"></script>
<!-- Main Custom js file -->
<script src="site3/js/function.js"></script>
<!-- Main Custom js file -->
<script src="site3/js/script.js"></script>
</body>

</html>