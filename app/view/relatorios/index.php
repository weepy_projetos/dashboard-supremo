<div class="content-wrapper">
  <div class="row">
    <div class="col-md-12 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Contato</h4>
          <p class="card-description">
            Conteúdo de destaque do site com seu serviço ou produto.
          </p>
          <table class="table table-responsive table-striped">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Assunto</th>
                <th>Mensagem</th>
                <th>Telefone</th>
                <th>Data</th>
                <th></th>  
              </tr>
            </thead>
            <tbody>
            <?php foreach ($relatoriosLista as $linha) { ?> <!--LISTA SERVICO-->
              
				<?php 
														
					$arrayServicos = unserialize($linha->servico);
					$servicoTotal[] = array();
					foreach ($arrayServicos as $servico) {
						array_push($servicoTotal, $servico);
					}
				?>
                
            <?php } ?>
            <?php 
				foreach ($servicoLista as $servico){
					@$totalDia += count( array_keys( $servicoTotal , $servico->id ) ) * $servico->valor;                  
				}
				echo "TOTAL DIA R$" .$totalDia. "<br/>";
			?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>