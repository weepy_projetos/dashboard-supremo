<link rel="stylesheet" href="<?php echo URL; ?>css/goldtime.css">
<?php
  use App\Controller\NotificacaoController;
  use App\Model\Nivel;
  use App\Model\Usuario;

  $notificacaoLista = new NotificacaoController();
  $notificacaoLista = $notificacaoLista->notificacao();

  $nivel = strtolower($_SESSION['nivelUsuario']);
  $nivelLista = new Nivel();
  $nivelLista = $nivelLista->listaNivel("$nivel");

  foreach ( $nivelLista as $linha) {
    $controles[] = $linha->controler;
  }

  $usuario = new Usuario();
  $usuario = $usuario->lista($_SESSION['idUsuario']);
?>

<body>
<div class="preload"></div>
  <div class="container-scroller">
    <!-- INICIO HEADER -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="navbar-brand-wrapper d-flex justify-content-center">
            <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
            <a class="navbar-brand brand-logo" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>images/logo.png" alt="logo" /></a>
            <a class="navbar-brand brand-logo-mini" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>images/logo-mini.png" alt="logo" /></a>
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi mdi-sort-variant"></span>
            </button>
            </div>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item dropdown mr-4">
                <a class="noti nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center notification-dropdown" id="notificationDropdown" href="#" data-toggle="dropdown">
                <i class="mdi mdi-bell mx-0"></i>
                <span class="count" style="display:none"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal dropdown-header">Notificações</p>
                <div id="notificacao-ajax" qtd="<?php echo count($notificacaoLista); ?>">
                <?php foreach ($notificacaoLista as $linha) { ?>

                <a class="dropdown-item">
                  <div class="item-thumbnail">
                    <div class="item-icon bg-success">
                      <i class="mdi mdi-information mx-0"></i>
                    </div>
                  </div>
                  <div class="item-content">
                    <h6 class="font-weight-normal"><?php echo $linha->categoria; ?></h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      <?php echo $linha->descricao; ?>
                    </p>
                  </div>
                </a>              
                <?php } ?>
                </div>
                  <audio id="audio" src="<?php echo URL; ?>som/notificacao.mp3"></audio>
                </div>
            </li>
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                <img src="<?php echo URL; ?>images/usuario/<?php $img = explode('.',$usuario[0]->img); echo $img[0] . "_mini." . $img[1]; ?>" alt="profile" /> 
                <span class="nav-profile-name"><?php echo $usuario[0]->nome?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                <a href="<?php echo URL; ?>profissional/perfil" class="dropdown-item">
                    <i class="mdi mdi-account-outline text-primary"></i>
                    Perfil
                </a>
                <a class="dropdown-item" href="<?php echo URL; ?>login/sairUsuario">
                    <i class="mdi mdi-power text-primary"></i>
                    Sair
                </a>
                </div>
              </li>
          </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- FIM HEADER -->
    
    <!-- INICIO Container principal -->
    <div class="container-fluid page-body-wrapper">

      <!-- INICIO SIDEBAR -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#reserva-chegada" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-account-multiple-outline menu-icon"></i>
              <span class="menu-title">Fila de espera</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="reserva-chegada">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>profissional/reserva">Fila</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>profissional/atendimento">Atendimento</a></li>
              </ul>
            </div>
          </li>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#reserva" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-calendar-check menu-icon"></i>
              <span class="menu-title">Agendamento</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="reserva">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>reservatempo">Agenda</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>reservatempo/atendimento">Atendimento</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>notificacao/inserir">Marcar Reserva</a></li>
              </ul>
            </div>
          </li>-->
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>profissional/servico">
              <i class="mdi mdi-content-cut menu-icon menu-icon"></i>
              <span class="menu-title">Serviço</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>profissional/diastrabalho">
              <i class="mdi mdi-calendar menu-icon"></i>
              <span class="menu-title">Escala</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>profissional/cliente">
              <i class="mdi mdi-account-outline menu-icon"></i>
              <span class="menu-title">Cliente</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>login/sairUsuario">
              <i class="mdi mdi-power menu-icon"></i>
              <span class="menu-title">Sair</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- FIM SIDEBAR -->
      <div id="main" class="main-panel">
        <div class="carregar-pagina">
          <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
          <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
          </svg>
        </div>
