<link rel="stylesheet" href="<?php echo URL; ?>css/goldtime.css">
<?php
  use App\Controller\NotificacaoController;
  use App\Model\Nivel;
  use App\Model\Usuario;

  $notificacaoLista = new NotificacaoController();
  $notificacaoLista = $notificacaoLista->notificacao();

  $nivel = strtolower($_SESSION['nivelUsuario']);
  $nivelLista = new Nivel();
  $nivelLista = $nivelLista->listaNivel("$nivel");

  foreach ( $nivelLista as $linha) {
    $controles[] = $linha->controler;
  }

  $usuario = new Usuario();
  $usuario = $usuario->lista($_SESSION['idUsuario']);

?>

<body>
<div class="preload"></div>
  <div class="container-scroller">
    <!-- INICIO HEADER -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="navbar-brand-wrapper d-flex justify-content-center">
            <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
            <a class="navbar-brand brand-logo" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>images/logo.png" alt="logo" /></a>
            <a class="navbar-brand brand-logo-mini" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>images/logo-mini.png" alt="logo" /></a>
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi mdi-sort-variant"></span>
            </button>
            </div>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <!--<"ul class="navbar-nav mr-lg-4 w-100">
                <li class="nav-item nav-search d-none d-lg-block w-100">
                    <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="search">
                        <i class="mdi mdi-magnify"></i>
                        </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Search now" aria-label="search" aria-describedby="search">
                    </div>
                </li>
                </ul>-->
            <ul class="navbar-nav navbar-nav-right">
            <!--<li class="nav-item dropdown mr-1">
                    <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
                    <i class="mdi mdi-message-text mx-0"></i>
                    <span class="count"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="messageDropdown">
                    <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                    <a class="dropdown-item">
                        <div class="item-thumbnail">
                            <img src="images/faces/face1.jpg" alt="image" class="profile-pic">
                        </div>
                        <div class="item-content flex-grow">
                        <h6 class="ellipsis font-weight-normal">David Grey
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                            The meeting is cancelled
                        </p>
                        </div>
                    </a>
                    <a class="dropdown-item">
                        <div class="item-thumbnail">
                            <img src="images/faces/face1.jpg" alt="image" class="profile-pic">
                        </div>
                        <div class="item-content flex-grow">
                        <h6 class="ellipsis font-weight-normal">Tim Cook
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                            New product launch
                        </p>
                        </div>
                    </a>
                    <a class="dropdown-item">
                        <div class="item-thumbnail">
                            <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                        </div>
                        <div class="item-content flex-grow">
                        <h6 class="ellipsis font-weight-normal"> Johnson
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                            Upcoming board meeting
                        </p>
                        </div>
                    </a>
                    </div>
                </li>-->
            <li class="nav-item dropdown mr-4">
                <a class="noti nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center notification-dropdown" id="notificationDropdown" href="#" data-toggle="dropdown">
                <i class="mdi mdi-bell mx-0"></i>
                <span class="count" style="display:none"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal dropdown-header">Notificações</p>
                <div id="notificacao-ajax" qtd="<?php echo count($notificacaoLista); ?>">
                <?php foreach ($notificacaoLista as $linha) { ?>

                <a class="dropdown-item">
                  <div class="item-thumbnail">
                    <div class="item-icon bg-success">
                      <i class="mdi mdi-information mx-0"></i>
                    </div>
                  </div>
                  <div class="item-content">
                    <h6 class="font-weight-normal"><?php echo $linha->categoria; ?></h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      <?php echo $linha->descricao; ?>
                    </p>
                  </div>
                </a>              
                  
                <?php } ?>
                
                </div>
                <audio id="audio" src="<?php echo URL; ?>som/notificacao.mp3"></audio>
                <!--<a class="dropdown-item">
                    <div class="item-thumbnail">
                    <div class="item-icon bg-success">
                        <i class="mdi mdi-information mx-0"></i>
                    </div>
                    </div>
                    <div class="item-content">
                    <h6 class="font-weight-normal">Categoria</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                        Nome
                    </p>
                    </div>
                </a>
                <a class="dropdown-item">
                    <div class="item-thumbnail">
                    <div class="item-icon bg-warning">
                        <i class="mdi mdi-settings mx-0"></i>
                    </div>
                    </div>
                    <div class="item-content">
                    <h6 class="font-weight-normal">Categoria</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                        Nome
                    </p>
                    </div>
                </a>
                <a class="dropdown-item">
                    <div class="item-thumbnail">
                    <div class="item-icon bg-info">
                        <i class="mdi mdi-account-box mx-0"></i>
                    </div>
                    </div>
                    <div class="item-content">
                    <h6 class="font-weight-normal">Novo contato</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                        nome
                    </p>
                    </div>
                </a>-->
                </div>
            </li>
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                <img src="<?php echo URL; ?>images/usuario/<?php $img = explode('.',$usuario[0]->img); echo $img[0] . "_mini." . $img[1]; ?>" alt="profile" /> 
                <span class="nav-profile-name"><?php echo $usuario[0]->nome?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                <a href="<?php echo URL; ?>perfil" class="dropdown-item">
                    <i class="mdi mdi-account-outline text-primary"></i>
                    Perfil
                </a>
                <a class="dropdown-item" href="<?php echo URL; ?>login/sairUsuario">
                    <i class="mdi mdi-power text-primary"></i>
                    Sair
                </a>
                </div>
            </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- FIM HEADER -->
    
    <!-- INICIO Container principal -->
    <div class="container-fluid page-body-wrapper">

      <!-- INICIO SIDEBAR -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <?php //if (in_array("PainelController", $controles)) { ?>
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>painel">
              <i class="mdi mdi-home menu-icon"></i>
              <span class="menu-title">Painel</span>
            </a>
          </li>-->
          <?php //}?>
          <?php if (in_array("PaginasController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#paginas" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-newspaper menu-icon"></i>
              <span class="menu-title">Página</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="paginas">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>paginas/inicio">Início</a></li>
                <?php if (in_array("SliderController", $controles)) { ?>
                <li class="nav-item"> <a link="sobre" class="nav-link" href="<?php echo URL; ?>slider">Banner</a></li>
                <?php }?>
              </ul>
            </div>
          </li>
          <?php }?>
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>blog">     
              <i class="mdi mdi-star-outline menu-icon"></i>
              <span class="menu-title">Blog</span>
            </a>
          </li>
          <li class="nav-item">
            <button link="sobre" class="link-ajax nav-link">        
              <i class="mdi mdi-star-outline menu-icon"></i>
              <span class="menu-title">Destaque AJAX</span>
            </button>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>destaque">     
              <i class="mdi mdi-content-cut menu-icon"></i>
              <span class="menu-title">Serviços</span>
            </a>
          </li>-->
          <?php if (in_array("ReservaController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#reserva-chegada" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-account-multiple-outline menu-icon"></i>
              <span class="menu-title">Fila de espera</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="reserva-chegada">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>reserva">Fila</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>reserva/atendimento">Atendimento</a></li>
              </ul>
            </div>
          </li>
          <?php }?>
          <?php if (in_array("ReservaTempoController", $controles)) { ?>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#reserva" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-calendar-check menu-icon"></i>
              <span class="menu-title">Agendamento</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="reserva">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>reservatempo">Agenda</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>reservatempo/atendimento">Atendimento</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>notificacao/inserir">Marcar Reserva</a></li>
              </ul>
            </div>
          </li>-->
          <?php }?>
          <?php if (in_array("ServicoHomemController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>servicohomem">
              <i class="mdi mdi-content-cut menu-icon menu-icon"></i>
              <span class="menu-title">Serviço</span>
            </a>
          </li>
          <?php }?>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#servico" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-content-cut menu-icon"></i>
              <span class="menu-title">Serviço</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="servico">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>servicohomem">Homem</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>servicomulher">Mulher</a></li>
              </ul>
            </div>
          </li>--> 
          <?php if (in_array("EquipeController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>equipe">
              <i class="mdi mdi-account-card-details menu-icon"></i>
              <span class="menu-title">Profissional</span>
            </a>
          </li>
          <?php } ?>
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>notificacao/inserir">
              <i class="mdi mdi-account-multiple-outline menu-icon"></i>
              <span class="menu-title">Marcar Reserva</span>
            </a>
          </li>-->
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>galeria">
              <i class="mdi mdi-file-image menu-icon"></i>
              <span class="menu-title">Galeria</span>
            </a>
          </li>-->
          <?php if (in_array("DepoimentoController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>depoimento">
              <i class="mdi mdi-comment-account-outline menu-icon"></i>
              <span class="menu-title">Depoimento</span>
            </a>
          </li>
          <?php } ?>
          <?php if (in_array("ClienteController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>cliente">
              <i class="mdi mdi-account-outline menu-icon"></i>
              <span class="menu-title">Cliente</span>
            </a>
          </li>
          <?php } ?>
          <?php if (in_array("ContatoController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>contato">
              <i class="mdi mdi-email-outline menu-icon"></i>
              <span class="menu-title">Contato</span>
            </a>
          </li>
          <?php } ?>
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>cliente-parcerias">
              <i class="mdi mdi-thumb-up-outline menu-icon"></i>
              <span class="menu-title">Clientes | Parcerias</span>
            </a>
          </li>-->
          <?php //if (in_array("RelatoriosController", $controles)) { ?>
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>relatorios">
              <i class="mdi mdi-chart-line menu-icon"></i>
              <span class="menu-title">Relatórios</span>
            </a>
          </li>-->
          <?php //} ?>
          <?php if (in_array("InformacaoController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>informacao">
              <i class="mdi mdi-bulletin-board menu-icon"></i>
              <span class="menu-title">Informação de contato</span>
            </a>
          </li>
          <?php } ?>
          <?php if (in_array("UsuarioController", $controles)) { ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>usuario">
              <i class="mdi mdi-account-circle menu-icon"></i>
              <span class="menu-title">Usuário</span>
            </a>
          </li>
          <?php } ?>
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#usuario" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-account-outline menu-icon"></i>
              <span class="menu-title">Usuário</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="usuario">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>usuario">Usuários</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>add-usuario">Adicionar usuário</a></li>
                <li class="nav-item"> <a class="nav-link" href="pages/ui-features/buttons.html">Privilégios</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#config" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-settings menu-icon"></i>
              <span class="menu-title">Configuração</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="config">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="area-de-atendimento">Área de atendimento</a></li>
                <li class="nav-item"> <a class="nav-link" href="gatilho">Gatilhos</a></li>
                <li class="nav-item"> <a class="nav-link" href="<?php echo URL; ?>informacao">Informação de contato</a></li>
                <li class="nav-item"> <a class="nav-link" href="#">Visual</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="faturas">
              <i class="mdi mdi-currency-usd menu-icon"></i>
              <span class="menu-title">Faturas</span>
            </a>
          </li>-->
          <!--<li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#weepy" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-help-circle-outline menu-icon"></i>
              <span class="menu-title">Suporte</span>
              <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="weepy">
              <ul class="nav flex-column sub-menu">
                <li class="nav-item"> <a class="nav-link" href="suporte-tecnico">Técnico</a></li>
                <li class="nav-item"> <a class="nav-link" href="suporte-financeiro">Financeiro</a></li>
              </ul>
            </div>
          </li>-->
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>login/sairUsuario">
              <i class="mdi mdi-power menu-icon"></i>
              <span class="menu-title">Sair</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- FIM SIDEBAR -->
      <div id="main" class="main-panel">
        <div class="carregar-pagina">
          <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
          <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
          </svg>
        </div>
