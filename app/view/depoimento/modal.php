<!-- modal -->
<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Solicitar depoimento</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5 id="data">Modal Body</h5>
                <form id="formularios">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" name="nome" class="form-control" value="">
                            </div>
                            <div class="form-group">
                                <label>Assunto</label>
                                <input type="text" name="assunto" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Data</label>
                                <input type="text" name="data" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="form-group">
                                <label>Mensagem</label>
                                <textarea name="mensagem" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="linkobjeto" class="btn btn-success">Enviar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- FIM modal -->