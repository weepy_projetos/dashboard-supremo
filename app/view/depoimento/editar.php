<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Editar Depoimento</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <?php foreach ($depoimentoLista as $linha) { ?>
          <form id="formularios">
            <div class="row">
            <div class="col-md-6 ">
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="nome" value="<?php echo $linha->nome;?>" class="form-control">
                </div>
                <div class="form-group">
                  <label>Profissão</label>
                  <input type="text" name="profissao" value="<?php echo $linha->profissao;?>" class="form-control">
                </div>
                <div class="form-group">
                  <label>Depoimento</label>
                  <textarea name="depoimento" class="form-control" rows="4"><?php echo $linha->depoimento;?></textarea>
                </div>
                <div class="form-group">
                  <label>Avaliação</label>
                  <div class="stars">
                    <input type="radio" name="avaliacao" value="1" class="star-1" id="star-1" <?php if($linha->avaliacao == 1){echo 'checked';};?>>
                    <label class="star-1" for="star-1">1</label>
                    <input type="radio" name="avaliacao" value="2" class="star-2" id="star-2" <?php if($linha->avaliacao == 2){echo 'checked';};?>>
                    <label class="star-2" for="star-2">2</label>
                    <input type="radio" name="avaliacao" value="3" class="star-3" id="star-3" <?php if($linha->avaliacao == 3){echo 'checked';};?>>
                    <label class="star-3" for="star-3">3</label>
                    <input type="radio" name="avaliacao" value="4" class="star-4" id="star-4" <?php if($linha->avaliacao == 4){echo 'checked';};?>>
                    <label class="star-4" for="star-4">4</label>
                    <input type="radio" name="avaliacao" value="5" class="star-5" id="star-5" <?php if($linha->avaliacao == 5){echo 'checked';};?>>
                    <label class="star-5" for="star-5">5</label>
                    <span></span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Imagem</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?><?php if(property_exists($linha, 'img')){echo "images/depoimento/".$linha->img;}else{echo'images/img-upload.png';};?>');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem">
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Descrição da imagem</label>
                      <textarea name="alt" class="form-control" rows="4"><?php echo $linha->alt;?></textarea>
                      <button type="button" id="editarImagem" class=" btn btn-primary btn-sm"><i
                          class="mdi mdi-upload"></i>Escolher</button>
                      <button type="button" id="removerImagem" class="btn btn-danger btn-sm"><i
                          class="mdi mdi-close"></i>Remover</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php } ?>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu depoimento</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado </p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por </p>
            </div>
          </div>
          <button destino="depoimento/atualizar/<?php echo $id;?>" class="link-ajax-atualizar btn btn-inverse-primary btn-rounded btn-fw">
            Atualizar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>