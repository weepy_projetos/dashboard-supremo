<div class="content-wrapper container-sem-menu">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="atualizado-sucesso">
    <div class="col-md-12">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Iniciou o serviço com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="erro-atualizar">
    <div class="col-md-12">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="alerta">
    <div class="col-md-12">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-alerta">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body dashboard-tabs p-0">
                  
                  <div class="tab-content py-0 px-0">
                    <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview-tab">
                      <div class="d-flex flex-wrap justify-content-xl-between">                    
                        <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                          <i class="mdi mdi-currency-usd mr-3 icon-lg text-danger"></i>
                          <div class="d-flex flex-column justify-content-around">
                            <small class="mb-1 text-muted">Lucro do dia</small>
							<h5 class="mr-2 mb-0">
							<?php foreach ($relatoriosLista as $linha) { ?> <!--LISTA SERVICO-->
								<?php 
									$arrayServicos = unserialize($linha->servico);
									$servicoTotal[] = array();
									foreach ($arrayServicos as $servico) {
									array_push($servicoTotal, $servico);
								}?>
							<?php } ?>
							<?php 
								foreach ($servicoListaR as $servico){
									@$totalDia += count( array_keys( $servicoTotal , $servico->id ) ) * $servico->valor;                  
								}
								echo $totalDia;
							?>
							</h5> 
                          </div>
                        </div>
                        <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                          <i class="mdi mdi-currency-usd mr-3 icon-lg text-success"></i>
                          <div class="d-flex flex-column justify-content-around">
                            <small class="mb-1 text-muted">Lucro do mês</small>
                            <h5 class="mr-2 mb-0">9833550</h5>
                          </div>
                        </div>
                        <div class="d-flex border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                          <i class="mdi mdi-account-settings mr-3 icon-lg text-warning"></i>
                          <div class="d-flex flex-column justify-content-around">
                            <small class="mb-1 text-muted">Clientes</small>
                            <h5 class="mr-2 mb-0">2233783</h5>
                          </div>
                        </div>
                        <div class="d-flex py-3 border-md-right flex-grow-1 align-items-center justify-content-center p-3 item">
                          <i class="mdi mdi-account-off mr-3 icon-lg text-danger"></i>
                          <div class="d-flex flex-column justify-content-around">
                            <small class="mb-1 text-muted">Desistência</small>
                            <h5 class="mr-2 mb-0">3497843</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-12 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Atendimento</h4>
          <span class="ajuda" data-tip="Na área: Fila de Atendimento, é possivél visualizar o status e gerenciar o atendimento." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
            Fila de espera... A lista abaixo apresenta a relação de cliente que realizaram a solicitação para entrar na fila.
          </p> 
          <button type="button" id="reservachegada" class="btn btn-primary btn-sm">Cancelar Reservas</button>
          <table class="table table-responsive table-striped ajax-carregamento">
            <thead>
              <tr>
                <th>Nome</th>
                <th>Serviço</th>
                <th>Observação</th>
                <th>Estatus</th>
                <th></th>  
              </tr>
            </thead>
            <tbody id="lista-Atendimento" >
            <?php foreach ($reservaLista as $key => $linha) { ?>
              <tr id="linha<?php echo $linha->id; ?>">
                <td>
                  <?php echo $linha->nome_cliente;?>
                </td>
                <td>
                <?php
                $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as $linhaa) {
                        if (in_array($linhaa->id, $servicos)) {
                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                        }   
                    };
                  ?>
                </td>
                <td>
                  <?php echo $linha->observacao; ?>
                </td>
                <td  <?php if ($key == 0) { echo "id='estatus$linha->id'" ;}; ?>>
                <label class="badge badge-info"><?php echo $linha->estatus; ?></label>
                </td>
                <td>
                <?php if ($key == 0) {?>
                  <?php if ($linha->estatus != "Atendimento") {?>
                    <button title="Visualizar Mensagem" idobjeto="<?php echo $linha->id;?>"  destino="reserva/iniciarAtendimento" class="iniciar-atendimento btn btn-primary btn-rounded btn-fw">
                      <div class="centro-icone">
                        <!--<i class="mdi mdi-content-cut"></i>-->Atender
                      </div>
                    </button>
                  <?php } ?>
                  <button type="button" style="display:<?php if($linha->estatus == "Atendimento"){echo "block";}else{echo "none";};?>" class="finalizado-atendimento deletar-modal btn btn-success btn-rounded btn-fw" idobjeto="<?php echo $linha->id;?>" msg="<?php echo $linha->nome_cliente; ?>" destino="reserva/finalizarAtendimento" data-toggle="modal" data-target="#FinalizadoModal">Finalizado</button>
                  <?php if ($linha->estatus != "Atendimento") {?>
                    <button type="button" class="alerta-modal  perdeu-atendimento-oculta btn btn-warning btn-rounded btn-fw" msg='<?php echo $linha->nome_cliente; ?>' idobjeto="<?php echo $linha->id;?>" destino="reserva/perdeuAtendimento" data-toggle="modal" data-target="#AlertaModal">Perdeu</button>
                  <?php } ?>
                <?php } ?>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>