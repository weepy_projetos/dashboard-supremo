<div class="content-wrapper container-sem-menu">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Reservado com sucesso</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao reservar</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title msg">Atendimento por chegada</h4>
          <span class="ajuda" data-tip="É uma cliente novo? Marque que sim e informe o nome do cliente, agora se não for um cliente novo, basta informa o nome e selecionar. Após escolha o profissional e o(s) serviço(s) que seu cliente deseja. Automaticamente ele entrará na fila de espera." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
			  Faça o atendimento presencial escolhendo se é um cliente novo ou não, e após escolha o profissional e o(s) serviço(s) que seu cliente deseja. Automaticamente ele entrará na fila de espera.
          </p>
          <div class="row">
            <div class="col-md-6 ">
            <form id="formularios">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Cliente sem cadastro?</label>
                      <button type="button" class="novo-cliente btn btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off">
                      <div class="handle"></div>
                      <input type="checkbox" name="cliente_sem_cadastro" value="1" hidden="">
                    </button>
                  </div>
                  <div class="form-group novo-input" style="display:none">
                    <label>Nome do cliente</label>
                    <input type="text" name="nome" class="form-control">
                  </div>
                  <div class="form-group pesquisa-input">
                    <label>Pesquisar Cliente</label>
                    <div class="input-group">
                      <input type="text" name="nome-pesquisa" class="form-control" placeholder="Digite o nome">
                      <input type="text" name="id" hidden>
                      <div class="input-group-append">
                        <button id="consultar-nome" class="btn btn-sm btn-primary" type="button"><i class="mdi mdi-account-search icon-md"></i></button>
                      </div>
                    </div>
                    <ul id="lista-nome">
                    </ul>
                  </div>
                  <?php if ($_SESSION['nivelUsuario'] != 'cabeleireiro') { ?>
                  <div class="form-group">
                    <label>Profissional</label>
                    <select class="form-control" name="profissional" id="ProfissionalEscolha">
                      <option value="ambos" imagem="<?php echo URL . 'images/usuario/usuario.jpg'?>">Ambos</option>
                      <?php foreach ($profissionalLista as $profissional) { ?>
                        <option value="<?php echo $profissional->id;?>" imagem="<?php echo URL . 'images/usuario/'.$profissional->img;?>"><?php echo $profissional->nome;?></option>
                      <?php } ?>
                      </select>
                  </div>
                  <?php } ?>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Serviço</label>
                    <ul class="menu-servico">
                      <?php foreach ($categoriaLista as $linhaCategoria) { ?>
                      <?php foreach ($servicoLista as $key => $linha) { ?>
                      <?php if ($linhaCategoria->categoria == $linha->categoria) { ?>         
                        <li> 
                          <input type="checkbox" id="item<?php echo $key;?>">
                          <label for="item<?php echo $key;?>">
                            <div class="toggleIcon"></div><?php echo $linhaCategoria->categoria ?></label>
                          <div class="options">
                            <ul>
                              <?php foreach ($servicoLista as $linha) { ?>
                              <?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
                              <li>
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" name="servico[]" value="<?php echo $linha->id ?>"><?php echo $linha->nome_servico ?>                          <i class="input-helper"></i></label>
                                </div>
                              </li>
                              <?php };?>
                              <?php };?>
                            </ul>
                          </div>
                        </li>
                      <?php break; };?>
                      <?php };?>
                      <?php };?>          
                    </ul>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label>Observação</label>
                    <textarea class="form-control" name="observacao" cols="30" rows="5"></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <button type="button" id="reservachegada" class="btn btn-primary btn-sm">Reservar</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
            <div class="col-md-6 ">
            <label>Fila de espera</label>
              <div class="row ajax-carregamento">
                <table class="table table-responsive table-striped">
                  <thead>
                    <tr>
                      <th>Cabeleireiro</th>
                      <th>Nome</th>
                      <th>Serviço</th>
                      <th>Observação</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php 
                   foreach ($reservaLista as $linha) { ?>
                    <tr>
                    <td><?php echo $linha->nome;?></td>
                    <td><?php echo $linha->nome_cliente;?></td>
                    <td>
                      <?php 
                        $virgula = true;
                        $servicos = unserialize($linha->servico);
                        foreach ($servicoLista as $key => $linhaa) {
                          
                          if (in_array($linhaa->id, $servicos)) {
  
                              if($virgula){
                                  echo $linhaa->nome_servico;
                                  $virgula = false;
                              } else {
                                  echo ", " . $linhaa->nome_servico;
                              }
                              
                          }
                        }
                      ?> 
                    </td>
                    <td><span class='ajuda' data-tip='<?php echo $linha->observacao;?>' tabindex='1'><i class='mdi mdi-help '></i></span></td>
                    </tr>
                   <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>