<!-- Apagar modal -->
<div class="modal fade" id="FinalizadoModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Serviço finalizado</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>Confimar finalização do servico do cliente <span id="descricaodeletar"></span>?</h5>
            </div>
            <div class="modal-footer">
                <button id="linkdeletar"  type="button" class="finalizar-atendimento-ajax btn btn-outline-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="AlertaModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Próximo da reserva</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>Confimar cancelamento do serviço do cliente <span id="descricao-alerta"></span>?</h5>
            </div>
            <div class="modal-footer">
                <button id="link-alerta" destino="" idobjeto="" type="button" class="perdeu-atendimento btn btn-outline-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- Apagar modal FIm-->