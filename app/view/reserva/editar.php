<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Editar destaque</h4>
          <?php foreach ($destaqueLista as $linha) { ?>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label for="exampleInputName1">Título</label>
                  <input type="text" name="titulo" class="form-control" id="exampleInputName1" placeholder="Name"
                    value="<?php echo $linha->titulo;?>">
                </div>
                <div class="form-group">
                  <label>Descrição</label>
                  <textarea name="descricao" class="form-control" rows="4"><?php echo $linha->descricao;?></textarea>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Imagem</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?><?php if(property_exists($linha, 'img')){echo "images/destaque/".$linha->img;}else{echo'images/img-upload.png';};?>');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem">
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Descrição da imagem</label>
                      <textarea name="alt" class="form-control" rows="4"><?php echo $linha->alt;?></textarea>
                      <button type="button" id="editarImagem" class=" btn btn-primary btn-sm"><i
                          class="mdi mdi-upload"></i>Escolher</button>
                      <button type="button" id="removerImagem" class="btn btn-danger btn-sm"><i
                          class="mdi mdi-close"></i>Remover</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php } ?>
        </div>
      </div>
      
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu destaque</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado </p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por </p>
            </div>
          </div>
          <button destino="destaque/atualizar/<?php echo $id;?>" class="link-ajax-atualizar btn btn-primary btn-rounded btn-fw">
            Atualizar
          </button>
        </div>
      </div>
    </div>
    
  </div>
  
</div>