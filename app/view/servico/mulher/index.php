<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar"
    <?php if(isset($MsgModell) && $MsgModell == 'erro'){echo 'style="display:block"';};?>>
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Serviços para Mulher</h4>
          <p class="card-description">
            Conteúdo de destaque do site com seu serviço ou produto.
          </p>
          <table class="table table-responsive table-striped">
            <thead>
              <tr>
                <th>Serviço</th>
                <th>Imagem</th>
                <th>Descrição</th>
                <th>Valor</th>
                <th>Opções</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($ServicoMulherLista as $linha) { ?>
              <tr id="linha<?php echo $linha->id; ?>">
                <td>
                  <?php echo $linha->nome_servico; ?>
                </td>
                <td class="py-1">
                  <img src="//localhost/weepy/dashboard/admin/images/servico/homem/<?php $img = explode('.',$linha->img); echo $img[0] . "_mini." . $img[1]; ?>" alt="image">
                </td>
                <td>
                  <?php echo mb_strimwidth($linha->descricao, 0, 30, "..."); ?>
                </td>
                <td>
                  R$<?php echo $linha->valor; ?>
                </td>
                <td>
                <a title="Editar" href="servicohomem/editar/<?php echo $linha->id; ?>" class="btn btn-inverse-primary btn-rounded btn-icon">
                  <div class="centro-icone">
                    <i class="mdi mdi-lead-pencil"></i>
                  </div>
                </a>
                  <button type="button" title="Deletar" class="deletar-modal btn btn-inverse-danger btn-rounded btn-icon"  idobjeto="<?php echo $linha->id; ?>" msg="destaque <?php echo $linha->nome_servico; ?>" destino="servicohomem/deletar/" data-toggle="modal" data-target="#deletarModal">
                    <i class="mdi mdi-delete-forever"></i>
                  </button>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu Serviço</h4>   
          <div class="media">
            <i class="mdi mdi-file-document-box icon-sm d-flex align-self-start mr-2"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text"><?php echo count($ServicoMulherLista); ?></p>
            </div>
          </div><div class="media">        
            <i class="mdi mdi-delete-forever icon-smd-flex align-self-start mr-2"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text"><a href="destaque/lixeira">2</a></p> 
            </div>
          </div>
          <a href="servicomulher/novo" class="btn btn-light btn-rounded btn-fw">
            Novo
          </a>
        </div>
      </div>
    </div>