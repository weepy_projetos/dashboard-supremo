<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM -->
  <div class="row">
    <div class="col-md-9 grid-margin">
    <form id="formularios">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Serviços</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
            Selecione os serviços que efetua.
          </p>
            <div class="row">
              <div class="col-md-12 ">
                  <div class="row">
                    <?php  
                   
                    $servicos = unserialize($equipeLista[0]->servicos);

                    foreach ($categorias as $key => $categorias) {
                    ?>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label><?php echo $categorias;?></label>
                        <div class="checkboxes-wrap">
                          <div class="checkboxes-options">
                            <a href="#" class="check-all btn btn-outline-success btn-fw btn-sm">Todos</a> | <a href="#" class="check-none btn btn-outline-warning btn-fw btn-sm">Remover todos</a>
                          </div>
                          <div class="checkboxes">
                          <?php $servico = count($servicoLista);?>
                          <?php if($servico == 0){?>
                            <p>Nenhum serviço registrado</p>
                            <a class="btn btn-primary btn-rounded btn-sm" href="<?php echo URL;?>servicohomem">Adicionar</a>
                          <?php };?>
                          <?php foreach ($servicoLista as $linha) {?>
                            <?php if($linha->categoria == $categorias){ ?>
                            <div class="form-check form-check-success">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" name="<?php echo 'servico[]';?>" value="<?php echo $linha->id;?>" <?php if ($servicos != false){if (in_array($linha->id, $servicos)) { echo "checked";}};?>><?php echo $linha->nome_servico;?>
                              </label>
                            </div>
                            <?php };?>
                          <?php };?>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php };?>
                  </div>
              </div>
            </div>       
        </div>
      </div>
      </form> 
    </div>
    
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title msg">Menu membro</h4>
            <div class="media">
              <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
              <div class="media-body d-flex align-self-center">
                <p class="card-text">Atualizado </p>
              </div>
            </div>
            <div class="media">
              <span class="d-flex align-self-start mr-3">
                <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile" style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
              </span>
              <div class="media-body d-flex align-self-center">
                  <p class="card-text">Editado por </p>
              </div>
            </div>
            <button destino="profissional/inserir" class="novo-ajax btn btn-inverse-primary btn-rounded btn-fw">
              Atualizar
            </button>
          </div>
        </div>
      </div>