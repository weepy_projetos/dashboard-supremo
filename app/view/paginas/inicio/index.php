
        <div class="content-wrapper">
          <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
            <div class="col-md-9">
              <div class="card border-0">
                <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
                  <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
                  <div class="d-flex">
                    <button id="bannerClose1" class="btn border-0 p-0">
                      <i class="mdi mdi-close text-white"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row ativo-msg-modal d-none" id="erro-atualizar">
            <div class="col-md-9">
              <div class="card border-0">
                <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
                  <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
                  <div class="d-flex">
                    <button id="bannerClose2" class="btn border-0 p-0">
                      <i class="mdi mdi-close text-white"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9 grid-margin">
              <div class="card grid-margin">
                <div class="card-body">
                  <h4 class="card-title">Página <?php echo $diretorioAtual;?></h4>
                  <p class="card-description">
                  </p>
                  <?php foreach ($PaginasLista as $linha) { ?>
                  <form class="forms-sample" id="myForm" enctype="multipart/form-data">
                    <div class="row">
                      <div class="col-md-6 ">
                        <div class="form-group">
                          <label for="exampleInputName1">Título</label>
                          <input type="text" name="titulo" class="form-control" id="exampleInputName1" placeholder="Name" value="<?php echo $linha->titulo;?>">
                        </div>
                        <div class="form-group">
                          <label>Descrição</label>
                          <textarea name="descricao" class="form-control" rows="8"><?php echo $linha->descricao;?></textarea>
                        </div>
                      </div>
                      <div class="col-md-6 ">
                        <div class="form-group">
                          <label>Imagem</label>
                          <div class="row">
                          <div class="col-md-6">
                              <div class="profile" style="background-image: url('<?php echo URL; ?>images/<?php if(property_exists($linha, 'img')){echo $linha->img;}else{echo'img-upload.png';};?>');">
                                <label class="edit">
                                  <span><i class="mdi mdi-upload"></i></span>
                                  <input type="file" size="32" name="imagem" id="inputImagem" value="">
                                </label>
                              </div>
                              <button type="button" id="editarImagem" class="btn btn-outline-primary btn-sm">Escolher</button>
                              <button type="button" id="removerImagem" class="btn btn-outline-danger btn-sm">Remover</button>
                          </div>
                          <div class="col-md-6">
                            <label>Descrição da imagem</label>
                            <textarea name="alt" class="form-control" rows="4" ><?php echo $linha->alt;?></textarea>
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>   
              <div class="card grid-margin">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <h4 class="card-title">Descrição de pesquisa</h4>
                      <span class="ajuda" data-tip="Tem direito a 2 páginas por exemplo: página principal, página serviço, página contato, página sobre nós." tabindex="1"><i class="mdi mdi-help "></i></span>
                      <div class="form-group">
                        <label>Título</label>
                        <input type="text" name="meta_titulo" value="<?php echo $linha->meta_titulo;?>" id="input-metatag-titulo" class="form-control form-control-lg" placeholder="Username" aria-label="Username" maxlength="70">
                        <span class="caracter"><span class="contadorCaracter">0</span> / 70</span>
                      </div>
                      <div class="form-group">
                        <label>Descrição</label>
                        <textarea class="form-control" name="meta_descricao" id="textarea-metatag-descricao" rows="4" maxlength="156"><?php echo $linha->meta_descricao;?></textarea>
                        <span class="caracter"><span class="contadorCaracter">0</span> / 156</span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h4 class="card-title">Exemplo da pesquisa</h4>
                      <div class="meta-tag">
                          <h4 id="titulo-meta-tag">Nome home | site institucional</h4>
                          <p><span id="url-meta-tag">https:<?php echo URL;?></span><span id="pag-meta-tag"><?php //echo $diretorioAtual;?></span>/<i class="mdi mdi-menu-down"></i></p>
                          <textarea id="descricao-meta-tag" disabled>Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição.</textarea>      
                        </div>
                    </div>
                  </div>
                </div>
              </div>

              <?php } ?>

              <!--<div class="card grid-margin">
                <div class="card-body">
                  <h4 class="card-title">Opções</h4>
                  <span class="ajuda" data-tip="Tem direito a 2 páginas por exemplo: página principal, página serviço, página contato, página sobre nós." tabindex="1"><i class="mdi mdi-help "></i></span>
                  <p class="card-description">
                    Add classes like <code>.form-control-lg</code> and <code>.form-control-sm</code>
                  </p>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label>Compartilhamento</label>
                    </div>

                    <div class="col-sm-3">
                      <button type="button" class="btn btn-toggle <?php if($PaginasLista[0]->compartilhamento){echo 'active';};?>" data-toggle="button" aria-pressed="<?php if($PaginasLista[0]->compartilhamento){echo 'true';}else{echo 'false';};?>" autocomplete="off">
                        <div class="handle"></div>
                        <input type="checkbox" name="compartilhamento" value="1" <?php if($PaginasLista[0]->compartilhamento){echo 'checked';};?> hidden>
                      </button>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <label>Gatilho</label>
                    </div>

                    <div class="col-sm-3">
                      <select class="form-control form-control-sm" name="gatilho">
                      <?php foreach ($GatilhoLista as $linha) { ?>
                        <option value="<?php echo $linha->id;?>"<?php if($PaginasLista[0]->gatilho == $linha->id){echo 'selected';};?>><?php echo $linha->titulo;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>-->
              </form >
              <div class="card grid-margin">
                <div class="card-body">
                  <h4 class="card-title">Revisão</h4>
                  <span class="ajuda" data-tip="Tem direito a 2 páginas por exemplo: página principal, página serviço, página contato, página sobre nós." tabindex="1"><i class="mdi mdi-help "></i></span>
                  <div class="col-sm-6">
                    <table class="table table-responsive table-striped">
                      <thead>
                        <tr>
                          <th>Perfil</th>
                          <th>Usuário</th>
                          <th>Alteração</th>
                          <th>Restaurar</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($RestaurarLista as $linha) { ?>
                        <tr>
                          <td class="py-1">
                            <img src="<?php echo URL;?>images/faces/face1.jpg" alt="image">
                          </td>
                          <td>
                          <?php echo $linha->usuario;?>
                          </td>
                          <td>
                          <?php echo $linha->data_alteracao;?>
                          </td>
                          <td>
                            <button type="button" class="restaurar-modal btn btn-inverse-primary btn-rounded btn-icon" idRestaurar="restaurar/<?php echo $linha->id;?>" data="<?php echo $linha->data_alteracao;?>" data-toggle="modal" data-target="#restaurarModal">
                              <i class="mdi mdi-backup-restore"></i>
                            </button>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <?php foreach ($PaginasLista as $linha) { ?>
            <div class="col-md-3 grid-margin">
              <div class="card menu-atualizar">
                <div class="card-body">
                  <h4 class="card-title">Menu <?php echo $diretorioAtual;?></h4>
                  <div class="media">
                    <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
                    <div class="media-body d-flex align-self-center">
                      <p class="card-text">Atualizado <?php echo $linha->data_alteracao;?></p>
                    </div>
                  </div>
                  <div class="media">
                    <span class="d-flex align-self-start mr-3">
                      <img src="<?php echo URL;?>images/usuario/usuario.jpg" alt="profile" style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
                    </span>
                    <div class="media-body d-flex align-self-center">
                      <p class="card-text">Editado por <?php echo $linha->usuario;?></p>
                    </div>
                  </div>
                  <button class="btn btn-inverse-primary btn-rounded btn-icon-text" id="atualizarPaginaa">
                    Atualizar
                  </button>
                </div>
              </div>
            </div>
            <?php } ?>
          </div> 
        </div>               


      