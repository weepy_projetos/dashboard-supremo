
        <div class="content-wrapper">
          <div class="row">
            <div class="col-md-9 grid-margin">
              <div class="card grid-margin">
                <div class="card-body">
                  <h4 class="card-title">Página Home</h4>
                  <p class="card-description">
                    Basic form elements
                  </p>
                  <form class="forms-sample">
                    <div class="row">
                      <div class="col-md-6 ">
                        <div class="form-group">
                          <label for="exampleInputName1">Título</label>
                          <input type="text" class="form-control" id="exampleInputName1" placeholder="Name">
                        </div>
                        <div class="form-group">
                          <label for="exampleTextarea1">Descrição</label>
                          <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                        </div>
                      </div>
                      <div class="col-md-6 ">
                        <div class="form-group">
                          <label>Imagem</label>
                          <div class="row">
                          <div class="col-md-6">
                              <div class="profile" style="background-image: url('<?php echo URL; ?>images/img-upload.png');">
                                <label class="edit">
                                  <span><i class="mdi mdi-upload"></i></span>
                                  <input type="file" id="inputImagem">
                                </label>
                                <div class="delete" onclick="removePhoto()"><i class="mdi mdi-close"></i></div>
                              </div>
                          </div>
                          <div class="col-md-6">
                              <label for="exampleTextarea1">Descrição da imagem</label>
                              <textarea class="form-control" id="exampleTextarea1" rows="4"></textarea>
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>   
              <div class="card grid-margin">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <h4 class="card-title">Descrição de pesquisa</h4>
                      <span class="ajuda" data-tip="Tem direito a 2 páginas por exemplo: página principal, página serviço, página contato, página sobre nós." tabindex="1"><i class="mdi mdi-help "></i></span>
                      <p class="card-description">
                        Add classes like <code>.form-control-lg</code> and <code>.form-control-sm</code>.
                      </p>
                      <div class="form-group">
                        <label>Título</label>
                        <input type="text" value="Nome home | site institucionall" id="input-metatag-titulo" class="form-control form-control-lg" placeholder="Username" aria-label="Username" maxlength="70">
                        <span class="caracter"><span class="contadorCaracter">0</span> / 70</span>
                      </div>
                      <div class="form-group">
                        <label for="exampleTextarea1">Descrição</label>
                        <textarea class="form-control" id="textarea-metatag-descricao" rows="4" maxlength="156">descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição.</textarea>
                        <span class="caracter"><span class="contadorCaracter">0</span> / 156</span>
                      </div>
                      <div class="form-group">
                        <label>Palavras Chaves</label>
                        <input type="text" class="form-control form-control-sm" placeholder="Username" aria-label="Username">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h4 class="card-title">Exemplo da pesquisa</h4>
                      <p class="card-description">
                        Add classes like <code>.form-control-lg</code> and <code>.form-control-sm</code>.
                      </p>
                      <div class="meta-tag">
                          <h4 id="titulo-meta-tag">Nome home | site institucional</h4>
                          <p><span id="url-meta-tag">www.nome.com.br</span>/<span id="pag-meta-tag"></span>/<i class="mdi mdi-menu-down"></i></p>
                          <textarea id="descricao-meta-tag" disabled>Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição.</textarea>      
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Revisão</h4>
                  <p class="card-description">
                    Add classes like <code>.form-control-lg</code> and <code>.form-control-sm</code>.
                  </p>
                  <div class="table-responsive">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th></th>
                          <th>Nome</th>
                          <th>Alteração</th>
                          <th>Restaurar</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="py-1">
                            <img src="../../images/faces/face1.jpg" alt="image">
                          </td>
                          <td>
                            Herman Beck
                          </td>

                          <td>
                            $ 77.99
                          </td>
                          <td>
                            <button type="button" class="btn btn-outline-secondary btn-rounded btn-icon">
                              <i class="mdi mdi-backup-restore text-primary"></i>
                            </button>
                          </td>
                        </tr>
                        <tr>
                          <td class="py-1">
                            <img src="../../images/faces/face7.jpg" alt="image">
                          </td>
                          <td>
                            Henry Tom
                          </td>
                          <td>
                            $ 150.00
                          </td>
                          <td>
                            June 16, 2015
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3 grid-margin">
              <div class="card menu-atualizar">
                <div class="card-body">
                  <h4 class="card-title">Home menu</h4>
                  <div class="media">
                    <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
                    <div class="media-body d-flex align-self-center">
                      <p class="card-text">Atualizado dia 04/11/2019</p>
                    </div>
                  </div>
                  <div class="media">
                      <span class="d-flex align-self-start mr-3">
                          <img src="../../images/faces/face4.jpg" alt="profile" style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
                      </span>
                      <div class="media-body d-flex align-self-center">
                        <p class="card-text">Editado por Nome</p>
                      </div>
                    </div>
                  <button type="button" class="btn btn-primary btn-rounded btn-icon-text">
                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                    Atualizar
                  </button>
                </div>
              </div>
            </div>
          </div> 
