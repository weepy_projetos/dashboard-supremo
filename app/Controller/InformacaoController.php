<?php
namespace App\Controller;

use App\Model\InformacaoContato;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;

class InformacaoController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {

        $informacaoLista = new InformacaoContato();
        $informacaoLista = $informacaoLista->listaTodos();

        require APP . 'view/informacao-contato/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/informacao-contato/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizar($id)
    {

        $informacao = new InformacaoContato();
        $msgModal = $informacao->atualizar($id, $_POST["endereco"], $_POST["numero"], $_POST["bairro"], $_POST["cep"], $_POST["telefone"], $_POST["celular"], $_POST["whatsapp"], $_POST["email"], $_POST["facebook"], $_POST["instagram"], $_POST["linkedin"], $_POST["youtube"], $_POST["twitter"], $_POST["abertura"], $_POST["fechamento"]);

        echo json_encode($msgModal);    

    }

}

