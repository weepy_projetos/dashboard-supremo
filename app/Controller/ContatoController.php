<?php
namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Contato;
use App\Model\Usuario;
use App\Controller\LoginController;
use Mailgun\Mailgun;

class ContatoController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {

        $contatoLista = new Contato();
        $contatoLista = $contatoLista->listaTodos();

        require APP . 'view/contato/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/contato/index.php';
        require APP . 'view/contato/modal.php';
        require APP . 'view/templates/footer.php';

    }


    public function contatoJson()
    {
        $id = $_POST['id'];

        $contatoLista = new Contato();
        $contatoLista = $contatoLista->listaContatoJson($id);

        echo json_encode($contatoLista);  
    }

    public function novo()
    {

        require APP . 'view/contato/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/contato/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {

        $contatoLista = new Contato();
        $contatoLista = $contatoLista->lista($id);

        require APP . 'view/contato/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/contato/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function deletar($id)
    {
        
        $contato = new Contato();
        $contato = $contato->deletar($id);
        echo json_decode($contato);

    }

    public function enviar()
    {

        $assunto = $_POST['assunto'];
        $msg = $_POST['mensagem'];

        $email = $_POST['email'];
        $nome = 'icaro';
        $mgClient = new Mailgun('70f0da0bcc302d52a5c10de6f518d6a0-a9919d1f-4079c3dd');
        $domain = "sandboxae8c63109e424b0795b967a4c0459134.mailgun.org";

        $result = $mgClient->sendMessage("$domain",
            array('from' => 'Contato <contato@weepy.com.br>', 'to' => 'Você <' . $email . '>', 'subject' => $assunto, 'text' => $nome, 'html' => '<html style="width:500px"><style>html{width:500px; text-align:center;} img{width: 100%;} a{padding:5px 15px;}</style><img style="width:100%" src="http://www.infojr.com.br/git-github/assets/img/git-confirm.jpg"></img>
        <p>' . $msg . '</p>
        </html>'));

        if ($result) {
            echo json_encode(1);
        }

    }

    public function lixeira()
    {

        $contatoLista = new Contato();
        $contatoLista = $contatoLista->listaTodos();

        require APP . 'view/contato/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/contato/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

