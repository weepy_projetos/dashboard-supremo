<?php
namespace App\Controller;

use App\Model\Cliente;
use App\Model\Reservatempo;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;

class ReservatempoController
{
    
    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        
        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();  

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");
        $Hora = date_format($Data,"H:i");
        $HoraAtual = date_format($Data,"i");

        $horarioLista = new Reservatempo();
        $horarioLista = $horarioLista->horariosPorData($DataAtual);

        $inicioAtendimento=strtotime('08:00');
        $fim=strtotime('19:30');

         for ($i=$inicioAtendimento;$i<=$fim;$i = $i + 30*60)
         {  
            $tempo = date('H',$i);
            $verificarTempo = $tempo . ":" . '30';
            $verificarTempo = date("H:i", strtotime($verificarTempo));

            $tempoDisponivel = date('H:i',$i);
            $tempos = date('H:i',$i);
            $reservaLista = new Reservatempo();
            $reservaLista = $reservaLista->horarioocupado($tempos, $DataAtual);

            if ($verificarTempo <= $Hora) {

                //Verifica se tem horario e data de hoje
                if(!empty($reservaLista[0]->horario)) {
                    $horario = date_create($reservaLista[0]->horario);
                    $horarioCliente = date_format($horario,"H:i");

                    $nome = $reservaLista[0]->nome;
                    $atendimento = $reservaLista[0]->atendimento;
                    $servico = $reservaLista[0]->servico;
                    $observacao = $reservaLista[0]->observacao;
                    $myArray[] = array("tempo" => $tempoDisponivel,"nome" => $nome,"atendimento" => $atendimento,"servico" => $servico,"observacao" => $observacao);
                    
                }  

            } elseif (empty($reservaLista[0])) {
                $tempoDisponivel = date('H:i',$i);
                $nome = "";
                $atendimento = "";
                $servico = "";
                $observacao = "";
                $myArray[] = array("tempo" => $tempoDisponivel,"nome" => $nome,"atendimento" => $atendimento,"servico" => $servico,"observacao" => $observacao);

            } else {
                $tempoDisponivel = date('H:i',$i);
                $nome = $reservaLista[0]->nome;
                $atendimento = $reservaLista[0]->atendimento;
                $servico = $reservaLista[0]->servico;
                $observacao = $reservaLista[0]->observacao;
                $myArray[] = array("tempo" => $tempoDisponivel,"nome" => $nome,"atendimento" => $atendimento,"servico" => $servico,"observacao" => $observacao);
            }
            
         }
         
        require APP . 'view/reserva-tempo/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/reserva-tempo/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function atendimento()
    {

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reservatempo();
        $reservaLista = $reservaLista->atendimento($DataAtual);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        require APP . 'view/reserva-tempo/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/reserva-tempo/atendimento.php';
        require APP . 'view/reserva-tempo/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function iniciarAtendimento()
    {

        $id = $_POST['id'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $horaAtual = date_format($Data,"H:i:s");

        $reserva = new Reservatempo();
        $reserva = $reserva->atualizarStatus($id,"Atendimento",$horaAtual);

        echo json_encode($reserva);

        if ($reserva){
            //notificar todos da fila em status aguardando
        }

    }

    public function finalizarAtendimento()
    {

        $id = $_POST['id'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $horaAtual = date_format($Data,"H:i:s");

        $reserva = new Reservatempo();
        $reserva = $reserva->atualizarStatus($id,"Finalizado",$horaAtual);

        echo json_encode($reserva);

        if ($reserva){
            //notificar todos da fila em status aguardando
        }

    }

    public function perdeuAtendimento()
    {

        $id = $_POST['id'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $horaAtual = date_format($Data,"H:i:s");

        $reserva = new Reservatempo();
        $reserva = $reserva->atualizarStatus($id,"Perdeu a vez",$horaAtual);

        echo json_encode($reserva);

        if ($reserva){
            //notificar usuario do cancelamento
        }

    }

    public function carregarAtendimento()
    {

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reservatempo();
        $reservaLista = $reservaLista->atendimento($DataAtual);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        foreach ($reservaLista as $key => $linha) {
                echo 
                "<tr id='linha $linha->id'>
                    <td>
                        $linha->nome
                    </td>
                    <td>";
                    $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as $linhaa) {

                        if (in_array($linhaa->id, $servicos)) {

                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                            
                        }
                        
                    };
                    echo "</td>
                    <td>
                        $linha->observacao
                    </td>
                    <td>";
                        $horario = date_create($linha->horario);$hora = date_format($horario,"H:i");echo $hora;
                    echo "</td>
                    <td>
                        $linha->status
                    </td>";
                    if ($key == 0) {
                    echo"<td>
                        <button title='Visualizar Mensagem' idobjeto='$linha->id' destino='reservaTempo/iniciarAtendimento' class='iniciar-atendimento iniciar-atendimento-carregado btn btn-primary btn-rounded btn-fw'>Atender</button>
                        <button type='button' style='display:none' class='finalizado-atendimento deletar-modal btn btn-success btn-rounded btn-fw' idobjeto='$linha->id' msg='$linha->nome' destino='reservatempo/enviar/' data-toggle='modal' data-target='#FinalizadoModal'>Finalizado</button>
                        <button type='button' idobjeto='$linha->id' msg='$linha->nome' destino='reservaTempo/perdeuAtendimento' class='perdeu-atendimento btn btn-warning btn-rounded btn-fw' data-toggle='modal' data-target='#ProximoModal'>Perdeu a vez</button>
                    </td>";
                    }else {
                        echo "<td></td>";
                    };
                echo "</tr>";
                              
        }
       
        
    }

    public function carregarFila()
    {

        $data = $_POST['dataa'];

        $reservaLista = new Reservatempo();
        $reservaLista = $reservaLista-> reservaHoje("$data");

        $horarioLista = new Reservatempo();
        $horarioLista = $horarioLista->horariosPorData("$data");

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        echo 
            "<table class='table table-responsive table-striped'>
                <thead>
                  <tr>
                    <th>Horário</th>
                    <th>Cabeleireiro</th>
                    <th>Nome</th>
                    <th>Serviço</th>
                    <th>Observação</th>
                  </tr>
                </thead>
                <tbody>";
        foreach ($reservaLista as $linha) {
                echo 
                "<tr id='linha $linha->id'>
                    <td>";
                    $horario = date_create($linha->horario);$hora = date_format($horario,"H:i");echo $hora;
                    echo"</td>
                    <td>
                    $linha->atendimento
                    </td>
                    <td>
                    $linha->nome
                    </td>
                    <td>";
                    $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as  $linhaa) {

                        if (in_array($linhaa->id, $servicos)) {

                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                            
                        }
                        
                    };
                    echo"</td>
                    <td>
                    <span class='ajuda' data-tip='$linha->observacao' tabindex='1'><i class='mdi mdi-help '></i></span>
                    </td>
                </tr>";
                              
        }
        echo    "</tbody>
            </table>";
        
    }

    public function carregarhorario()
    {
        $data =  str_replace("/", "-", $_POST['data']);
        $date=date_create($data);
        $data = date_format($date,"Y/m/d");

        $horarioLista = new Reservatempo();
        $horarioLista = $horarioLista->horariosPorData("$data");

        $arrayHorarios = array();

        foreach ($horarioLista as $linhHorario) {
            $date = date_create($linhHorario->horario);
            $horario = date_format($date,"H:i");
            $arrayHorarios[] = $horario;
        }

        $inicioAtendimento=strtotime($Hora . '08:00'); 
        $fim=strtotime('19:30');

        for ($i=$inicioAtendimento;$i<=$fim;$i = $i + 30*60) {
          $tempo = date('H:i',$i);

          if (!empty($horarioLista)) {

            foreach ($arrayHorarios as $linhHorario) {

                if (in_array("$tempo",$arrayHorarios)) {
                    break;
                } else {
                    echo "<option value='$tempo'>$tempo</option>";
                    break;
                }
                
            }
            
          } else {
            echo "<option value='$tempo'>$tempo</option>";
          }
          
        } 
        
    }

    public function carregarFilaConsulta()
    {
        
        $data =  str_replace("/", "-", $_POST['dataa']);
        $date=date_create($data);
        $data = date_format($date,"Y/m/d");

        $horarioLista = new Reservatempo();
        $horarioLista = $horarioLista->horariosPorData("$data");

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        $arrayHorarios = array();

        foreach ($horarioLista as $linhHorario) {
            $date = date_create($linhHorario->horario);
            $horario = date_format($date,"H:i");
            $arrayHorarios[] = $horario;
        }

        $inicioAtendimento=strtotime('08:00'); 
        $fim=strtotime('19:30');

        echo 
            "<table class='table table-responsive table-striped'>
                <thead>
                  <tr>
                    <th>Horário</th>
                    <th>Cabeleireiro</th>
                    <th>Nome</th>
                    <th>Serviço</th>
                    <th>Observação</th>
                  </tr>
                </thead>
                <tbody>";
        for ($i=$inicioAtendimento;$i<=$fim;$i = $i + 30*60) {
        $tempo = date('H:i',$i); 

        if (!empty($arrayHorarios)) {     

            if (in_array("$tempo",$arrayHorarios)) {

                $reservaLista = new Reservatempo();
                $reservaLista = $reservaLista-> horariosPorHora("$tempo", $data);

                foreach ($reservaLista as $linha) {
                
                    
                echo 
                "<tr>
                    <td>";
                    $horario = date_create($linha->horario);$hora = date_format($horario,"H:i");echo $hora;
                    echo"</td>
                    <td>
                        
                    </td>
                    <td>
                      $linha->nome
                    </td>
                    <td>";
                    $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as  $linhaa) {

                        if (in_array($linhaa->id, $servicos)) {

                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                            
                        }
                        
                    };
                    echo"</td>
                    <td>
                    <span class='ajuda' data-tip='$linha->observacao' tabindex='1'><i class='mdi mdi-help '></i></span>
                    </td>
                </tr>";
                }
            }  else {
                echo 
                "<tr>
                    <td>
                    $tempo
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>";
            }
                              
        } else {
        echo 
        "<tr id='linha'>
            <td>
            $tempo
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>";
        }
    }

        echo    "</tbody>
            </table>";
        
    }

    public function analise()
    {
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $nomePesquisa = $_POST['nome-pesquisa'];
        $data = $_POST['data'];
        $horario = $_POST['horario'];
        $servico = serialize($_POST['servico']);
        $observacao = $_POST['observacao'];

        $dataa = str_replace('/', '-', $data);

        $array = array(); 

        if (empty($nome)){

            //$data = date_create($data);
            //$data = date_format($data,'Y-m-d');

            $data =  date('Y-m-d',strtotime($dataa));

            $reserva = new Reservatempo();
            $reserva =  $reserva->verificarhorario($horario, $data);

            if ($reserva == null) {

                $cliente = new Cliente();
                $cliente = $cliente->nome($id);

                $reserva = new Reservatempo();
                $msgModal = $reserva->inserirCliente($cliente[0]->nome_cliente, $servico,$observacao,$_SESSION['nomeUsuario'],$horario,"$data");

                if($msgModal){
                    $array += [ "boleano" => 1 ];
                    $array += [ "data" => "$data" ];
                   
                }

            } else {
                $array += [ "boleano" => 0 ];
            } 

            $msgModal = $array;
            
        } else { // Cliente Presencial

            $data =  date('Y-m-d',strtotime($dataa));

            $reserva = new Reservatempo();
            $reserva =  $reserva->verificarhorario($horario, $data);

            if ($reserva == null) {

                $cliente = new Cliente();
                $msgModal = $cliente->inserir($nome, '', '', 'usuario.jpg', '', '', 'Presencial', "$data");

                if ($msgModal) {

                    $reserva = new Reservatempo();
                    $msgModal = $reserva->inserirCliente($nome, $servico,$observacao,$_SESSION['nomeUsuario'],$horario,"$data");

                    $array += [ "boleano" => 1 ];
                    $array += [ "data" => "$data" ];

                }
            }else {
                $array += [ "boleano" => 0 ];
            } 

            $msgModal = $array;

        }
        echo json_encode($msgModal);
 
    }

}

