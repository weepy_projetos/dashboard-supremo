<?php

namespace App\Controller;

use App\Model\Paginas;
use App\Model\Slider;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Model\Equipe;
use App\Model\Depoimento;
use App\Model\Reserva;

class HomeController
{
    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");       

        $paginaLista = new Paginas();
        $paginaLista = $paginaLista->lista(1);

        $reserva = new Reserva();
        $reservaLista = $reserva->filaEsperaa($DataAtual);

        $sliderLista = new Slider();
        $sliderLista = $sliderLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->listaTodos();

        $servicoReserva = $servico->reservaIndex();

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $equipeLista = new Equipe();
        $equipeLista = $equipeLista->listaCabeleireiro();

        $depoimentoLista = new Depoimento();
        $depoimentoLista = $depoimentoLista->listaTodos();
        
        //backgound random popup
        $a = array(1, 2, 3, 4, 5);
        $random_keys = array_rand($a, 3);

        require APP . 'view/site/home/head.php'; 
        require APP . 'view/site/home/index.php';
    }
}
