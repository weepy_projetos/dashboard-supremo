<?php
namespace App\Controller;

use App\Model\Slider;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class SliderController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {

        $destaqueLista = new Slider();
        $Lista = $destaqueLista->listaTodos();

        require APP . 'view/slider/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/slider/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {

        $Usuario = new Usuario();
        $Usuario = $Usuario->usuario($_SESSION['idUsuario']);

        require APP . 'view/slider/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/slider/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {

        $destaqueLista = new Slider();
        $destaqueLista = $destaqueLista->lista($id);

        require APP . 'view/slider/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/slider/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function deletar($id)
    {
        //Deletar arquivo
        $slider = new Slider();
        $sliderr = $slider->lista($id);
        $deletarImg = explode('.',$sliderr[0]->img);
        $deletarImg = "images/slider/" .  $deletarImg[0] . '_mini.' . $deletarImg[1];
        unlink("images/slider/" . $sliderr[0]->img);
        unlink("$deletarImg");

        //Deletar banco
        $slider = $slider->deletar($id);
        echo json_decode($slider);

    }

    public function atualizar($id)
    {

        $imagem = $_FILES['imagem'];

        $slider = new Slider();
        $slider = $slider->lista($id);

        $imgBanco = $slider[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/slider');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->jpeg_quality         = 70;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 1280;
                $handle->jpeg_quality         = 70;
                $handle->image_ratio_y        = true;
                $handle->file_overwrite = true;
                $handle->file_auto_rename = false;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $slider = new Slider();
        $msgModal = $slider->atualizar($id, $_POST["titulo"], $_POST["descricao"], $img, $_POST["alt"]);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {

        $imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/slider');    
        $handle = new \Verot\Upload\Upload($imagem);

        //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
        //$nome = $imagem['name'];

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->jpeg_quality         = 70;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 1280;
            $handle->jpeg_quality         = 70;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_dst_name_body . '.' . $handle->file_dst_name_ext;
        $titulo = $_POST["titulo"];
        $descricao = $_POST["descricao"];

        $slider = new Slider();
        $msgModal = $slider->inserir(
            $_POST["titulo"],
            $_POST["descricao"],
            $img,
            $_POST["alt"]);
 
        echo json_encode($msgModal);
    }

}

