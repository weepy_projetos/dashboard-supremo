<?php
namespace App\Controller;

use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class ServicoHomemController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        $ServicoHomemLista = new ServicoHomem();
        $ServicoHomemLista = $ServicoHomemLista->listaTodos();

        require APP . 'view/servico/homem/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/homem/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        require APP . 'view/servico/homem/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/homem/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $ServicoHomemLista = new ServicoHomem();
        $ServicoHomemLista = $ServicoHomemLista->lista($id);

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        require APP . 'view/servico/homem/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/homem/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function deletar($id)
    {
        $servico = new ServicoHomem();
        $servico = $servico->deletar($id);
        echo json_decode($servico);
    }

    public function atualizar($id)
    {
        $imagem = $_FILES['imagem'];

        $servico = new ServicoHomem();
        $servico = $servico->lista($id);

        $imgBanco = $servico[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/servico/homem');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $servico = new ServicoHomem();
        $msgModal = $servico->atualizar($id, $_POST["nomeServico"], $_POST["descricao"], $_POST["categoria"], $_POST["valor"], $_POST["tempo"], $img, $_POST["alt"]);

        echo json_encode($msgModal);    
    }

    public function inserir()
    {
        $imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/servico/homem');    
        $handle = new \Verot\Upload\Upload($imagem);

        //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
        //$nome = $imagem['name'];

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 680;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_src_name;
        if (empty($img)) {
            $img = 'img-upload-homem.jpg';
        } else {
            $img = $handle->file_src_name;
        }
        $titulo = $_POST["titulo"];
        $descricao = $_POST["descricao"];

        $servico = new ServicoHomem();
        $msgModal = $servico->inserir(
            $_POST["titulo"],
            $_POST["descricao"],
            $_POST["categoria"],
            $_POST["valor"],
            $_POST["tempo"],
            $img,
            $_POST["alt"]);
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {
        $ServicoHomemLista = new ServicoHomem();
        $ServicoHomemLista = $ServicoHomemLista->listaTodos();

        require APP . 'view/servico/homem/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/homem/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

