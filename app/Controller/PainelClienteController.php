<?php

namespace App\Controller;

use App\Model\Cliente;
use App\Model\Usuario;
use App\Model\Reserva;
use App\Model\Depoimento;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Model\Notificacao;
use App\Controller\LoginController;
use Verot\Upload;

class PainelclienteController
{
    private $nome;
    private $email;
    private $senha;
    private $IdCabeleireiro;
    private $servico;

    public function __construct()
    {
        (new LoginController)->clienteLongado();

        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->listaTodos();

        header('location: ' . URL . 'painelcliente/reserva');
    }

    public function perfil()
    {
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->lista($_SESSION['idCliente']);

        require APP . 'view/painel-cliente/head.php';
        require APP . 'view/templates/header-cliente.php';
        require APP . 'view/painel-cliente/perfil.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function reserva()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data, "Y-m-d");

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $reserva = new Reserva();
        $existeReserva = $reserva->verificar($_SESSION['idCliente']); 

        $usuario = new Usuario();
        $profissionalLista = $usuario->listaTodosProfissional();

        if (!empty($existeReserva)) {

            $existeReserva = $reserva->verificarReserva($_SESSION['idCliente'], $DataAtual); 

            $reservaLista = $reserva->posiçãoReserva($DataAtual);

            //Obter posição da fila
            foreach ($reservaLista as $key => $linha) {
                if ($linha->id_cliente == $_SESSION['idCliente']) {
                    $posicaoFila = $key = ++$key;
                    break;
                }
            }
            
            require APP . 'view/painel-cliente/head.php';
            require APP . 'view/templates/header-cliente.php';
            require APP . 'view/painel-cliente/status-reserva.php';
            require APP . 'view/templates/footer.php';
        } else {
            require APP . 'view/painel-cliente/head.php';
            require APP . 'view/templates/header-cliente.php';
            require APP . 'view/painel-cliente/reserva.php';
            require APP . 'view/templates/footer.php';
        }
        
    }

    public function fazerReserva()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data, "Y-m-d");

        $this->IdCabeleireiro = $_POST['profissional'];
        $this->servico = serialize($_POST['servico']);
        $observacao = $_POST['observacao'];

        $reserva = new Reserva();
        $msgModal = $reserva->inserirCliente($_SESSION['idCliente'], $this->servico, $observacao, $this->IdCabeleireiro, $DataAtual, "Aguardando");

        if ($msgModal) {
            $notificar = new Notificacao();
            $notificar->inserir("cliente", "registro de reserva");
            $msgModal = true;
        } else {
            $msgModal = false;
        }

        echo json_encode($msgModal);
    }

    public function cancelarReserva($id)
    {
        $reserva = new Reserva();
        $reserva = $reserva->cancelarReserva($_SESSION['idCliente'],"Cancelado");

        echo json_encode($reserva);

        if ($reserva){
            //notificar usuario do cancelamento
        }

    }

    public function depoimento()
    {
        $depoimento = new Depoimento();
        $depoimento = $depoimento->listaIdUsuario($_SESSION['idCliente']);

        require APP . 'view/painel-cliente/head.php';
        require APP . 'view/templates/header-cliente.php';
        require APP . 'view/painel-cliente/depoimento.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {

        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->lista($id);

        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizarPerfil()
    {
        $id = $_SESSION['idCliente'];
        $cliente = new Cliente();
        $cliente = $cliente->lista($id);

        $senhadHash = $cliente[0]->senha;
        $senhaAntiga = $_POST['senha_antiga'];

        $imagem = $_FILES['imagem'];

        $imgBanco = $cliente[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;

        if (!empty($imgInput)) { //Se tiver imagem input

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/usuario');
            $handle = new \Verot\Upload\Upload($imagem);

            if ($handle->uploaded) {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
            }

            if ($handle->uploaded) {
                $handle->image_resize         = true;
                $handle->image_x              = 300;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;

        } else {
            $img = $imgBanco;
        }

        $boleano = array();
        

        if (!empty($senhaAntiga)) {
            if (password_verify($senhaAntiga, $senhadHash)) {
                $senha = $_POST['nova_senha'];
                $senhadHash = password_hash($senha, PASSWORD_DEFAULT);
                $boleano['senha'] = 1;
            } else {
                $boleano['senha'] = 0;
            }
        }

        $clientes = new Cliente();
        $clientes = $clientes->atualizarCliente
        (
            $id,
            $_POST['nome'],
            $_POST['email'],
            $_POST['whatsapp'],
            $_POST['aniversario'],
            $senhadHash,
            $img
        );
        $boleano['dados'] = $clientes;

        echo json_encode($boleano);
    }

    public function inserirDepoimento()
    {
        $depoimento = new Depoimento();
        $msgModal = $depoimento->inserirCliente($_SESSION["idCliente"],$_POST["profissao"],$_POST["depoimento"], $_POST["avaliacao"],$_SESSION["imgCliente"], "analise");
 
        echo json_encode($msgModal);
    }

    public function atualizarDepoimento()
    {
        $depoimento = new Depoimento();
        $msgModal = $depoimento->atualizarCliente($_SESSION["idCliente"],$_POST["profissao"],$_POST["depoimento"], $_POST["avaliacao"],$_SESSION["imgCliente"], "analise");
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {

        $ClienteLista = new Cliente();
        $ClienteLista = $ClienteLista->listaTodos();

        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/lixeira.php';
        require APP . 'view/templates/footer.php';
    }
}
