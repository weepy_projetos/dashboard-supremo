<?php
namespace App\Controller;

use App\Model\ServicoMulher;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class ServicoMulherController
{

    public function index()
    {
        (new LoginController)->usuarioLongado();

        $ServicoMulherLista = new ServicoMulher();
        $ServicoMulherLista = $ServicoMulherLista->listaTodos();

        require APP . 'view/servico/mulher/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/mulher/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        (new LoginController)->usuarioLongado();

        require APP . 'view/servico/mulher/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/mulher/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        (new LoginController)->usuarioLongado();

        $ServicoMulherLista = new ServicoMulher();
        $ServicoMulherLista = $ServicoMulherLista->lista($id);

        require APP . 'view/servico/mulher/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/mulher/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function deletar($id)
    {
        $servico = new ServicoMulher();
        $servico = $servico->deletar($id);
        echo json_decode($servico);

    }

    public function atualizar($id)
    {
        (new LoginController)->usuarioLongado();

        $imagem = $_FILES['imagem'];

        $servico = new ServicoMulher();
        $servico = $servico->lista($id);

        $imgBanco = $servico[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/servico/mulher');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $servico = new ServicoMulher();
        $msgModal = $servico->atualizar($id, $_POST["nomeServico"], $_POST["descricao"], $_POST["categoria"], $_POST["valor"], $_POST["tempo"], $img, $_POST["alt"]);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        (new LoginController)->usuarioLongado();

        $imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/servico/mulher');    
        $handle = new \Verot\Upload\Upload($imagem);

        //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
        //$nome = $imagem['name'];

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 680;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_src_name;
        $titulo = $_POST["titulo"];
        $descricao = $_POST["descricao"];

        $servico = new ServicoMulher();
        $msgModal = $servico->inserir(
            $_POST["titulo"],
            $_POST["descricao"],
            $_POST["categoria"],
            $_POST["valor"],
            $_POST["tempo"],
            $img,
            $_POST["alt"]);
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {
        (new LoginController)->usuarioLongado();

        $ServicoMulherLista = new ServicoMulher();
        $ServicoMulherLista = $ServicoMulherLista->listaTodos();

        require APP . 'view/servico/mulher/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/servico/mulher/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

