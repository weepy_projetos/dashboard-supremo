<?php
namespace App\Controller;

use App\Controller\EmailController;
use App\Model\Cliente;
use App\Model\Usuario;
use App\Model\Reserva;



class ClienteController
{
    private $nome;
    private $email;
    private $senha;

    public function __construct()
    {
        //(new LoginController)->usuarioLongado();
      
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->listaTodos();

        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function consultaNome()
    {
        $nome = $_POST['nome'];

        if(!empty($nome))
        {
            $consultaNome = new Cliente();
            $consultaNome = $consultaNome->consultaNome($nome);

            if (!empty($consultaNome)) {
                foreach ($consultaNome as $linha) {

                    $img = explode('.',$linha->img);
    
                    echo 
                    "<div class='nomeescolhido'>
                    <div class='form-check'>
                        <label class='form-check-label'>
                            <input type='radio' class=' form-check-input' name='nomeselecionado' id='$linha->id' value='$linha->nome_cliente'>                    
                            $linha->nome_cliente $linha->sobrenome
                            <i class='input-helper'></i>";
                            echo "<img src='" . URL . "images/cliente/" . $img[0] . "_mini." . $img[1] . "'" . "alt='image' style='width: 20px';>
                        </label>
                    </div></div>";     
                    
                }
            } else {
                echo "<div class='nomeescolhido'><div class='form-check'>Nenhum nome encontrado</div></div>";
            } 
        } 
    }

    public function novo()
    {

        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {

        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->lista($id);

        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function deletar($id)
    {
        $cliente = new Cliente();
        $cliente = $cliente->deletar($id);
        echo json_decode($cliente);

    }

    public function atualizar($id)
    {

        $imagem = $_FILES['imagem'];

        $cliente = new Cliente();
        $cliente = $cliente->lista($id);

        $imgBanco = $cliente[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/cliente');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $cliente = new Cliente();
        $msgModal = $cliente->atualizar($id, $_POST['nome'], $_POST['sobrenome'], $_POST['aniversario'], $_POST['email'], $_POST['telefone'], $img );

        echo json_encode($msgModal);    

    }

    public function cadastro()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");
        
        $cliente = new Cliente();
        $existeEmail = $cliente->existeEmail($_POST['email']);

        if (!empty($existeEmail)) {
            echo json_encode(0);
        } else {
            $senhadHash = password_hash($_POST['senha'], PASSWORD_DEFAULT);
            $msgModal = $cliente->cadastro($_POST['nome'],$_POST['email'], $_POST['celular'], $senhadHash, "usuario.jpg", "Online", $DataAtual, 0);
            
            //Envio de Email
            $usuario = new Usuario();
            $listaUsuario = $usuario->listaAdmin();
            
            if ($msgModal) {
                foreach ($listaUsuario as  $linha) {
                    $emailClass = new EmailController($_POST['nome'],"Novo Cliente",$linha->email,'','');
                    $email = $emailClass->novoClienteCadastrado();
                }
                echo json_encode($email);
            } else {
                echo json_encode(0);
            }
        }
    }

    public function cadastroReserva()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $cliente = new Cliente();
        $existeEmail = $cliente->existeEmail($_POST['email']);

        if (!empty($existeEmail)) {
            echo json_encode(0);
        } else {
            $senhadHash = password_hash($_POST['senha'], PASSWORD_DEFAULT);
            $id = $cliente->cadastroReserva($_POST['nome'],$_POST['email'], $_POST['celular'], $senhadHash, "usuario.jpg", "Online", $DataAtual, 0);

            $servico = serialize(json_decode($_POST['servico']));

            $_SESSION['idCliente'] = $id;
            $_SESSION['nomeCliente'] = $_POST['nome'];
            $_SESSION['imgCliente'] = "usuario.jpg";
                
            $reserva = new Reserva();
            $msgModal = $reserva->inserirCliente($id,$servico,$_POST['observacao'], $_POST['profissional'], $DataAtual, "Aguardando");
             
            //Envio de Email
            $usuario = new Usuario();
            $listaUsuario = $usuario->listaAdmin();

            echo json_encode(1);
            if ($msgModal) {
                foreach ($listaUsuario as  $linha) {
                    $emailClass = new EmailController($_POST['nome'],'',"Novo Cliente",$linha->email,'','');
                    $email = $emailClass->novoClienteCadastrado();
                }
                
            } else {
                echo json_encode(0);
            }
        }
    }

    public function inserir()
    {
        $imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/cliente');    
        $handle = new \Verot\Upload\Upload($imagem);

        //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
        //$nome = $imagem['name'];

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 680;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_src_name;

        $cliente = new Cliente();
        $msgModal = $cliente->inserir($_POST['nome'], $_POST['sobrenome'], $_POST['aniversario'], $img, $_POST['email'], $_POST['telefone'], "Presencial", "");
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {

        $ClienteLista = new Cliente();
        $ClienteLista = $ClienteLista->listaTodos();

        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

