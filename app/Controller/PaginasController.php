<?php
namespace App\Controller;

use App\Model\Paginas;
use App\Model\Gatilho;
use App\Model\Restaurar;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class PaginasController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        LoginController::autentificacao();

        require APP . 'view/paginas/inicio/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/paginas/inicio/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizar()
    {

            $PaginasLista = new Paginas();
            $PaginasLista = $PaginasLista->lista(1);

            $Usuario = new Usuario();
            $Usuario = $Usuario->usuario($_SESSION['idUsuario']);

            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images');    
            $handle = new \Verot\Upload\Upload($_FILES['imagem']);

            if ($handle->uploaded) {
              $handle->file_new_name_body   = null;
              $handle->image_resize         = true;
              $handle->image_x              = 100;
              $handle->image_ratio_y        = true;
              $handle->process($diretorio_destino);
              if ($handle->processed) {
                echo 'image resized';
                $handle->clean();
                } else {
                echo 'error : ' . $handle->error;
              }
            }
            $ImgExiste = $PaginasLista[0]->img;
            $ImgNova = $handle->file_src_name;

            if (!empty($ImgNova)) {
                $img = $handle->file_src_name;
            }elseif (empty($ImgExiste)) { 
                $img = $handle->file_src_name;
            } else {
                $img = $PaginasLista[0]->img;
            }   
            
            // DATA
            date_default_timezone_set('America/Sao_Paulo');
            $Data = date_create();
            $DataHora = date_format($Data,"d/m/Y H:i");
            
            $Paginas = new Paginas();
            $msgModal = $Paginas->atualizar
            (
                1,
                $_POST["titulo"],
                $_POST["descricao"],
                $_POST["alt"],
                $img,
                $_POST["meta_titulo"],
                $_POST["meta_descricao"], 
                $_POST["compartilhamento"],
                $_POST["gatilho"],
                $DataHora,
                $Usuario[0]->nome
            );

            echo json_encode($msgModal);
            

            // Se modal retornar TRUE gerar restauração
            if ($msgModal) {
                //$PaginaRestaurar = new Restaurar();
                //$PaginaRestaurar->criarRestauracao(1);
            }
        
    }

    public function restaurar($id_pagina)
    {
            
        $PaginaRestaurar = new Restaurar();
        $msgModal = $PaginaRestaurar->restaurar($id_pagina);

        echo json_encode($msgModal);
                
    }


    public function inicio()
    {

        $ArrPATH = explode( "/", $_SERVER['REQUEST_URI'] );
        $diretorioAtual = $ArrPATH[ count( $ArrPATH ) - 1 ];
       
        $PaginasLista = new Paginas();
        $PaginasLista = $PaginasLista->lista(1);

        $GatilhoLista = new Gatilho();
        $GatilhoLista = $GatilhoLista->lista();

        $RestaurarLista = new Restaurar();
        $RestaurarLista = $RestaurarLista->lista(1);

        $Usuario = new Usuario();
        $Usuario = $Usuario->usuario($_SESSION['idUsuario']);

        require APP . 'view/paginas/inicio/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/paginas/inicio/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';

    }
     
    public function sobre()
    {
        require APP . 'view/paginas/sobre/index.php';
    }
}

