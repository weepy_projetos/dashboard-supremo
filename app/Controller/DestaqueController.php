<?php
namespace App\Controller;

use App\Model\Destaque;
use App\Model\Usuario;
use App\Controller\LoginController;
use Verot\Upload;

class DestaqueController
{

    public function index()
    {
        (new LoginController)->usuarioLongado();

        $destaqueLista = new Destaque();
        $destaqueLista = $destaqueLista->listaTodos();

        require APP . 'view/destaque/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/destaque/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        (new LoginController)->usuarioLongado();

        require APP . 'view/destaque/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/destaque/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        (new LoginController)->usuarioLongado();

        $destaqueLista = new Destaque();
        $destaqueLista = $destaqueLista->lista($id);

        require APP . 'view/destaque/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/destaque/editar.php';
        require APP . 'view/templates/footer.php';

    }

    public function deletar($id)
    {
        $destaque = new Destaque();
        $destaque = $destaque->deletar($id);
        echo json_decode($destaque);

    }

    public function atualizar($id)
    {
        (new LoginController)->usuarioLongado();

        $imagem = $_FILES['imagem'];

        $destaque = new Destaque();
        $destaque = $destaque->lista($id);

        $imgBanco = $destaque[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/destaque');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $destaque = new Destaque();
        $msgModal = $destaque->atualizar($id, $_POST["titulo"], $_POST["descricao"], $img, $_POST["alt"]);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        (new LoginController)->usuarioLongado();

        $imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/destaque');    
        $handle = new \Verot\Upload\Upload($imagem);

        //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
        //$nome = $imagem['name'];

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 680;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_src_name;
        $titulo = $_POST["titulo"];
        $descricao = $_POST["descricao"];

        $destaque = new Destaque();
        $msgModal = $destaque->inserir(
            $_POST["titulo"],
            $_POST["descricao"],
            $img,
            $_POST["alt"]);
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {
        (new LoginController)->usuarioLongado();

        $destaqueLista = new Destaque();
        $destaqueLista = $destaqueLista->listaTodos();

        require APP . 'view/destaque/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/destaque/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

