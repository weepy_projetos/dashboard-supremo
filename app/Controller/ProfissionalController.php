<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Equipe;
use App\Model\Reserva;
use App\Model\Cliente;
use App\Model\Usuario;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use Verot\Upload;

class ProfissionalController
{
    private $categorias;

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        $equipe = new Equipe();
        $equipeLista = $equipe->lista();

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function diasTrabalho()
    {
        $equipe = new Equipe();
        $diasLista = $equipe->listaDias($_SESSION['idUsuario']);

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/dias-trabalho.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function cliente()
    {
        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->listaClienteProfissional($_SESSION['idUsuario']);

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/cliente.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function reserva()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reserva();
        $reservaLista = $reservaLista->listaAtendimentoProfissional($DataAtual, $_SESSION['idUsuario']);

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos(); 

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        $clienteLista = new Cliente();
        $clienteLista = $clienteLista->listaTodos();

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/reserva/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function atendimento()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

		$reservaListaClass = new Reserva();
        $reservaLista = $reservaListaClass->listaAtendimento($DataAtual, 1);
		$relatoriosLista = $reservaListaClass->ServicosFinalizados();

        $servicoListaClass = new ServicoHomem();
        $servicoLista = $servicoListaClass->TodosIdNomeCategoria();
		$servicoListaR = $servicoListaClass->TodosIdNome();

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/reserva/atendimento.php';
        require APP . 'view/reserva/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function servico()
    {
        $equipe = new Equipe();
        $equipeLista = $equipe->equipe($_SESSION['idUsuario']);

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->TodosIdNomeCategorias();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->id);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/servico.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizarDias()
    {

        if (isset($_POST["monday"])) {
            $monday = $_POST["monday"];
        } else {
            $monday = 0;
        }

        if (isset($_POST["tuesday"])) {
            $tuesday = $_POST["tuesday"];
        } else {
            $tuesday = 0;
        }

        if (isset($_POST["wednesday"])) {
            $wednesday = $_POST["wednesday"];
        } else {
            $wednesday = 0;
        }

        if (isset($_POST["thursday"])) {
            $thursday = $_POST["thursday"];
        } else {
            $thursday = 0;
        }

        if (isset($_POST["thursday"])) {
            $thursday = $_POST["thursday"];
        } else {
            $thursday = 0;
        }

        if (isset($_POST["friday"])) {
            $friday = $_POST["friday"];
        } else {
            $friday = 0;
        }
        
        if (isset($_POST["friday"])) {
            $friday = $_POST["friday"];
        } else {
            $friday = 0;
        }
 
        if (isset($_POST["saturday"])) {
            $saturday = $_POST["saturday"];
        } else {
            $saturday = 0;
        }

        if (isset($_POST["sunday"])) {
            $sunday = $_POST["sunday"];
        } else {
            $sunday = 0;
        }

        $equipe = new Equipe();
        $msgModal = $equipe->atualizarDias( $_SESSION['idUsuario'], $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        /*$categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        foreach ($categorias as $key => $linha) {
            if (!empty($_POST['servico'."$key"])) {
                $array[] = [$linha=> $_POST['servico'."$key"]];
            } else {
                $array[] = [$linha=> false];
            }  
        }*/

        $servicos = serialize($_POST['servico']);
        $equipe = new Equipe();
        $msgModal = $equipe->atualizarServico(
            $_SESSION['idUsuario'],
            $servicos    
        );
 
        echo json_encode($msgModal);
    }

    public function perfil()
    {
        $usuarioLista = new Usuario();
        $usuarioLista = $usuarioLista->lista($_SESSION['idUsuario']);

        require APP . 'view/profissional/head.php';
        require APP . 'view/templates/header-cabele.php';
        require APP . 'view/profissional/perfil.php';
        require APP . 'view/templates/footer.php';
    }
}

