<?php

/**
 * Classe HomeController
 *
 */

namespace App\Controller;
use InstagramAPI\InstagramAPI;

class TesteController
{
    public function index()
    {

        $ig = new \InstagramAPI\Instagram();

        require APP . 'view/teste/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/teste/index.php';
        require APP . 'view/templates/footer.php';
    }
}
