<?php

namespace App\Controller;

use App\Model\Galeria;

class GaleriaController
{
    public function index()
    {

        $galeria = new Galeria();
        $galeriaLista = $galeria->lista();

        require APP . 'view/galeria/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/galeria/index.php';
        require APP . 'view/templates/footer.php';
    }
}
