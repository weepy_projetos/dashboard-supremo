<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Equipe;
use App\Model\Usuario;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use Respect\Validation\Rules\Length;
use Verot\Upload;

class EquipeController
{
    private $categorias;

    public function __construct()
    {
        //(new LoginController)->usuarioLongado();
        
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        $equipe = new Equipe();
        $equipeLista = $equipe->lista();

        require APP . 'view/equipe/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/equipe/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function diasTrabalho()
    {
        $equipe = new Equipe();
        $diasLista = $equipe->listaDias($_SESSION['idUsuario']);

        require APP . 'view/equipe/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/equipe/dias-trabalho.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function servico()
    {
        $equipe = new Equipe();
        $equipeLista = $equipe->equipe($_SESSION['idUsuario']);

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->TodosIdNomeCategoria();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        require APP . 'view/equipe/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/equipe/servico.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
        $teste = $nivelAcesso;

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();
        $servicoLista = $servico->TodosIdNomeCategoria();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        require APP . 'view/equipe/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/equipe/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $equipe = new Equipe();
        $equipeLista = $equipe->equipe($id);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->listaTodos();

        require APP . 'view/equipe/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/equipe/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function deletar($id)
    {
        $equipe = new Equipe();
        $equipe = $equipe->deletar($id);
        echo json_decode($equipe);
    }

    public function atualizar($id)
    {
        $imagem = $_FILES['imagem'];

        $destaque = new Equipe();
        $destaque = $destaque->equipe($id);

        $imgBanco = $destaque[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/equipe');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 300;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 

        $servicos = serialize($_POST['servico']);
        
        $destaque = new Equipe();
        $msgModal = $destaque->atualizar($id, $_POST["nome"], $_POST["email"], $img, $servicos);

        echo json_encode($msgModal);    
    }

    public function atualizarDias()
    {

        if (isset($_POST["monday"])) {
            $monday = $_POST["monday"];
        } else {
            $monday = 0;
        }

        if (isset($_POST["tuesday"])) {
            $tuesday = $_POST["tuesday"];
        } else {
            $tuesday = 0;
        }

        if (isset($_POST["wednesday"])) {
            $wednesday = $_POST["wednesday"];
        } else {
            $wednesday = 0;
        }

        if (isset($_POST["thursday"])) {
            $thursday = $_POST["thursday"];
        } else {
            $thursday = 0;
        }

        if (isset($_POST["thursday"])) {
            $thursday = $_POST["thursday"];
        } else {
            $thursday = 0;
        }

        if (isset($_POST["friday"])) {
            $friday = $_POST["friday"];
        } else {
            $friday = 0;
        }

        if (isset($_POST["friday"])) {
            $friday = $_POST["friday"];
        } else {
            $friday = 0;
        }
  
        if (isset($_POST["saturday"])) {
            $saturday = $_POST["saturday"];
        } else {
            $saturday = 0;
        }

        if (isset($_POST["sunday"])) {
            $sunday = $_POST["sunday"];
        } else {
            $sunday = 0;
        }

        $equipe = new Equipe();
        $msgModal = $equipe->atualizarDias( $_SESSION['idUsuario'], $monday, $tuesday, $wednesday, $thursday, $friday, $saturday, $sunday);

        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos();

        $servico = new ServicoHomem();

        $categorias = array();
        foreach ($categoriaLista as $linha) {

            $categoria = $servico->consultaCategoria($linha->categoria);

            if (!empty($categoria)) {
                array_push($categorias, $linha->categoria); 
            }
        }

        foreach ($categorias as $key => $linha) {
            if (!empty($_POST['servico'."$key"])) {
                $array[] = [$linha=> $_POST['servico'."$key"]];
            } else {
                $array[] = [$linha=> false];
            }  
        }

        $img = "icaro.jpg";
        $servicos = serialize($array);
        $usuario = new Equipe();
        $msgModal = $usuario->inserir(
            $_SESSION['idUsuario'],
            $_POST["email"],
            $img,
            $servicos    
        );
 
        echo json_encode($msgModal);
    }

    public function nomeDia()
    {
        $diasemana = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        $hoje = date('Y-m-d');
        $diasemana_numero = date('w', strtotime($hoje));
        return $diasemana[$diasemana_numero];
    }

    public function consultaProfissional()
    {
        $equipe = new Equipe();
        $equipeTrabalhaHoje = $equipe->consultaDia($this->nomeDia());

        //Array de id de serviços Checked
        $servicoCheked = json_decode($_POST['servico']);

       $totalServico = count($servicoCheked);
        $equipeDisponivel = array();
        $t = 0;
        foreach ($equipeTrabalhaHoje as  $profissional) {
            $equipeServico = $equipe->consultaServico($profissional->id);
            $equipeServico = unserialize($equipeServico[0]->servicos);

            for ($i=0; $i < $totalServico;  $i++) { 
                //Se não tiver o serviço
                if (in_array($servicoCheked[$i], $equipeServico)) {
                    $t = ++$i;  
                    if($totalServico == $t) {
                        array_push($equipeDisponivel,$profissional->id);
                    } 
              } else{
            break;
              }
            }      
        }

        if (!empty($equipeDisponivel)) {
            $ids = '';
            $len = count($equipeDisponivel);
            foreach ($equipeDisponivel as $index => $item) {
                if ($index == 0) {
                    $ids .= $item;
                } else if ($index == $len - 1) {
                    $ids .= ','.$item;
                }
            }
        }

        if (isset($ids)) {
            $equipeLista = $equipe->listaCabeleireiroFiltro($ids);
            echo '<option value="ambos" imagem="'.URL.'images/usuario/usuario.jpg">Ambos</option>';
            foreach ($equipeLista as $profissional) {
                echo '<option value="' . $profissional->id . '" imagem="'.URL.'images/usuario/'.$profissional->img.'">'. $profissional->nome .'</option>';
            }
        } else {
            echo json_encode(0);
        }

    }

}

