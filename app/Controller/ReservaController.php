<?php
namespace App\Controller;

use App\Model\Cliente;
use App\Model\Reserva;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Controller\NivelController;
use App\Model\Usuario;
use App\Controller\LoginController;

class ReservaController
{
    private $cabeleireiro;


    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");
        $Hora = date_format($Data,"H:i");
        $HoraAtual = date_format($Data,"i");

        $reservaListaClass = new Reserva();
        $reservaLista = $reservaListaClass->listaAtendimento($DataAtual, 1);

        $servicoListaClass = new ServicoHomem();
        $servicoLista = $servicoListaClass->TodosIdNomeCategoria();

        $categoriaLista = new CategoriaServico();
        $categoriaLista = $categoriaLista->listaTodos(); 

        $usuario = new Usuario();
        $profissionalLista = $usuario->listaTodosProfissional();
		
	
        
        require APP . 'view/reserva/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/reserva/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function carregarFila()
    {
        $this->cabeleireiro = $_SESSION['nomeUsuario'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reserva();
        $reservaLista = $reservaLista->listaAtendimento($DataAtual, $_SESSION['idUsuario']);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        echo 
                "<table class='table table-responsive table-striped'>
                <thead>
                  <tr>
                    <th>Cabeleireiro</th>
                    <th>Cliente</th>
                    <th>Serviço</th>
                    <th>Observação</th>
                  </tr>
                </thead>
                <tbody>";
        foreach ($reservaLista as $linha) {

                echo 
                "<tr>
                    <td>
                    $linha->nome
                    </td>
                    <td>
                      $linha->nome_cliente
                    </td>
                    <td>";
                    $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as $linhaa) {

                        if (in_array($linhaa->id, $servicos)) {

                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                            
                        }
                        
                    }; 
                echo "</td>
                    <td>
                      $linha->observacao
                    </td>
                  </tr>";
                                 
        }
        echo "</tbody>
              </table>";
        
    }

    public function atendimento()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

		$reservaListaClass = new Reserva();
        $reservaLista = $reservaListaClass->listaAtendimento($DataAtual, 1);
		$relatoriosLista = $reservaListaClass->ServicosFinalizados();

        $servicoListaClass = new ServicoHomem();
        $servicoLista = $servicoListaClass->TodosIdNomeCategoria();
		$servicoListaR = $servicoListaClass->TodosIdNome();

        require APP . 'view/reserva/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/reserva/atendimento.php';
        require APP . 'view/reserva/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function iniciarAtendimento()
    {
        $id = $_POST['id'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $horaAtual = date_format($Data,"H:i:s");

        $reserva = new Reserva();
        $reserva = $reserva->atualizarStatus($id,"Atendimento",$horaAtual);

        echo json_encode($reserva);

        if ($reserva){
            //notificar todos da fila em status aguardando
        }
    }

    public function finalizarAtendimento()
    {
        $id = $_POST['id'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $horaAtual = date_format($Data,"H:i:s");

        $reserva = new Reserva();
        $reserva = $reserva->atualizarStatus($id,"Finalizado",$horaAtual);

        echo json_encode($reserva);

        if ($reserva){
            //notificar todos da fila em status aguardando
        }
    }

    public function perdeuAtendimento()
    {
        $id = $_POST['id'];

        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $horaAtual = date_format($Data,"H:i:s");

        $reserva = new Reserva();
        $reserva = $reserva->atualizarStatus($id,"Perdeu a vez",$horaAtual);

        echo json_encode($reserva);

        if ($reserva){
            //notificar usuario do cancelamento
        }

    }

    public function carregarAtendimento()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reserva();
        $reservaLista = $reservaLista->carregarAtendimento($DataAtual);

        $servicoLista = new ServicoHomem();
        $servicoLista = $servicoLista->TodosIdNomeCategoria();

        foreach ($reservaLista as $key => $linha) {
                echo 
                "<tr id='linha$linha->id'>
                    <td>
                        $linha->nome_cliente
                    </td>
                    <td>";
                    $servicos = unserialize($linha->servico);
                    $virgula = true;
                    foreach ($servicoLista as $linhaa) {

                        if (in_array($linhaa->id, $servicos)) {

                            if($virgula){
                                echo $linhaa->nome_servico;
                                $virgula = false;
                            } else {
                                echo ", " . $linhaa->nome_servico;
                            }
                            
                        }
                        
                    };
                    echo "</td>
                    <td>
                        $linha->observacao
                    </td>
                    <td id='estatus$linha->id'>
                    <label class='badge badge-info'>$linha->estatus</label>
                    </td>";
                    if ($key == 0) {
                    echo"<td>
                        <button title='Visualizar Mensagem' idobjeto='$linha->id' destino='reserva/iniciarAtendimento' class='iniciar-atendimento iniciar-atendimento-carregado btn btn-primary btn-rounded btn-fw'>Atender</button>
                        <button type='button' style='display:none' class='finalizado-atendimento finalizar-atendimento-carregado deletar-modal deletar-modal-carregado btn btn-success btn-rounded btn-fw' idobjeto='$linha->id' msg='$linha->nome_cliente' destino='reserva/finalizarAtendimento' data-toggle='modal' data-target='#FinalizadoModal'>Finalizado</button>
                        <button type='button' idobjeto='$linha->id' msg='$linha->nome_cliente' destino='reserva/perdeuAtendimento' class='alerta-modal-carregado perdeu-atendimento-oculta perdeu-atendimento-carregado btn btn-warning btn-rounded btn-fw' data-toggle='modal' data-target='#AlertaModal'>Perdeu</button>
                    </td>";
                    }else {
                        echo "<td></td>";
                    };
                echo "</tr>";
                              
        }
           
    }

    public function analise()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        $reservaLista = new Reserva();
        $reservaLista = $reservaLista->listaAtendimento($DataAtual, 1);
        $id = $_POST['id'];
        //$nome = $_POST['cliente_sem_cadastro'];

        $servico = serialize($_POST['servico']);
        $observacao = $_POST['observacao'];
        if (isset($_POST['profissional'])) {
            $profissional = $_POST['profissional'];
        }

        
        if (!isset($_POST['cliente_sem_cadastro'])){ //checked
            
            if ($_SESSION['nivelUsuario'] == "cabeleireiro") {
                $reserva = new Reserva();
                $msgModal = $reserva->inserirCliente($id,$servico,$observacao, $_SESSION['idUsuario'], $DataAtual, "Aguardando");
            } else {
                $reserva = new Reserva();
                $msgModal = $reserva->inserirCliente($id,$servico,$observacao, $profissional, $DataAtual, "Aguardando");
            }
            
        } else {

            $cliente = new Cliente();
            $msgModal_1 = $cliente->inserir($_POST['nome'], "", '', 'usuario.jpg', "", '', 'semCadastro',  $DataAtual);

            $reserva = new Reserva();
            $msgModal_2 = $reserva->inserirCliente($msgModal_1['id'],$servico,$observacao, $_SESSION['idUsuario'], $DataAtual, "Aguardando");

            if ($msgModal_1['boleano'] && $msgModal_2){
                $msgModal = true;
            } else {
                $msgModal = false;
            }
            
        }
 
        echo json_encode($msgModal);
    }

    public function lixeira()
    {

        $reservaLista = new Reserva();
        $reservaLista = $reservaLista->listaTodos();

        require APP . 'view/reserva/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/reserva/lixeira.php';
        require APP . 'view/templates/footer.php';
    }

}

