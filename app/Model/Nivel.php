<?php

namespace App\Model;

use App\Core\Model;

class Nivel extends Model
{

    public function lista()
    {
        $sql = "SELECT * FROM 'controler_nivel WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaNivel($nivel)
    {
        $sql = "SELECT controler FROM `controler_nivel` WHERE $nivel='1'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function acessoControler($nomeController, $nivel)
    {
        $sql = "SELECT $nivel FROM `controler_nivel` WHERE controler='$nomeController' ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id_usuario, $img, $nome, $email, $senha, $nivel)
    {
        $sql = "UPDATE controler_nivel SET nome = :nome, email = :email, data_nasc = :data_nasc, cpf = :cpf WHERE id = :cliente_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, 'cpf' => $cpf, ':cliente_id' => $id_usuario);

        $query->execute($parameters);
    }

    public function adicionar($img, $nome, $email, $senha, $nivel)
    {
        $sql = "INSERT INTO controler_nivel (nome, email, data_nasc, cpf) VALUES (:nome, :email, :data_nasc, :cpf)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, ':cpf' => $cpf);

        $query->execute($parameters);
    }

    public function deletar($id_usuario)
    {
        $sql = "DELETE FROM controler_nivel WHERE id = :id_usuario";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_usuario);

        $query->execute($parameters);
    }

}
