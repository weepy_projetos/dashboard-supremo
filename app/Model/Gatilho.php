<?php

namespace App\Model;

use App\Core\Model;

class Gatilho extends Model
{

    public function lista()
    {
        $sql = "SELECT * FROM `gatilhos` WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id_gatilho, $titulo, $descricao, $botao1, $botao2)
    {
        $sql = "UPDATE gatilhos SET nome = :nome, email = :email, data_nasc = :data_nasc, cpf = :cpf WHERE id = :cliente_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, 'cpf' => $cpf, ':cliente_id' => $id_gatilho);

        $query->execute($parameters);
    }

    public function adicionar($titulo, $descricao, $botao1, $botao2)
    {
        $sql = "INSERT INTO gatilhos (nome, email, data_nasc, cpf) VALUES (:nome, :email, :data_nasc, :cpf)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, ':cpf' => $cpf);

        $query->execute($parameters);
    }

    public function deletar($id_gatilho)
    {
        $sql = "DELETE FROM gatilhos WHERE id = :id_gatilho";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_gatilho);

        $query->execute($parameters);
    }

}
