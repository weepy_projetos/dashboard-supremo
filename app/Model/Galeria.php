<?php

namespace App\Model;

use App\Core\Model;

class Galeria extends Model
{

    public function lista()
    {
        $sql = "SELECT * FROM `galeria` WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id_galeria, $categoria, $img, $alt)
    {
        $sql = "UPDATE galeria SET nome = :nome, email = :email, data_nasc = :data_nasc, cpf = :cpf WHERE id = :cliente_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, 'cpf' => $cpf, ':cliente_id' => $id_galeria);

        $query->execute($parameters);
    }

    public function adicionar($titulo, $subtitulo, $img, $descricao, $alt)
    {
        $sql = "INSERT INTO galeria (nome, email, data_nasc, cpf) VALUES (:nome, :email, :data_nasc, :cpf)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, ':cpf' => $cpf);

        $query->execute($parameters);
    }

    public function deletar($id_galeria)
    {
        $sql = "DELETE FROM galeria WHERE id = :id_galeria";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_galeria);

        $query->execute($parameters);
    }

}
