<?php

namespace App\Model;

use App\Core\Model;

class Relatorios extends Model
{

    public function ServicosFinalizados()
    {
        $sql = "SELECT servico FROM reserva WHERE estatus='Finalizado'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function ServicosFinalizadosData($data)
    {
        $sql = "SELECT servico FROM reserva WHERE estatus='Finalizado' AND data_servico='$data' ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function ServicosCancelados()
    {
        $sql = "SELECT servico FROM reserva WHERE estatus='Cancelados' ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function ServicosLucrativos()
    {
        $sql = "SELECT servico FROM reserva WHERE estatus='Finalizado' AND data_servico='$data' ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function ClientesLucrativos()
    {
        $sql = "SELECT nome, sobrenome FROM reserva WHERE estatus='Finalizado'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function LucroMensal()
    {
        $sql = "SELECT servico FROM reserva WHERE estatus='Finalizado'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM destaque WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $titulo, $descricao, $img, $alt )
    {
        $sql = "update destaque set titulo = '".$titulo."', descricao = '".$descricao."',img = '".$img."',alt = '".$alt."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserir($titulo, $descricao, $img, $alt)
    {
        $sql = "INSERT INTO destaque (titulo, descricao, img, alt) VALUES (:titulo, :descricao,  :img, :alt)";
        $query = $this->db->prepare($sql);
        $parameters = array(':titulo' => $titulo, ':descricao' => $descricao, ':img' => $img, 'alt' => $alt);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM destaque WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
