<?php

namespace App\Model;

use App\Core\Model;

class Equipe extends Model
{

    public function lista()
    {
        $sql = "SELECT equipe.*, usuario.img, usuario.nome FROM `equipe` INNER JOIN usuario ON equipe.id_usuario = usuario.id WHERE usuario.id_nivel=3";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaTodos()
    {
        $sql = "SELECT * FROM `equipe` WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaCabeleireiro()
    {
        
        $sql = "SELECT usuario.id,usuario.img, usuario.nome, equipe.servicos FROM `equipe` INNER JOIN usuario ON equipe.id_usuario = usuario.id WHERE usuario.id_nivel=3";
        $query = $this->db->prepare($sql);
        $resultado = $query->execute();

        /*if (empty($resultado)) {
            $sql = "SELECT usuario.id,usuario.img, usuario.nome, equipe.servicos, (0) as fila FROM reserva INNER JOIN usuario ON usuario.id =reserva.id_usuario INNER JOIN equipe on equipe.id_usuario = reserva.id_usuario WHERE usuario.id_nivel = 3 GROUP BY reserva.id_usuario";
            $query = $this->db->prepare($sql);
            $query->execute();


        } else {
            $sql = "SELECT usuario.id,usuario.img, usuario.nome, equipe.servicos, if(COUNT(*)<0,0,COUNT(*)) as fila FROM reserva INNER JOIN usuario ON usuario.id =reserva.id_usuario INNER JOIN equipe on equipe.id_usuario = reserva.id_usuario WHERE  reserva.estatus = 'Aguardando' GROUP BY reserva.id_usuario";
            $query = $this->db->prepare($sql);
            $query->execute();

        }*/
        return $query->fetchAll();
    }

    public function listaCabeleireiroFiltro($ids)
    {
        $sql = "SELECT equipe.servicos, usuario.id, usuario.img, usuario.nome FROM `equipe` INNER JOIN usuario ON equipe.id_usuario = usuario.id WHERE equipe.id_usuario IN ($ids)";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function consultaDia($dia)
    {
        $sql = "SELECT usuario.id FROM `equipe` INNER JOIN usuario ON equipe.id_usuario = usuario.id WHERE equipe.$dia=1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function consultaServico($id)
    {
        $sql = "SELECT equipe.servicos FROM `equipe` INNER JOIN usuario ON equipe.id_usuario = usuario.id WHERE equipe.id_usuario=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaDias($id)
    {
        $sql = "SELECT monday,tuesday, wednesday, thursday, friday, saturday, sunday FROM `equipe` WHERE id_usuario='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function equipe($id)
    {
        $sql = "SELECT equipe.*, usuario.img, usuario.nome FROM `equipe` INNER JOIN usuario ON equipe.id_usuario = usuario.id WHERE equipe.id_usuario='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $nome, $email, $img, $servicos )
    {
        $sql = "update equipe set nome = '".$nome."', email = '".$email."', servicos = '".$servicos."',img = '".$img."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function atualizarDias($id, $monday, $tuesday,$wednesday, $thursday, $friday, $saturday, $sunday )
    {
        $sql = "update equipe set Monday = '".$monday."', Tuesday = '".$tuesday."', wednesday = '".$wednesday."',thursday = '".$thursday."',friday = '".$friday."',saturday = '".$saturday."',sunday = '".$sunday."' where id_usuario = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserirNovo($id_usuario)
    {
        $sql = "INSERT INTO equipe (id_usuario) VALUES (:id_usuario)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id_usuario' => $id_usuario);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserir($id_usuario, $servicos)
    {
        $sql = "INSERT INTO equipe (id_usuario, servicos) VALUES (:id_usuario, :servicos)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id_usuario' => $id_usuario, 'servicos' => $servicos);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function atualizarServico($id_usuario, $servicos)
    {
        $sql = "update equipe set servicos = '".$servicos."' where id_usuario='$id_usuario'";
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM equipe WHERE id= $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
