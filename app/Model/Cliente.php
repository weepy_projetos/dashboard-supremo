<?php

namespace App\Model;

use App\Core\Model;

class Cliente extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `cliente` WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaClienteProfissional($id)
    {
        $sql = "SELECT cliente.nome_cliente,cliente.sobrenome,cliente.tipo_cadastro,cliente.img FROM cliente INNER JOIN reserva ON reserva.id_cliente=cliente.id WHERE reserva.id_usuario=$id ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function consultaNome($nome)
    {
        $sql = "SELECT id,nome_cliente,sobrenome,img FROM cliente WHERE nome_cliente LIKE '%$nome%'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function recuperarSenha($email)
    {
        $sql = "SELECT id,nome_cliente FROM cliente WHERE email='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM `cliente` WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function img($id)
    {
        $sql = "SELECT img FROM `cliente` WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function id($email)
    {
        $sql = "SELECT id FROM `cliente` WHERE email='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function idRecuperar($id)
    {
        $sql = "SELECT id,recuperar FROM `cliente` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function recuperar($id, $recuperar)
    {
        $sql = "UPDATE cliente SET recuperar = '$recuperar' WHERE id = $id";
        $query = $this->db->prepare($sql);   
        $query->execute(); 
    }

    public function atualizarSenha($id, $senha)
    {
        $sql = "UPDATE cliente SET senha = '$senha' WHERE id = $id";
        $query = $this->db->prepare($sql);   
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function idNomeImg($id)
    {
        $sql = "SELECT id,nome_cliente,img,recuperar FROM `cliente` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function limparaRecuperar($id)
    {
        $sql = "UPDATE cliente SET recuperar = '' WHERE id = $id";
        $query = $this->db->prepare($sql);   
        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    
    public function nome($id)
    {
        $sql = "SELECT id,nome_cliente FROM `cliente` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function acessocliente($email)
    {
        $sql = "SELECT id,nome_cliente,senha, img FROM `cliente` WHERE email='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function atualizar($id, $nome, $sobrenome, $aniversario, $email, $telefone, $img)
    {
        $sql = "update cliente set nome_cliente = '".$nome."', sobrenome = '".$sobrenome."',aniversario = '".$aniversario."',email = '".$email."',whatsapp = '".$telefone."',img = '".$img."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }

    public function atualizarCliente($id, $nome, $email, $celular, $aniversario, $senha, $img)
    {
        $sql = "update cliente set nome_cliente = '".$nome."', email = '".$email."',whatsapp = '".$celular."',senha = '".$senha."',aniversario = '".$aniversario."',img = '".$img."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function cadastro($nome_cliente,$email, $whatsapp, $senha, $img, $tipo_cadastro, $data_cadastro, $verificacao)
    {
        $sql = "INSERT INTO cliente (nome_cliente,email, whatsapp, tipo_cadastro, data_cadastro, senha, img, verificacao) VALUES (:nome_cliente, :email, :whatsapp, :tipo_cadastro, :data_cadastro, :senha, :img, :verificacao)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome_cliente' => $nome_cliente, ':email' => $email, ':whatsapp' => $whatsapp, ':tipo_cadastro' => $tipo_cadastro, ':data_cadastro' => $data_cadastro, ':senha' => $senha, ':img' => $img, ':verificacao' => $verificacao);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function cadastroReserva($nome_cliente,$email, $whatsapp, $senha, $img, $tipo_cadastro, $data_cadastro, $verificacao)
    {
        $sql = "INSERT INTO cliente (nome_cliente,email, whatsapp, tipo_cadastro, data_cadastro, senha, img, verificacao) VALUES (:nome_cliente, :email, :whatsapp, :tipo_cadastro, :data_cadastro, :senha, :img, :verificacao)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome_cliente' => $nome_cliente, ':email' => $email, ':whatsapp' => $whatsapp, ':tipo_cadastro' => $tipo_cadastro, ':data_cadastro' => $data_cadastro, ':senha' => $senha, ':img' => $img, ':verificacao' => $verificacao);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return $this->db->lastInsertId();
        }else{
            return false;
        }
    }

    public function existeEmail($email)
    {
        $sql = "SELECT id FROM `cliente` WHERE email='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function inserir($nome_cliente, $sobrenome, $aniversario, $img, $email, $whatsapp, $tipo_cadastro, $data_cadastro)
    {
        $sql = "INSERT INTO cliente (nome_cliente, sobrenome, aniversario, img, email, whatsapp, tipo_cadastro, data_cadastro) VALUES (:nome_cliente, :sobrenome, :aniversario, :img, :email, :whatsapp, :tipo_cadastro, :data_cadastro)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome_cliente' => $nome_cliente, ':sobrenome' => $sobrenome, ':aniversario' =>$aniversario, ':img' => $img, ':email' => $email, ':whatsapp' => $whatsapp, ':tipo_cadastro' => $tipo_cadastro, ':data_cadastro' => $data_cadastro);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return array('id'=> $this->db->lastInsertId(),'boleano'=> 1);
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM cliente WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
