<?php

define('ENVIRONMENT', 'development');

if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);

/**
 * Este é o lugar onde você define suas credenciais de banco de dados, tipo de banco de dados, etc.
 */
define('DB_TYPE', 'mysql');
define('DB_HOST', 'goldtimegestao.com.br:3306');

define('DB_NAME', 'goldtime');
define('DB_USER', 'goldtime');
define('DB_PASS', 'Sup*Dig*84');

//define('DB_NAME', 'site');
//define('DB_USER', 'root');
//define('DB_PASS', '');
define('DB_CHARSET', 'utf8mb4');
