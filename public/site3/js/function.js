/* ----------------------------------------------------------------------------------------
* Author        : Awaiken
* Template Name : App Launch - App Landing Page HTML5 Template
* File          : Custom JS
* Version       : 1.0
* ---------------------------------------------------------------------------------------- */
(function ($) {
    "use strict";
	
	var $window = $(window); 
	
	/* Preloader Effect */
	$window.load(function() {
	    $(".preloader").fadeOut(600);
    });
	
	/* Top Menu */
	$('#navigation ul li a').on('click', function(){
		var id = $(this).attr('href');
		var h = parseFloat($(id).offset().top);
		$('body,html').stop().animate({
			scrollTop: h - 70
		}, 800);
		$(".navbar-collapse").collapse("hide");

		return false;
	});

	$('#menu-celular a').on('click', function(){
		var id = $(this).attr('href');
		var h = parseFloat($(id).offset().top);
		$('body,html').stop().animate({
			scrollTop: h - 70
		}, 800);
		$(".navbar-collapse").collapse("hide");

		return false;
	});
	
	/* Sticky header */
	$window.scroll(function(){
    	if ($window.scrollTop() > 200) {
			$('.navbar').addClass('sticky-header');
		} else {
			$('.navbar').removeClass('sticky-header');
		}
	});
	
	/* slick nav */
	$('#main-menu').slicknav({prependTo:'#responsive-menu',label:''});

	/*OwlCarousels Serviço Start*/
	$('#servico-slider').owlCarousel({
		loop: true,
		items: 3,
		margin: 30,
		responsiveClass: true,
		responsive : {
			0 : {
				items: 1
			},
			 
			768 : {
				items: 2
			},
			
			1024 : {
				items: 3
			}
		},
		nav: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 4000,
		navText: [ '<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
	});

	/*OwlCarousels Equipe Start*/
	$('#equipe-slider').owlCarousel({
		loop: true,
		items: 3,
		margin: 30,
		responsiveClass: true,
		responsive : {
			0 : {
				items: 1
			},
			 
			768 : {
				items: 2
			},
			
			1024 : {
				items: 3
			}
		},
		nav: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 2500,
		navText: [ '<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
	});
	
	/*OwlCarousels Testimonial Start*/
	$('#testimonial-slider').owlCarousel({
		loop: true,
		items: 3,
		margin: 30,
		responsiveClass: true,
		responsive : {
			0 : {
				items: 1
			},
			 
			768 : {
				items: 2
			},
			
			1024 : {
				items: 3
			}
		},
		nav: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 1000,
		navText: [ '<i class="fa fa-chevron-left"></i>','<i class="fa fa-chevron-right"></i>'],
	});
	
})(jQuery);

function validarEmail(email) {

	var validinput = email;
  
	var validatedAT = false;
	var validatedEND = false;
  
	// Get the text entered
	var text = validinput.value;
  
	// We will look for the @ symbol
	var AT = "@"
  
	// Acceptable endings of email
	var endings = [".com", ".edu", ".net", ".org", ".com.br"];
  
	// Look for the @ symbol 
	if (text.indexOf(AT) > 0) {
	  // Symbolis there
	  validatedAT = true;
	}
  
	// Check if email ends with one of the accepted emails
	for (var i = 0; i < endings.length; i++) {
  
	  if (text.indexOf(endings[i]) + endings[i].length === text.length) {
		console.log(text.indexOf(endings[i]), endings[i].length, text.length, endings[i]);
		validatedEND = true;
	  }
  
	}
  
	// Updates validation
	if (validatedAT && validatedEND) {
	  return true;
	} else {
	  return false;
	}
  
  }