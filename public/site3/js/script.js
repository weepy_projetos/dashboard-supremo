// AJAX 

var url = document.location.origin;
if (url == "https://goldtimegestao.com.br") {
  var url = document.location.origin + "/goldtime";
} else{
  var url = document.location.origin + "/weepy/dashboard-supremo";
}

$('.evento-fechar-acordiao').click(function () { 
  var teste =$('.fechar-acordiao input[type="checkbox"]');
  teste.prop('checked', false);
  $('.valor-total-servico').hide();
});
/*==================================
          
====================================*/
$('.btn-reserva').click(function (e) { 
  e.preventDefault();

  //id form form-reserva-id
  var idform = $(this).attr('idform');
  var form = '#form-reserva-id'+idform;

  var listaServicos = [];
  $.each($("input[name='servico[]']:checked"), function(){
    listaServicos.push($(this).val());
  });

  if (listaServicos.length == 0) {
    $(form).find('.mensagem').show('slow');
    $(form).find('.mensagem').addClass('erro');
  } else {
    $("#login-reserva-modal").modal();
    $(form).find('.mensagem').hide();
  }

  //passar para o btn login
  $('#login-cliente-reservas').attr('idform', idform);
  $('#cadastro-cliente-reserva').attr('idform', idform);
});

$('#login-cliente-reservas').click(function (e) { 
  e.preventDefault();

  var evento = this;
  console.log(evento);
  $(evento).toggleClass('button--loading');

  var idform = $(this).attr('idform');
  var form = '#form-reserva-id'+idform;

  var listaServicos = [];
  $.each($(form + " input[name='servico[]']:checked"), function(){
    listaServicos.push($(this).val());
  });

  var dados = new FormData();
  dados.append('servico', JSON.stringify(listaServicos));
  dados.append('observacao', $(form + ' [name="observacao"]').val());
  dados.append('profissional', $(form + ' [name="profissional"]').val());
  dados.append('email', $('#form-login-reservas [name="email"]').val());
  dados.append('senha', $('#form-login-reservas [name="senha"]').val());

  $.ajax({
    url: url + "/login/analiseReserva",
    method: 'post',
    data: dados,
    cache: false,
    contentType: false,
    processData: false,
    dataType: "html",
    success: function (retorno) {
      //$('#form-login-reservas').html(retorno);
      var msgModal = JSON.parse(retorno);
      if (msgModal["boleano"]) {
        $('#form-login-reservas').find('.mensagem').show('slow');
        $('#form-login-reservas').find('.mensagem').addClass('sucesso');
        $('#form-login-reservas').find('.mensagem').text(msgModal["msg"]);
        setTimeout(function(){ window.location.href = ''; }, 2500);
      } else {
        $('#form-login-reservas').find('.mensagem').show('slow');
        $('#form-login-reservas').find('.mensagem').addClass('erro');
        $('#form-login-reservas').find('.mensagem').text(msgModal["msg"])
      }
    },
    beforeSend: function () {
     
    },
    complete: function (msg) {
      $(evento).toggleClass('button--loading');
    }
  });
  
});

$('#cadastro-cliente-reserva').click(function (e) { 
  e.preventDefault();

  var evento = this;
  $(evento).toggleClass('button--loading');

  var idform = $(this).attr('idform');
  var form = '#form-reserva-id'+idform;

  var listaServicos = [];
  $.each($(form + " input[name='servico[]']:checked"), function(){
    listaServicos.push($(this).val());
  });

  var dados = new FormData();
  dados.append('servico', JSON.stringify(listaServicos));
  dados.append('observacao', $(form + ' [name="observacao"]').val());
  dados.append('profissional', $(form + ' [name="profissional"]').val());
  dados.append('nome', $('#form-cadastro-cliente-reserva [name="nome"]').val());
  dados.append('celular', $('#form-cadastro-cliente-reserva [name="celular"]').val());
  dados.append('email', $('#form-cadastro-cliente-reserva [name="email_cadastro"]').val());
  dados.append('senha', $('#form-cadastro-cliente-reserva [name="senha_cadastro"]').val());

  var email = document.getElementById("email-reserva");

  if (validarEmail(email)) {
  
    $(evento).prop('disabled', false);

    var senha = $("#senha-cadastro-reserva").val();
    var repita_senha = $("#senha-repita-reserva").val();

    if (senha == repita_senha) {

      $(form).find('.mensagem').hide();

      $.ajax({
        url: url + "/cliente/cadastroReserva",
        method: 'post',
        data: dados,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html",
        success: function (retorno) {
          var msgModal = JSON.parse(retorno);
          if (msgModal) {
            $('#form-cadastro-cliente-reserva').find('.mensagem').show('slow');
            $('#form-cadastro-cliente-reserva').find('.mensagem').addClass('sucesso');
            $('#form-cadastro-cliente-reserva').find('.mensagem').text('Reserva e cadastro feito com sucesso.');
          } else {
            $('#form-cadastro-cliente-reserva').find('.mensagem').show('slow');
            $('#form-cadastro-cliente-reserva').find('.mensagem').addClass('erro');
            $('#form-cadastro-cliente-reserva').find('.mensagem').text('Email já cadastrado');
          }
        },
        beforeSend: function () {

        },
        complete: function (msg) {
          $(evento).toggleClass('button--loading');
        }
      });
    } else {
      $('#form-cadastro-cliente-reserva').find('.mensagem').show('slow');
      $('#form-cadastro-cliente-reserva').find('.mensagem').addClass('erro');
      $('#form-cadastro-cliente-reserva').find('.mensagem').text('Senha não são iguais');
      $(evento).toggleClass('button--loading');
    }

  } else {
    $('#form-cadastro-cliente-reserva').find('.mensagem').show('slow');
    $('#form-cadastro-cliente-reserva').find('.mensagem').addClass('erro');
    $('#form-cadastro-cliente-reserva').find('.mensagem').text('Email inválido');
    $(evento).toggleClass('button--loading');
  }
  
});


/*==================================
          SOMAR CHECKBOX
====================================*/
$("input[name='servico[]']").change(function () { 
  $('.valor-total-servico').show('slow');

  var listaServicos = [];
  $.each($("input[name='servico[]']:checked"), function(){
    listaServicos.push($(this).attr('valor'));
  });

  $('.valor').text(eval(listaServicos.join('+')));
    
});

/*==================================
          VALIDAR RESERVA
====================================*/

var profissional = false;

$('input[name="servico[]"').change(function (e) { 
  e.preventDefault();
  var form = $('.form-reserva');

  $('.foto-reserva').hide();

  var listaServicos = [];
  $.each($("input[name='servico[]']:checked"), function(){
    listaServicos.push($(this).val());
  });

  var dados = new FormData();
  dados.append('servico', JSON.stringify(listaServicos));

  $.ajax({
    url: url + "/equipe/consultaProfissional",
    method: 'post',
    data: dados,
    dataType: "html",
    cache: false,
    contentType: false,
    processData: false,
    success: function (resultado) {
      if (resultado == 0 ) { 
        $('#ProfissionalEscolha').prop('disabled', true);
        $('.foto-reserva').hide();
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').text('Não temos profissionais para esse serviço.');
        $('.is-active').animate({scrollTop: 0 },1000,'linear');
        profissional = false;
      } else {
        $('#ProfissionalEscolha').prop('disabled', false);
        $('#reservaObs').prop('disabled', false);
        $('.foto-reserva').show('slow');
        $(form).find('.mensagem').hide();
        $('.is-active').animate({scrollTop: 800 },1000,'linear');
        profissional = true;
      }
    }
  });
});

/*==================================
          MUDAR FOTO RESERVA
====================================*/
$(document).ready(function() {
  $( "#ProfissionalEscolha" ).change(function() {
    var evento = this;
    var img = $('#ProfissionalEscolha option:selected').attr('imagem');
    $('#profissionalFoto').attr('src', img);
  });
});

/*==================================
          AVANÇAR RESERVA
====================================*/
$(document).ready(function() {

  console.log(profissional);
  $('.avancar-form').click(function (e) { 
    e.preventDefault();
    var form = $('.form-wrapper');
    
    // CHECKE SERVICO TRUE
    var servico = $('input[name="servico[]"]');

    for (i = 0; i < servico.length; i++) {
      if(servico[i].checked == true){
        var servicos= true;
        console.log(servicos);
      }
      break;
    }

    /*if (servicos) {
      $('select[name="profissional"]').prop('disabled',false);
      $('.is-active').animate({scrollTop: 0 },1000,'linear');
    } else {
      $(form).find('.mensagem').show('slow');
      $(form).find('.mensagem').addClass('erro');
      $(form).find('.mensagem').text('Escolha o serviço');
    }*/

    if (profissional) {
      var button = $(this);
      var currentSection = button.parents(".section");
      var currentSectionIndex = currentSection.index();
      var headerSection = $('.steps li').eq(currentSectionIndex);
      currentSection.removeClass("is-active").next().addClass("is-active");
      headerSection.removeClass("is-active").next().addClass("is-active");

      $(".form-wrapper").submit(function(e) {
        e.preventDefault();
      });

      if (currentSectionIndex === 3) {
        $(document).find(".form-wrapper .section").first().addClass("is-active");
        $(document).find(".steps li").first().addClass("is-active");
      }
    }
  
  }); 
});

function avancarReserva() {
      var button = $(this);
      var currentSection = button.parents(".section");
      var currentSectionIndex = currentSection.index();
      var headerSection = $('.steps li').eq(currentSectionIndex);
      currentSection.removeClass("is-active").next().addClass("is-active");
      headerSection.removeClass("is-active").next().addClass("is-active");

      $(".form-wrapper").submit(function(e) {
        e.preventDefault();
      });

      if (currentSectionIndex === 3) {
        $(document).find(".form-wrapper .section").first().addClass("is-active");
        $(document).find(".steps li").first().addClass("is-active");
      }
}

/*==================================
          FORM CADASTRO
====================================*/
$("#cadastro-cliente-reservaaaa").click(function () {
  event.preventDefault();

  var evento = this;
  var form = $('.form-reserva');
  $(evento).toggleClass('button--loading');

  var email = document.getElementById("emailCadastroo");

  if (validarEmail(email)) {
  
    $(evento).prop('disabled', false);

    var senha = $("#senha-cadastro").val();
    var repita_senha = $("input[name=repita-senha]").val();

    if (senha == repita_senha) {

      $(form).find('.mensagem').hide();

      $.ajax({
        url: url + "/cliente/cadastroReserva",
        method: 'post',
        data: new FormData($('.form-reserva')[0]),
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html",
        success: function (retorno) {
          $('.criarconta').html(retorno);
          var msgModal = JSON.parse(retorno);
          if (msgModal) {
            $(form).find('.mensagem').show('slow');
            $(form).find('.mensagem').addClass('sucesso');
            $(form).find('.mensagem').text('Reserva e cadastro feito com sucesso.');
          } else {
            $(form).find('.mensagem').show('slow');
            $(form).find('.mensagem').addClass('erro');
            $(form).find('.mensagem').text('Email já cadastrado');
          }
        },
        beforeSend: function () {

        },
        complete: function (msg) {
          $(evento).toggleClass('button--loading');
        }
      });
    } else {
      $(form).find('.mensagem').show('slow');
      $(form).find('.mensagem').addClass('erro');
      $(form).find('.mensagem').text('Senha não são iguais');
      $(evento).toggleClass('button--loading');
    }

  } else {
    $(form).find('.mensagem').show('slow');
    $(form).find('.mensagem').addClass('erro');
    $(form).find('.mensagem').text('Email inválido');
    $(evento).toggleClass('button--loading');
  }

});
$("#cadastro-cliente").click(function () {
  event.preventDefault();

  var evento = this;
  var form = $('#form-cadastro-cliente');
  $(evento).toggleClass('button--loading');

  if (validarEmail()) {
  
    $(evento).prop('disabled', false);

    var senha = $("#senha-cadastro").val();
    var repita_senha = $("input[name=repita-senha]").val();

    if (senha == repita_senha) {

      $(form).find('.mensagem').hide();

      $.ajax({
        url: url + "/cliente/cadastro",
        method: 'post',
        data: new FormData($('#form-cadastro-cliente')[0]),
        cache: false,
        contentType: false,
        processData: false,
        dataType: "html",
        success: function (retorno) {
          var msgModal = JSON.parse(retorno);
          if (msgModal) {
            $(form).find('.mensagem').show('slow');
            $(form).find('.mensagem').addClass('sucesso');
            $(form).find('.mensagem').text('Cadastro feito');
          } else {
            $(form).find('.mensagem').show('slow');
            $(form).find('.mensagem').addClass('erro');
            $(form).find('.mensagem').text('Email já cadastrado');
          }
        },
        beforeSend: function () {

        },
        complete: function (msg) {
          $(evento).toggleClass('button--loading');
        }
      });
    } else {
      $(form).find('.mensagem').show('slow');
      $(form).find('.mensagem').addClass('erro');
      $(form).find('.mensagem').text('Senha não são iguais');
      $(evento).toggleClass('button--loading');
    }

  } else {
    $(form).find('.mensagem').show('slow');
    $(form).find('.mensagem').addClass('erro');
    $(form).find('.mensagem').text('Email inválido');
    $(evento).toggleClass('button--loading');
  }

});

/*==================================
          FORM LOGIN
====================================*/
$("#login-cliente-reserva").click(function () {
  event.preventDefault();

  var evento = this;
  var form = $('.form-reserva');
  $(evento).toggleClass('button--loading');

  $(form).find('.mensagem').hide();

  $.ajax({
    url: url + "/login/analiseReserva",
    method: 'post',
    data: new FormData($('.form-reserva')[0]),
    cache: false,
    contentType: false,
    processData: false,
    dataType: "html",
    success: function (retorno) {
      var msgModal = JSON.parse(retorno);
      if (msgModal["boleano"]) {
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('sucesso');
        $(form).find('.mensagem').text(msgModal["msg"])
      } else {
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').text(msgModal["msg"])
      }
    },
    beforeSend: function () {

    },
    complete: function (msg) {
      $(evento).toggleClass('button--loading');
    }
  });
});
$("#login-cliente").click(function () {
  event.preventDefault();

  var evento = this;
  var form = $('#form-login');
  $(evento).toggleClass('button--loading');

  $(form).find('.mensagem').hide();

  $.ajax({
    url: url + "/login/acessoCliente",
    method: 'post',
    data: new FormData($('#form-login')[0]),
    cache: false,
    contentType: false,
    processData: false,
    dataType: "html",
    success: function (retorno) {
      $('.teste').html(retorno);
      var msgModal = JSON.parse(retorno);
      if (msgModal["boleano"]) {
        window.location.href = '';
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('sucesso');
        $(form).find('.mensagem').text(msgModal["msg"])
      } else {
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').text(msgModal["msg"])
      }
    },
    beforeSend: function () {

    },
    complete: function (msg) {
      $(evento).toggleClass('button--loading');
    }
  });
});

$("#recuperar-cliente").click(function () {
  event.preventDefault();

  var evento = this;
  var form = $('#form-recuperar-cliente');

  $(evento).prop('disabled', true);
  $(evento).toggleClass('button--loading');

  $(form).find('.mensagem').hide('slow');

  $.ajax({
    url: url + "/login/recuperarSenhaCliente",
    method: 'post',
    data: new FormData($('#form-recuperar-cliente')[0]),
    cache: false,
    contentType: false,
    processData: false,
    dataType: "html",
    success: function (retorno) {
      var msgModal = JSON.parse(retorno);
      if (msgModal) {
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('sucesso');
        $(form).find('.mensagem').text('Enviamos para seu E-mail');
        $(evento).prop('disabled', false);
      } else {
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').text('Não existe esse Email cadastrado');
        $(evento).prop('disabled', false);
      }
    },
    beforeSend: function () {

    },
    complete: function (msg) {
      $(evento).toggleClass('button--loading');
    }
  });
});

// set the first nav item as active
var dis = $(".list-wrap li").eq(0);

// align the wave
align(dis);

// align the wave on click
$(".list-wrap li").click(function () {
  dis = $(this);

  align(dis);
});

$('body').on('keydown', function (e) {
  var code = e.keyCode || e.which;

  if (code == 9) {

    if (dis.is(':last-child')) {
      $(".list-wrap li:nth-child(1)").trigger("click");
    }
    else {
      dis.next().trigger("click");
    }

  }
});

$("body").keydown(function (e) {
  if (e.keyCode == 37) { // left
    $("#showroom").animate({
      left: "-=980"
    });
  }
  else if (e.keyCode == 39) { // right
    $("#showroom").animate({
      left: "+=980"
    });
  }
});

function align(dis) {

  // get index of item
  var index = dis.index() + 1;

  // add active class to the new item
  $(".list-wrap li").removeClass("active");
  dis.delay(100).queue(function () {
    dis.addClass('active').dequeue();
  });

  largura = screen.width;;
  largura = largura / 5;
  // move the wave
  var left = largura + largura;//* 80 - 98;

  $("#wave").css('left', left);

  // ▼ this is not necessary for the navigation ▼

}

function ReservaAvancar() {
  $(document).ready(function() {
    $(".form-wrapper .avancar-form").click(function() {
      var button = $(this);
      var currentSection = button.parents(".section");
      var currentSectionIndex = currentSection.index();
      var headerSection = $('.steps li').eq(currentSectionIndex);
      currentSection.removeClass("is-active").next().addClass("is-active");
      headerSection.removeClass("is-active").next().addClass("is-active");

      $(".form-wrapper").submit(function(e) {
        e.preventDefault();
      });

      if (currentSectionIndex === 3) {
        $(document).find(".form-wrapper .section").first().addClass("is-active");
        $(document).find(".steps li").first().addClass("is-active");
      }
    });
  });
}

