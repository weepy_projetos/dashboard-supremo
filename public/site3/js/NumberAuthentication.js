
window.onload = function () {
    render();
};
function render() {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    recaptchaVerifier.render();
}

var form = $('#form-cadastro-cliente');

function phoneAuth() {

    $('#processo-enviar').html('<span class="glyphicon glyphicon-refresh processando-button"></span> Processando...');

    //get the number
    var number = document.getElementById('number').value;
    var numero = '+55' + number;
    //phone number authentication function of firebase
    //it takes two parameter first one is number,,,second one is recaptcha
    var recaptchaDesativado = firebase.auth().signInWithPhoneNumber(numero, window.recaptchaVerifier).then(function (confirmationResult) {
        //s is in lowercase
        window.confirmationResult = confirmationResult;
        coderesult = confirmationResult;
        console.log(coderesult);
        $('#recaptcha-container').hide('slow');
        $('#enviar-code').hide('slow');
        $('#verificar-code').show('slow');

        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('sucesso');
        $(form).find('.mensagem').text('SMS Enviado. Verifique')
    }).catch(function (error) {
        $('#processo-enviar').html('<span id="processo-enviar">Enviar código</span>');
        if (error.message == 'TOO_LONG') {
            $(form).find('.mensagem').hide();
            $(form).find('.mensagem').show('slow');
            $(form).find('.mensagem').addClass('erro');
            $(form).find('.mensagem').text('Verifique o número novamente')
        } else if(error.message == 'Invalid format.'){
            $(form).find('.mensagem').hide();
            $(form).find('.mensagem').show('slow');
            $(form).find('.mensagem').addClass('erro');
            $(form).find('.mensagem').text('Formato inválido')
        }
        console.log(error.message);
        
    });
   
}
function codeverify() {
    var code = document.getElementById('verificationCode').value;

    $('#verificar-processo').html('<span class="glyphicon glyphicon-refresh processando-button"></span> Processando...');

    coderesult.confirm(code).then(function (result) {
        //alert("Successfully registered");
        $('#verificar-code').hide('slow');
        $('#cadastro').show('slow');

        $(form).find('.mensagem').hide();
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('sucesso');
        $(form).find('.mensagem').text('Código verificado com sucesso')
        var user = result.user;
        console.log(user);
    }).catch(function (error) {
        $('#verificar-processo').html('<span id="verificar-processo">Verificar codigo</span>');
        $(form).find('.mensagem').hide();
        $(form).find('.mensagem').removeClass('sucesso');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').text('Código inválido, tente novamente')
        $('#verificar-code').show('slow');
        $('#cadastro').hide('slow');
        console.log(error.message);
    });
}