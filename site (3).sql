-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2020 at 03:07 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `site`
--

-- --------------------------------------------------------

--
-- Table structure for table `categoria_servico`
--

CREATE TABLE `categoria_servico` (
  `id` int(11) NOT NULL,
  `categoria` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categoria_servico`
--

INSERT INTO `categoria_servico` (`id`, `categoria`) VALUES
(1, 'Cabelo'),
(2, 'Barba'),
(3, 'Estética facil'),
(4, 'Manicure e pedicure'),
(5, 'Maquiagem');

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `nome_cliente` varchar(20) NOT NULL,
  `sobrenome` varchar(20) NOT NULL,
  `aniversario` date NOT NULL,
  `img` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `whatsapp` varchar(14) NOT NULL,
  `tipo_cadastro` varchar(15) NOT NULL,
  `data_cadastro` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id`, `nome_cliente`, `sobrenome`, `aniversario`, `img`, `email`, `whatsapp`, `tipo_cadastro`, `data_cadastro`) VALUES
(1, 'Gemaque', 'Barros', '0000-00-00', 'usuario.jpg', '', '0', 'Presencial', '0000-00-00'),
(2, 'Douglas', 'Santos', '0000-00-00', 'usuario.jpg', '', '0', 'Online', '0000-00-00'),
(3, 'Douglas', 'alves', '0000-00-00', 'usuario.jpg', '', '', 'Online', '0000-00-00'),
(5, 'Allepalmeira', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-09'),
(6, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-10'),
(7, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-10'),
(8, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-10-03'),
(9, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-10'),
(10, 'hjghjghj', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-11'),
(11, 'Marcos', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '0000-00-00'),
(12, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-14'),
(15, 'Silva silva', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-13'),
(16, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-14'),
(17, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-14'),
(18, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '1970-01-01'),
(19, 'Palmeira', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-14'),
(20, 'icaro', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-14'),
(21, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-16'),
(22, 'Marcos', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-17'),
(23, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-20'),
(24, 'Icaro Sammer', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-20'),
(25, 'Allepalmeira', '', '0000-00-00', 'usuario.jpg', '', '', 'Presencial', '2020-03-20');

-- --------------------------------------------------------

--
-- Table structure for table `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `assunto` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `mensagem` text NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contato`
--

INSERT INTO `contato` (`id`, `nome`, `assunto`, `email`, `mensagem`, `telefone`, `data`) VALUES
(1, 'icaro', 'teste', 'icaro.sammer@gmail.com', '', '1190000-0000', '2020-01-02');

-- --------------------------------------------------------

--
-- Table structure for table `controler_nivel`
--

CREATE TABLE `controler_nivel` (
  `id` int(11) NOT NULL,
  `controler` varchar(30) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `cabeleireiro` tinyint(1) NOT NULL,
  `atendente` tinyint(1) NOT NULL,
  `editor` tinyint(1) NOT NULL,
  `autor` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `controler_nivel`
--

INSERT INTO `controler_nivel` (`id`, `controler`, `admin`, `cabeleireiro`, `atendente`, `editor`, `autor`) VALUES
(1, 'EquipeController', 1, 0, 0, 1, 0),
(2, 'ServicoHomemController', 1, 0, 0, 1, 0),
(3, 'SliderController', 1, 0, 0, 0, 0),
(4, 'DepoimentoController', 1, 0, 0, 0, 0),
(5, 'ClienteController', 1, 1, 1, 0, 0),
(6, 'ContatoController', 1, 0, 1, 0, 0),
(7, 'RelatoriosController', 1, 0, 0, 0, 0),
(8, 'InformacaoController', 1, 0, 0, 0, 0),
(9, 'PaginasController', 1, 0, 0, 0, 0),
(10, 'UsuarioController', 1, 1, 0, 0, 0),
(11, 'PainelController', 1, 1, 1, 0, 0),
(12, 'ReservaController', 1, 1, 1, 0, 0),
(13, 'ReservaTempoController', 1, 1, 1, 0, 0),
(14, 'perfil', 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `depoimento`
--

CREATE TABLE `depoimento` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) DEFAULT NULL,
  `profissao` varchar(30) DEFAULT NULL,
  `depoimento` text NOT NULL,
  `avaliacao` varchar(1) NOT NULL,
  `img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `depoimento`
--

INSERT INTO `depoimento` (`id`, `nome`, `profissao`, `depoimento`, `avaliacao`, `img`) VALUES
(1, 'Icaro Sammer', 'web', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', '4', 'face16.jpg'),
(5, 'Icaro Sammer', 'fghfg', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', '5', 'face1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `destaque`
--

CREATE TABLE `destaque` (
  `id` int(11) NOT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `subtitulo` varchar(70) DEFAULT NULL,
  `descricao` text DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `destaque`
--

INSERT INTO `destaque` (`id`, `titulo`, `subtitulo`, `descricao`, `img`, `alt`) VALUES
(44, 'Títuloo', NULL, 'jkljklj', 'images.jpg', 'alt1'),
(58, 'novo', NULL, 'novo descrição', 'pp.jpg', 'novo alt');

-- --------------------------------------------------------

--
-- Table structure for table `distritos_sp`
--

CREATE TABLE `distritos_sp` (
  `id` int(11) NOT NULL,
  `regiao` varchar(10) NOT NULL,
  `distrito` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `distritos_sp`
--

INSERT INTO `distritos_sp` (`id`, `regiao`, `distrito`) VALUES
(1, 'leste', 'Água Rasa'),
(2, 'leste', 'Aricanduva'),
(3, 'leste', 'Artur Alvim'),
(4, 'leste', 'Brás'),
(5, 'leste', 'Belém'),
(6, 'leste', 'Cangaíba'),
(7, 'leste', 'Carrão'),
(8, 'leste', 'Mooca'),
(9, 'leste', 'Pari'),
(10, 'leste', 'Penha'),
(11, 'leste', 'São Lucas'),
(12, 'leste', 'Sapopemba'),
(13, 'leste', 'Tatuapé'),
(14, 'leste', 'Vila Formosa'),
(15, 'leste', 'Vila Matilde'),
(16, 'leste', 'Vila Prudente'),
(17, 'leste', 'Cidade Líder'),
(18, 'leste', 'Cidade Tiradentes'),
(19, 'leste', 'Ermelino Matarazzo'),
(20, 'leste', 'Guaianases'),
(21, 'leste', 'Iguatemi'),
(22, 'leste', 'Itaim Paulista'),
(23, 'leste', 'Itaquera'),
(24, 'leste', 'Jardim Helena'),
(25, 'leste', 'José Bonifácio'),
(26, 'leste', 'Lajeado'),
(27, 'leste', 'Parque do Carmo'),
(28, 'leste', 'Ponte Rasa'),
(29, 'leste', 'São Mateus'),
(30, 'leste', 'São Miguel'),
(31, 'leste', 'São Rafael'),
(32, 'leste', 'Vila Curuçá'),
(33, 'leste', 'Vila Jacuí'),
(34, 'sul', 'Campo Belo'),
(35, 'sul', 'Campo Grande'),
(36, 'sul', 'Cursino'),
(37, 'sul', 'Ipiranga'),
(38, 'sul', 'Jabaquara'),
(39, 'sul', 'Moema'),
(40, 'sul', 'Sacomã'),
(41, 'sul', 'Santo Amaro'),
(42, 'sul', 'Saúde'),
(43, 'sul', 'Vila Mariana'),
(44, 'sul', 'Campo Limpo'),
(45, 'sul', 'Capão Redondo'),
(46, 'sul', 'Capela do Socorro'),
(47, 'sul', 'Cidade Ademar'),
(48, 'sul', 'Cidade Dutra'),
(49, 'sul', 'Grajaú'),
(50, 'sul', 'Jardim Ângela'),
(51, 'sul', 'Jardim São Luís'),
(52, 'sul', 'Marsilac'),
(53, 'sul', 'Parelheiros'),
(54, 'sul', 'Pedreira'),
(55, 'sul', 'Vila Andrade'),
(56, 'oeste', 'Alto de Pinheiros'),
(57, 'oeste', 'Barra Funda'),
(58, 'oeste', 'Butantã'),
(59, 'oeste', 'Itaim Bibi'),
(60, 'oeste', 'Jaguara'),
(61, 'oeste', 'Jaguaré'),
(62, 'oeste', 'Jardim Paulista'),
(63, 'oeste', 'Lapa'),
(64, 'oeste', 'Morumbi'),
(65, 'oeste', 'Perdizes'),
(66, 'oeste', 'Pinheiros'),
(67, 'oeste', 'Raposo Tavares'),
(68, 'oeste', 'Rio Pequeno'),
(69, 'oeste', 'Vila Leopoldina'),
(70, 'oeste', 'Vila Sônia'),
(71, 'norte', 'Jaçanã'),
(72, 'norte', 'Mandaqui'),
(73, 'norte', 'Santana'),
(74, 'norte', 'Tremembé'),
(75, 'norte', 'Tucuruvi'),
(76, 'norte', 'Vila Guilherme'),
(77, 'norte', 'Vila Maria'),
(78, 'norte', 'Vila Medeiros'),
(79, 'norte', 'Anhangüera'),
(80, 'norte', 'Brasilândia'),
(81, 'norte', 'Cachoeirinha'),
(82, 'norte', 'Casa Verde'),
(83, 'norte', 'Freguesia do Ó'),
(84, 'norte', 'Jaraguá'),
(85, 'norte', 'Limão'),
(86, 'norte', 'Perus'),
(87, 'norte', 'Pirituba'),
(88, 'norte', 'São Domingos'),
(89, 'centro', 'Bela Vista'),
(90, 'centro', 'Bom Retiro'),
(91, 'centro', 'Cambuci'),
(92, 'centro', 'Consolação'),
(93, 'centro', 'Liberdade'),
(94, 'centro', 'República'),
(95, 'centro', 'Santa Cecília'),
(96, 'centro', 'Sé');

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `servicos` text DEFAULT NULL,
  `horario` varchar(70) DEFAULT NULL,
  `Monday` tinyint(1) NOT NULL,
  `Tuesday` tinyint(1) NOT NULL,
  `Wednesday` tinyint(1) NOT NULL,
  `Thursday` tinyint(1) NOT NULL,
  `Friday` tinyint(1) NOT NULL,
  `Saturday` tinyint(1) NOT NULL,
  `Sunday` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`id`, `id_usuario`, `servicos`, `horario`, `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday`, `Saturday`, `Sunday`) VALUES
(2, 0, 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"9\";}', NULL, 0, 0, 0, 0, 0, 0, 0),
(19, 1, 'a:3:{i:0;a:1:{s:6:\"Cabelo\";a:1:{i:0;s:1:\"7\";}}i:1;a:1:{s:5:\"Barba\";a:1:{i:0;s:1:\"6\";}}i:2;a:1:{s:15:\"Estética facil\";b:0;}}', NULL, 0, 1, 1, 1, 1, 1, 1),
(21, 22, NULL, NULL, 0, 0, 0, 0, 0, 0, 0),
(23, 24, NULL, NULL, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `galeria`
--

CREATE TABLE `galeria` (
  `id` int(11) NOT NULL,
  `categoria` varchar(30) DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `gatilhos`
--

CREATE TABLE `gatilhos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `descricao` text NOT NULL,
  `botao_1` varchar(20) DEFAULT NULL,
  `botao_2` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `gatilhos`
--

INSERT INTO `gatilhos` (`id`, `titulo`, `descricao`, `botao_1`, `botao_2`) VALUES
(1, 'título1', 'título1', NULL, NULL),
(2, 'título2', 'título2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `informacao_contato`
--

CREATE TABLE `informacao_contato` (
  `id` int(11) NOT NULL,
  `endereco` varchar(70) DEFAULT NULL,
  `bairro` varchar(70) DEFAULT NULL,
  `cep` varchar(70) DEFAULT NULL,
  `numero` varchar(5) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `celular` varchar(14) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `facebook` varchar(70) DEFAULT NULL,
  `linkedin` varchar(70) DEFAULT NULL,
  `youtube` varchar(70) DEFAULT NULL,
  `twitter` varchar(60) NOT NULL,
  `instagram` varchar(70) DEFAULT NULL,
  `abertura` time NOT NULL,
  `fechamento` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `informacao_contato`
--

INSERT INTO `informacao_contato` (`id`, `endereco`, `bairro`, `cep`, `numero`, `telefone`, `celular`, `whatsapp`, `email`, `facebook`, `linkedin`, `youtube`, `twitter`, `instagram`, `abertura`, `fechamento`) VALUES
(1, 'endereço', NULL, '', '', '', '', '', '@', 'facebook', 'linkedin', 'youtube', 'twitter', 'instagram', '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `metodo_nivel`
--

CREATE TABLE `metodo_nivel` (
  `id` int(11) NOT NULL,
  `id_controller` int(11) NOT NULL,
  `metodo` varchar(30) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `cabeleireiro` tinyint(1) NOT NULL,
  `atendente` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `metodo_nivel`
--

INSERT INTO `metodo_nivel` (`id`, `id_controller`, `metodo`, `admin`, `cabeleireiro`, `atendente`) VALUES
(1, 1, 'editar', 1, 0, 0),
(2, 2, 'atualizar', 1, 0, 0),
(3, 3, 'deletar', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `niveis`
--

CREATE TABLE `niveis` (
  `id` int(11) NOT NULL,
  `nivel` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `niveis`
--

INSERT INTO `niveis` (`id`, `nivel`) VALUES
(1, 'admin'),
(2, 'Editor'),
(3, 'cabeleireiro'),
(4, 'atendente');

-- --------------------------------------------------------

--
-- Table structure for table `notificacao`
--

CREATE TABLE `notificacao` (
  `id` int(11) NOT NULL,
  `categoria` varchar(20) NOT NULL,
  `descricao` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notificacao`
--

INSERT INTO `notificacao` (`id`, `categoria`, `descricao`) VALUES
(1, 'Categoria1', 'Texto'),
(2, 'Categoria2', 'texto'),
(3, 'Categoria1', 'Texto'),
(4, 'Categoria2', 'texto'),
(13, 'Categoria2', 'texto'),
(14, 'Categoria2', 'texto'),
(15, 'categoria', 'cliente'),
(16, 'categoria', 'cliente'),
(17, 'categoria', 'cliente'),
(18, 'categoria', 'cliente'),
(19, 'categoria', 'cliente'),
(20, 'categoria', 'cliente'),
(21, 'categoria', 'cliente'),
(22, 'categoria', 'cliente'),
(23, 'categoria', 'cliente'),
(24, 'categoria', 'cliente'),
(25, 'categoria', 'cliente'),
(26, 'categoria', 'cliente'),
(27, 'categoria', 'cliente'),
(28, 'categoria', 'cliente'),
(29, 'categoria', 'cliente'),
(30, 'categoria', 'cliente'),
(31, 'categoria', 'cliente'),
(32, 'categoria', 'cliente'),
(33, 'categoria', 'cliente'),
(34, 'categoria', 'cliente'),
(35, 'categoria', 'cliente'),
(36, 'categoria', 'cliente'),
(37, 'categoria', 'cliente'),
(38, 'categoria', 'cliente'),
(39, 'categoria', 'cliente');

-- --------------------------------------------------------

--
-- Table structure for table `paginas`
--

CREATE TABLE `paginas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `subtitulo` varchar(70) DEFAULT NULL,
  `descricao` text NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL,
  `meta_titulo` varchar(70) DEFAULT NULL,
  `meta_descricao` varchar(156) DEFAULT NULL,
  `data_alteracao` varchar(20) DEFAULT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `compartilhamento` tinyint(1) NOT NULL,
  `gatilho` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `subtitulo`, `descricao`, `img`, `alt`, `meta_titulo`, `meta_descricao`, `data_alteracao`, `usuario`, `compartilhamento`, `gatilho`) VALUES
(1, 'Sobre o Salão Daniel', '', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', 'img-upload.png', 'Alt', 'Salão do Daniel | Faça sua reserva!', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição.', '05/02/2020 16:48', 'admin', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `paginas_restauracao`
--

CREATE TABLE `paginas_restauracao` (
  `id` int(11) NOT NULL,
  `id_pagina` int(11) NOT NULL,
  `titulo` varchar(70) NOT NULL,
  `subtitulo` varchar(70) DEFAULT NULL,
  `descricao` text NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL,
  `meta_titulo` varchar(70) DEFAULT NULL,
  `meta_descricao` varchar(156) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `data_alteracao` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `paginas_restauracao`
--

INSERT INTO `paginas_restauracao` (`id`, `id_pagina`, `titulo`, `subtitulo`, `descricao`, `img`, `alt`, `meta_titulo`, `meta_descricao`, `usuario`, `data_alteracao`) VALUES
(1, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 01:39'),
(2, 1, 'Títuloo', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 19:05'),
(3, 1, 'Título3', '', 'Descrição3', 'EMUPkcyXUAAdzhb.jpg', 'Alt3', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 19:16'),
(4, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 15:20'),
(5, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 15:26'),
(6, 1, 'Título6', '', 'Descrição6', 'EMUPkcyXUAAdzhb.jpg', 'Alt6', '', '', 'Ícaro', '27/12/2019 15:30'),
(7, 1, 'Título7', '', 'Descrição7', 'EMUPkcyXUAAdzhb.jpg', 'Alt7', 'meta titulo7', 'meta descricao7', 'Ícaro', '27/12/2019 15:31'),
(8, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 16:28'),
(9, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta descricao', 'Ícaro', '27/12/2019 16:28'),
(10, 1, 'Títuloo', '', 'teste', 'EMUPkcyXUAAdzhb.jpg', 'Altt', 'meta titulo', 'meta', 'admin', '01/01/2020 11:48'),
(11, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta', 'admin', '01/01/2020 11:51'),
(12, 1, 'Título', '', 'Descrição', 'EMUPkcyXUAAdzhb.jpg', 'Alt', 'meta titulo', 'meta', 'admin', '01/01/2020 11:52');

-- --------------------------------------------------------

--
-- Table structure for table `reserva`
--

CREATE TABLE `reserva` (
  `id` int(11) NOT NULL,
  `ordem` int(11) NOT NULL,
  `id_cliente` int(20) NOT NULL,
  `servico` varchar(70) NOT NULL,
  `observacao` text NOT NULL,
  `id_usuario` int(20) NOT NULL,
  `estatus` varchar(20) NOT NULL,
  `data_servico` date NOT NULL,
  `inicio_atendimento` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reserva`
--

INSERT INTO `reserva` (`id`, `ordem`, `id_cliente`, `servico`, `observacao`, `id_usuario`, `estatus`, `data_servico`, `inicio_atendimento`) VALUES
(23, 0, 0, 'a:1:{i:0;s:1:\"7\";}', 'fdsfsdf', 1, '', '0000-00-00', '00:00:00'),
(24, 0, 0, 'a:1:{i:0;s:1:\"7\";}', '12', 1, '', '0000-00-00', '00:00:00'),
(36, 0, 0, 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"6\";}', 'teste', 1, 'Aguardando', '2020-03-17', '00:00:00'),
(37, 0, 19, 'a:3:{i:0;s:1:\"7\";i:1;s:1:\"6\";i:2;s:2:\"12\";}', '20', 22, 'Finalizado', '2020-03-20', '00:00:00'),
(38, 0, 24, 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"6\";}', '20', 22, 'Perdeu a vez', '2020-03-20', '17:20:46'),
(39, 0, 24, 'a:1:{i:0;s:2:\"11\";}', '20', 22, 'Finalizado', '2020-03-20', '17:21:57'),
(40, 24, 24, 'a:1:{i:0;s:1:\"7\";}', '20', 22, 'Perdeu a vez', '2020-03-20', '17:22:04'),
(41, 24, 24, 'a:1:{i:0;s:1:\"7\";}', '20', 22, 'Finalizado', '2020-03-20', '17:29:19'),
(42, 0, 1, 'a:3:{i:0;s:1:\"7\";i:1;s:1:\"6\";i:2;s:2:\"12\";}', '20', 22, 'Perdeu a vez', '2020-03-20', '17:29:25'),
(43, 0, 25, 'a:1:{i:0;s:1:\"7\";}', '20', 22, 'Aguardando', '2020-03-20', '00:00:00'),
(44, 0, 1, 'a:2:{i:0;s:2:\"11\";i:1;s:2:\"12\";}', '20', 22, 'Aguardando', '2020-03-20', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `reserva_tempo`
--

CREATE TABLE `reserva_tempo` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `servico` varchar(70) NOT NULL,
  `observacao` text NOT NULL,
  `atendimento` varchar(20) NOT NULL,
  `horario` time NOT NULL,
  `status` varchar(12) NOT NULL,
  `data_servico` date NOT NULL,
  `inicio_atendimento` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reserva_tempo`
--

INSERT INTO `reserva_tempo` (`id`, `nome`, `servico`, `observacao`, `atendimento`, `horario`, `status`, `data_servico`, `inicio_atendimento`) VALUES
(34, 'Icaro Sammer', 'a:1:{i:0;s:1:\"7\";}', 'teste2', 'Fulano', '15:00:00', 'Atendimento', '2020-03-13', '14:45:01'),
(39, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', 'teste', 'Fulano', '19:30:00', '', '2020-03-10', '00:00:00'),
(40, 'Icaro Sammer', 'a:1:{i:0;s:1:\"7\";}', '', 'Fulano', '12:00:00', 'Finalizado', '2020-03-10', '14:45:04'),
(41, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', 'dsfsdfs', 'Fulano', '16:30:00', '', '2020-03-10', '00:00:00'),
(42, 'Douglas', 'a:1:{i:0;s:1:\"6\";}', 'teste1', 'Fulano', '19:30:00', '', '2020-03-10', '00:00:00'),
(43, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', 'xvxcvx', 'Fulano', '19:00:00', '', '2020-03-10', '00:00:00'),
(44, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '', 'Fulano', '14:00:00', '', '2020-03-10', '00:00:00'),
(45, 'Gemaque', 'a:1:{i:0;s:1:\"9\";}', 'sdfsdf', 'Fulano', '15:30:00', '', '2020-03-10', '00:00:00'),
(48, 'Gemaque', 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"9\";}', 'fdfd', 'Fulano', '08:00:00', 'Finalizado', '2020-03-11', '20:43:18'),
(50, 'Icaro Sammer', 'a:1:{i:0;s:1:\"7\";}', 'icaro', 'Fulano', '17:00:00', '', '2020-03-10', '00:00:00'),
(52, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '1', 'Fulano', '18:00:00', '', '2020-03-10', '00:00:00'),
(54, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '12', 'Fulano', '13:00:00', '', '2020-03-12', '00:00:00'),
(61, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '', 'Fulano', '17:00:00', 'Atendimento', '2020-03-13', '20:43:12'),
(65, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '13', 'Fulano', '12:00:00', '', '2020-03-13', '00:00:00'),
(66, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '13', 'Fulano', '16:00:00', '', '2020-03-13', '00:00:00'),
(67, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '13', 'Fulano', '08:00:00', 'Atendimento', '2020-03-13', '18:39:59'),
(68, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '13', 'Fulano', '11:00:00', '', '2020-03-13', '00:00:00'),
(69, 'Gemaque', 'a:2:{i:0;s:1:\"7\";i:1;s:1:\"6\";}', 'teste', 'Fulano', '09:00:00', '', '2020-03-13', '00:00:00'),
(71, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '14', 'Fulano', '08:00:00', 'Perdeu a vez', '2020-03-14', '10:20:58'),
(72, 'Gemaque', 'a:1:{i:0;s:1:\"7\";}', '14', 'Fulano', '10:30:00', 'Perdeu a vez', '2020-03-14', '10:25:37'),
(73, 'Gemaque', 'a:1:{i:0;s:1:\"9\";}', '14', 'Fulano', '16:00:00', 'Atendimento', '2020-03-14', '10:41:46'),
(74, 'Icaro Sammer', 'a:1:{i:0;s:1:\"7\";}', '14', 'Fulano', '12:30:00', 'Perdeu a vez', '2020-03-14', '10:30:06'),
(76, 'Allepalmeira', 'a:1:{i:0;s:1:\"7\";}', '14', 'Fulano', '19:00:00', '', '2020-03-14', '00:00:00'),
(77, 'icaro', 'a:1:{i:0;s:1:\"7\";}', '14', 'Fulano', '18:30:00', '', '2020-03-14', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `servico_homem`
--

CREATE TABLE `servico_homem` (
  `id` int(11) NOT NULL,
  `nome_servico` varchar(70) DEFAULT NULL,
  `descricao` text NOT NULL,
  `categoria` varchar(40) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `tempo` time DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `servico_homem`
--

INSERT INTO `servico_homem` (`id`, `nome_servico`, `descricao`, `categoria`, `valor`, `tempo`, `img`, `alt`) VALUES
(6, 'Barba', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', 'Barba', 25, '00:45:00', 'img-upload-homem.jpg', 'alt'),
(7, 'Corte', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', 'Cabelo', 20, '00:35:00', 'img-upload-homem.jpg', 'alt'),
(9, 'Barbaa', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', 'Barba', 15, '00:30:00', 'img-upload-homem.jpg', 'alt'),
(11, 'Corte3', 'Descrição descrição descrição descrição descrição descrição descrição descrição descrição descrição', 'Cabelo', 20, '00:35:00', 'img-upload-homem.jpg', 'alt'),
(12, 'Estética', 'Estética', 'Estética facil', 45, '00:00:00', 'img-upload-homem.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `servico_mulher`
--

CREATE TABLE `servico_mulher` (
  `id` int(11) NOT NULL,
  `nome_servico` varchar(70) DEFAULT NULL,
  `descricao` text NOT NULL,
  `categoria` varchar(40) DEFAULT NULL,
  `valor` float DEFAULT NULL,
  `tempo` time DEFAULT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `servico_mulher`
--

INSERT INTO `servico_mulher` (`id`, `nome_servico`, `descricao`, `categoria`, `valor`, `tempo`, `img`, `alt`) VALUES
(6, 'Corte', 'corte', 'Corte', 25, '15:00:00', 'a2d1c159-7f5f-41f0-919e-9509a9f3ee65.jpg', 'alt'),
(7, 'Corte', 'corte', 'Corte', 20, '15:00:00', 'a2d1c159-7f5f-41f0-919e-9509a9f3ee65.jpg', 'alt'),
(8, 'Título', 'teste', 'Corte', 75, '11:00:00', 'a2d1c159-7f5f-41f0-919e-9509a9f3ee65.jpg', 'alt');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `titulo` varchar(70) DEFAULT NULL,
  `subtitulo` varchar(70) DEFAULT NULL,
  `descricao` text NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `alt` varchar(70) DEFAULT NULL,
  `usuario` varchar(20) NOT NULL,
  `data` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `titulo`, `subtitulo`, `descricao`, `img`, `alt`, `usuario`, `data`) VALUES
(17, 'Descubra seu estilo e beleza', NULL, 'SEU CORTE COM\r\nESTILO & DESIGN', 'banner.jpg', 'Salão cabeleireiro do Daniel', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) DEFAULT NULL,
  `email` varchar(70) DEFAULT NULL,
  `celular` varchar(14) NOT NULL,
  `telefone` varchar(14) NOT NULL,
  `aniversario` date NOT NULL,
  `senha` varchar(70) NOT NULL,
  `img` varchar(50) DEFAULT NULL,
  `id_nivel` int(11) DEFAULT NULL,
  `recuperar` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `email`, `celular`, `telefone`, `aniversario`, `senha`, `img`, `id_nivel`, `recuperar`) VALUES
(1, 'admin', 'icaro.sammerrr@gmail.com', '1199999999', '1199999999', '2020-03-04', '$2y$10$vE.5XKzIeHH3KqY4jA//.OUs9sqZ2b5I037q18d.D7fboouyabSNG', 'face8.jpg', 1, '1XA52z89463'),
(2, 'icaro', 'icaro.sammerr@gmail.com', '', '', '0000-00-00', '$2y$10$ltREAF22KJfj9f.fkjU6EeHbmkyuGOJM25rApc/mjlXOWwznt6O8e', 'usuario.jpg', 1, '2Xz358A7964'),
(3, 'ale', 'allepalmeira@supremodigital.com.br', '', '', '0000-00-00', '$2y$10$WHAlxK.t0Wa366mmh3KVuOLEJw9bLGBfCMZ0l9GcuAgFPT5yrnvSi', 'usuario.jpg', 1, NULL),
(22, 'Icaro Sammerr', 'icaro.sammer@gmail.com', '', '', '0000-00-00', '$2y$10$jAgQYQdF6QGf/W7nz.Bf.uADI7GaYkfMdmc0UoQvb3mswOLxdeDRW', 'face16_mini.jpg', 3, NULL),
(24, 'Icaro Sammer', 'icaro_bhf@hotmail.com', '', '', '0000-00-00', '$2y$10$Vfs0KSreQfHLE5LyCHiXJOxL1WT/p8wW8ZbKR0QbCNqfBNQ8LnwQm', NULL, 3, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria_servico`
--
ALTER TABLE `categoria_servico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `controler_nivel`
--
ALTER TABLE `controler_nivel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `depoimento`
--
ALTER TABLE `depoimento`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destaque`
--
ALTER TABLE `destaque`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distritos_sp`
--
ALTER TABLE `distritos_sp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gatilhos`
--
ALTER TABLE `gatilhos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informacao_contato`
--
ALTER TABLE `informacao_contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metodo_nivel`
--
ALTER TABLE `metodo_nivel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveis`
--
ALTER TABLE `niveis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notificacao`
--
ALTER TABLE `notificacao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas`
--
ALTER TABLE `paginas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paginas_restauracao`
--
ALTER TABLE `paginas_restauracao`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserva`
--
ALTER TABLE `reserva`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reserva_tempo`
--
ALTER TABLE `reserva_tempo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servico_homem`
--
ALTER TABLE `servico_homem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servico_mulher`
--
ALTER TABLE `servico_mulher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria_servico`
--
ALTER TABLE `categoria_servico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `controler_nivel`
--
ALTER TABLE `controler_nivel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `depoimento`
--
ALTER TABLE `depoimento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `destaque`
--
ALTER TABLE `destaque`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `distritos_sp`
--
ALTER TABLE `distritos_sp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gatilhos`
--
ALTER TABLE `gatilhos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `informacao_contato`
--
ALTER TABLE `informacao_contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `metodo_nivel`
--
ALTER TABLE `metodo_nivel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `niveis`
--
ALTER TABLE `niveis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notificacao`
--
ALTER TABLE `notificacao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `paginas`
--
ALTER TABLE `paginas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `paginas_restauracao`
--
ALTER TABLE `paginas_restauracao`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reserva`
--
ALTER TABLE `reserva`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `reserva_tempo`
--
ALTER TABLE `reserva_tempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `servico_homem`
--
ALTER TABLE `servico_homem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `servico_mulher`
--
ALTER TABLE `servico_mulher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
